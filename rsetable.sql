-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : ven. 06 nov. 2020 à 08:07
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `map`
--

-- --------------------------------------------------------

--
-- Structure de la table `rse_configmaps`
--

DROP TABLE IF EXISTS `rse_configmaps`;
CREATE TABLE IF NOT EXISTS `rse_configmaps` (
  `id_config` int(11) NOT NULL AUTO_INCREMENT,
  `name_config` varchar(255) NOT NULL,
  `value_config` varchar(255) NOT NULL,
  `description_config` varchar(255) NOT NULL,
  PRIMARY KEY (`id_config`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `rse_configmaps`
--

INSERT INTO `rse_configmaps` (`id_config`, `name_config`, `value_config`, `description_config`) VALUES
(1, 'popup', 'pop-up', "Pour paramÃ©trer le fait d\'ouvrir un pop-up ou un lien au clique sur un marqueur"),
(2, 'popup_pos', '0', 'DÃ©finit oÃ¹ se trouve la pop-up'),
(3, 'search', '0', 'Afficher ou non la bar de recherche Ã  diffÃ©rent emplacement');
-- --------------------------------------------------------

--
-- Structure de la table `rse_datamaps`
--

DROP TABLE IF EXISTS `rse_datamaps`;
CREATE TABLE IF NOT EXISTS `rse_datamaps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pin_url` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `pin_icon` varchar(500) CHARACTER SET utf8 NOT NULL,
  `image_url` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `cat_ChampPerso1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cat_ChampPerso2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cat_ChampPerso3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adresse` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code_postal` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ville` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` decimal(10,6) NOT NULL,
  `longitude` decimal(11,6) NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mot_cle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `maj_datetime` datetime NOT NULL,
  `telephone` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mail` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `site_web` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `rse_datamaps`
--

INSERT INTO `rse_datamaps` (`id`, `titre`, `pin_url`, `pin_icon`, `image_url`, `cat_ChampPerso1`, `cat_ChampPerso2`, `cat_ChampPerso3`, `adresse`, `code_postal`, `ville`, `latitude`, `longitude`, `description`, `mot_cle`, `maj_datetime`, `telephone`, `mail`, `site_web`) VALUES
(2, 'Draguignan', 'images/pin_bleu.png', '', '', 'Pin rond', 'Pin bleu', ' Choix1', 'centre ville ', '83300', 'Draguignan', '43.538066', '6.466056', '', '', '2020-11-05 11:36:28', '', '', '');

-- --------------------------------------------------------

--
-- Structure de la table `rse_gestion_filtres`
--

DROP TABLE IF EXISTS `rse_gestion_filtres`;
CREATE TABLE IF NOT EXISTS `rse_gestion_filtres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_filtres` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nom_filtres` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pin_value` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `maj_datetime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `rse_gestion_filtres`
--

INSERT INTO `rse_gestion_filtres` (`id`, `cat_filtres`, `nom_filtres`, `pin_value`, `maj_datetime`) VALUES
(4, 'ChampPerso1', 'Pin rond', '', '2020-11-05 13:04:22'),
(5, 'ChampPerso2', 'Pin bleu', 'images/pin_bleu.png', '2020-11-05 11:31:00'),
(6, 'ChampPerso3', 'Choix1', '', '2020-11-05 11:26:57'),
(7, 'ChampPerso3', 'Choix2', '', '2020-11-05 11:27:04');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
