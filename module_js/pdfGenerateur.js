// 		CRÉATION DU PDF
//Importer le cdn de jsPDF -> src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.debug.js" 
// Recup l'element HTML -> <UL>
liste_ul = document.getElementById('liste_all_orga');
//Fonction pour télécharger un array vers un pdf
function toPdf() { 				//Function v2
    document.getElementById('dlbutton').style.opacity = '0';
    var doc = new jsPDF()

    var img = new Image;
    var img2 = new Image;
    img.crossOrigin = "";  //type de l'img
    img.src = "images/logo_part75.jpg";  //url de l'image
    img2.crossOrigin = ""; 
    img2.src = "images/RepFrancev2.jpg";  
        
    img2.onload = function() {
        doc.addImage(img,0, 0);
        doc.addImage(img2,'JPEG', 170, 5, 33, 23);
        var nextLigne = 35;
        doc.setFontSize(18)
        doc.text('Liste des entreprises inclusives', 14, nextLigne)
        nextLigne = nextLigne+5;
        doc.setFontSize(11)
        doc.setTextColor(100)


        /* Ajouts dans le pdf d'un entete si l'on fait une recherche suivant les filtre actif  */
        /* Bloc Nom */
        if(document.getElementById('search_nom').innerHTML){
            /* si un filtre est actié mais trop long (plus de 70 charactère ~ ) aller on le découpe */
            var str = document.getElementById('search_nom').innerHTML
            if(str.length > 80){
                var strs = splitter(str, 80)
                doc.setTextColor(000)
                doc.text("Nom :", 14, nextLigne); 
                doc.setTextColor(100)
                for(var i = 0;i<strs.length;i++){
                    nextLigne = nextLigne + 5;
                    doc.text(strs[i], 14, nextLigne); 
                }
            }else{
                doc.setTextColor(000)
                doc.text("Nom :", 14, nextLigne); 
                doc.setTextColor(100)
                nextLigne = nextLigne + 5;
                doc.text(str, 14, nextLigne)
            }
            nextLigne = nextLigne + 10;
        /* Bloc Secteur */
        }if(document.getElementById('search_secteur').innerHTML){
            var str = document.getElementById('search_secteur').innerHTML
            if(str.length > 80){
                var strs = splitter(str, 80)
                doc.setTextColor(000)
                doc.text("Secteur d'activité :", 14, nextLigne); 
                doc.setTextColor(100) 
                for(var i = 0;i<strs.length;i++){
                    nextLigne = nextLigne + 5;
                    doc.text(strs[i], 14, nextLigne); 
                }
            }else{
                doc.setTextColor(000)
                doc.text("Secteur d'activité :", 14, nextLigne); 
                doc.setTextColor(100)
                nextLigne = nextLigne + 5;
                doc.text(str, 14, nextLigne)
            }
            nextLigne = nextLigne + 10;
        /* Bloc Structure */
        }if(document.getElementById('search_structure').innerHTML){
            doc.setTextColor(000)
            doc.text("Type de structure :", 14, nextLigne); 
            doc.setTextColor(100)
            nextLigne = nextLigne + 5;
            doc.text(document.getElementById('search_structure').innerHTML, 14, nextLigne)
            nextLigne = nextLigne + 10;
        }if(document.getElementById('search_territoire').innerHTML){
            var str = document.getElementById('search_territoire').innerHTML
            if(str.length > 80){
                var strs = splitter(str, 80)
                doc.setTextColor(000)
                doc.text("Territoire :", 14, nextLigne); 
                doc.setTextColor(100)
                for(var i = 0;i<strs.length;i++){
                    nextLigne = nextLigne + 5;
                    doc.text(strs[i], 14, nextLigne); 
                }
            }else{
                doc.setTextColor(000)
                doc.text("Territoire :", 14, nextLigne); 
                doc.setTextColor(100)
                nextLigne = nextLigne + 5;
                doc.text(str, 14, nextLigne)
            }
            nextLigne = nextLigne + 10;
        }

        /* Function pour couper un string en fonction du nb de charactère l */
        function splitter(str, l){
            var strs = [];
            while(str.length > l){
                var pos = str.substring(0, l).lastIndexOf(' ');
                pos = pos <= 0 ? l : pos;
                strs.push(str.substring(0, pos));
                var i = str.indexOf(' ', pos)+1;
                if(i < pos || i > pos+l)
                    i = pos;
                str = str.substring(i);
            }
            strs.push(str);
            return strs;
        }

        // From HTML > PDF
        nextLigne = nextLigne + 5;
        doc.autoTable({
            html: '.tableListe',
            startY: nextLigne,
            showHead: 'firstPage',
            useCss: false,
        })
            doc.save('Liste des entreprises inclusives - SiaePaca.pdf');
            setTimeout(function(){document.getElementById('dlbutton').style.opacity = '1'; }, 1000);
    };

}