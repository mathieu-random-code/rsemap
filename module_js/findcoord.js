/*	
	Ne pas oublier d'ajouter les scripts à la page dans laquelle vous souhaitez ajouter la map

	<script async defer src="https://maps.googleapis.com/maps/api/js?key=&callback=initMap&libraries=&v=weekly"defer></script>
	<script  src="https://code.jquery.com/jquery-3.5.1.js"  integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc="  crossorigin="anonymous"></script>
	<script type="text/javascript" src="../../module_js/mapsBis.js"></script>
	<script type="text/javascript" src="./findcoord.js"></script>
*/

var mainMapDiv = document.createElement('div'); // Initialise la div complete pour contenir tous ce qu'il y a en dessous
var ifram_open = false;
//Initialisation des div pour afficher la map sur laquelle on pourra trouver les coord précise 
function InitMainMap(){
	ifram_open = false
	var map = document.createElement('div'); // Initialise la div qui va contenire la carte
	map.id = 'datamapsGoogle';
	var content_copy =document.createElement('div');
	content_copy.id = 'content_copy';
	var errorMsg = document.createElement('p'); // Paragraphe pour afficher le potentiel message d'erreur
	errorMsg.id = 'mapErrorMsg';
	errorMsg.innerHTML ="Si la carte ne s'affiche pas recharger la page";
	var inputLat = document.createElement('input');
	inputLat.id = 'firstInput';
	var inputLong = document.createElement('input');
	inputLong.id = 'lastInput';
	var btnLat = document.createElement('div');
	btnLat.className = 'tooltip myLat';
	btnLat.innerHTML = "Mettre à jours les Coord";
	btnLat.onclick = function() {copyText()};
	var mapInfo = document.createElement('span');
	mapInfo.id = 'mapInfo';
	mapInfo.innerHTML = 'Cliquer & bouger le point sur la carte pour afficher les coordonnées exactes';
	mainMapDiv.appendChild(map);
	mainMapDiv.appendChild(errorMsg);
	mainMapDiv.appendChild(content_copy);
	content_copy.appendChild(inputLat);
	content_copy.appendChild(inputLong);
	content_copy.appendChild(btnLat);
	content_copy.appendChild(mapInfo);
	document.getElementById("map_findcoord").appendChild(errorMsg);
	document.getElementById("map_findcoord").appendChild(map);
	document.getElementById("map_findcoord").appendChild(content_copy);
	document.getElementById("map_findcoord").style.display = 'none';
	initMap();
};
InitMainMap();

function simpleGeocoding(link) {   
	var addr = document.getElementById('adresse').value;
	var cp = document.getElementById('cp').value;
	var ville = document.getElementById('ville').value;
	var coordonner_cal = "";
	fetch('https://nominatim.openstreetmap.org/search?q='+addr+' '+cp+' '+ville+'&format=json&polygon_geojson=1&addressdetails=1')
	.then(response => response.json())
	//.then(response => console.log(JSON.stringify(response)))
	.then(json => document.getElementById('coordonner_cal').value =json[0].lat+"|"+json[0].lon)
	.then(function() {
		coordonner_cal = document.getElementById('coordonner_cal').value;
		var coord_lat_lon =  coordonner_cal.split('|');
		var inputOpenMap = document.getElementById("openMapCoordfind");
		//Verife si on veut ouvrire la carte ou juste faire une methode "auto" moins précise
		if(ifram_open == false){
			createMap();
			initMap();
			ifram_open = true;
			inputOpenMap.value = "Fermer la carte";
		}else{
			removeMap();
			inputOpenMap.value = "Afficher la carte";
			ifram_open = false;
		}
	});  
}; 
//Mets à jours les coord
function copyText(){
	document.getElementById('latitude').value = document.getElementById("firstInput").value
	document.getElementById('longitude').value = document.getElementById("lastInput").value
	document.getElementById('linkToGoogleMap').href = "https://www.google.com/maps/search/?api=1&query="+document.getElementById("firstInput").value+","+document.getElementById("lastInput").value
};
//Affiche ou non la carte de mise a jours des coord
function createMap(){
	document.getElementById("map_findcoord").style.display = 'block';
};
function removeMap(){
	document.getElementById("map_findcoord").style.display = 'none';
};