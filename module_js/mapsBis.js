// On initialise la latitude et la longitude de Paris (centre de la carte)
var siteUrl = ""
var map;
var gmarkers = [];
//Recup des coord non précise pour centrer la carte
var latti;
var long;
const queryString = window.location.search;
if(document.getElementById('latitude').value != ""){
    var latti = document.getElementById('latitude').value;
    var long = document.getElementById('longitude').value;
}else{
    latti = 43.537;
    long = 6.464;
}     
const myLatLng = { lat: latti, lng: long };
console.log(myLatLng)
console.log(latti)

// Fonction d'initialisation de la carte
function initMap() {
  // Localise et initialise la map
  map = new google.maps.Map(
    document.getElementById('datamapsGoogle'), {
        zoom: 12,
        center: { lat: parseFloat(latti), lng: parseFloat(long) },
        options: {
          gestureHandling: 'greedy'
        },            
        mapTypeId: "roadmap",
        // Option des controles
        disableDefaultUI: true,
        mapTypeControlOptions: {
          // Cette option sert à définir comment les options se placent
          style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR
        },
      // Activation des options de navigation dans la carte (zoom...)
      navigationControl: true,
      navigationControlOptions: {
        // Comment ces options doivent-elles s'afficher
        style: google.maps.NavigationControlStyle.ZOOM_PAN
      }
    });

    //Place un marker au clique sur la carte
    google.maps.event.addListener(map, 'click', function(event) {
        removeMarkers();
        placeMarker(event.latLng);
        document.getElementById('firstInput').value = event.latLng.lat().toFixed(6);
        document.getElementById('lastInput').value =   event.latLng.lng().toFixed(6);

    });
    //remove tout les marquer présent  a faire avant d'en aficher un pour éviter les conflits d'affichage de coord
    function removeMarkers(){
        for(i=0; i<gmarkers.length; i++){
            gmarkers[i].setMap(null);
        }
    }
    //placement du marquer & recupération des ses coords
    function placeMarker(location) {
        var marker = new google.maps.Marker({
            position: location, 
            map: map,
            draggable: true
        });
        gmarkers.push(marker);

        google.maps.event.addListener(marker, 'dragend', function (evt) {
            document.getElementById('firstInput').value = evt.latLng.lat().toFixed(6);
            document.getElementById('lastInput').value =   evt.latLng.lng().toFixed(6);
        });
    
    }
}
