
// On initialise la latitude et la longitude de Paris (centre de la carte)
var siteUrl = "/rsemap"
var map;
var gmarkers = [];
var infoWindow;

/* Cluster */
var styles =[
  {
    width: 30,
    height: 30,
    className: "custom-clustericon-1",
  },
  {
    width: 40,
    height: 40,
    className: "custom-clustericon-2",
  },
  {
    width: 50,
    height: 50,
    className: "custom-clustericon-3",
  },
];
var markerClusterer = null;
/* ----- */
/* Recupère la configuration */
var configArray =  new Array();
var searchConfigUrl = siteUrl+'/module_data/read_config.php';
/* Recup un fichier xml depuis une BDD config  */
downloadUrl(searchConfigUrl, function(dataConfig){
  var xmlConfig = dataConfig.responseXML;
  var configXML = xmlConfig.documentElement.getElementsByTagName('config');
  Array.prototype.forEach.call(configXML, function(configXML)
  {   
    configArray[configXML.getAttribute('name')] = configXML.getAttribute('value');
  });
});
/* Execute en differé car sinon il ne prend pas en compte  */
setTimeout(function(){
  localStorage.setItem('configPopup', configArray['popup']);
  localStorage.setItem('configPopup_pos', configArray['popup_pos']);
  localStorage.setItem('configSearch_enable', configArray['search']);
  localStorage.setItem('map_api_key', configArray['map_api_key']);
  ApplyConfig();
},500);
/* Application de certain class en fonction des config */
function ApplyConfig(){
  var searchArrow = document.getElementById("openResponsive");
  var aside = document.getElementById("mainAside");
  switch(localStorage.getItem('configSearch_enable')){
    case "0":
      searchArrow.classList.add("asideDisable");
      aside.classList.add("asideDisable");;
      break;
    case "1":
      searchArrow.classList.add("searchRight");
      aside.classList.add("searchRight");
      break;
    case "2":
      searchArrow.classList.add("searchLeft");
      aside.classList.add("searchLeft");
      break;
    case "3":
      searchArrow.classList.add("searchBottom");
      aside.classList.add("searchBottom");
      break;
    default:
      break; 
  };

  var apiGooleMap = document.createElement("script");
  apiGooleMap.src = "https://maps.googleapis.com/maps/api/js?key="+localStorage.getItem('map_api_key')+"&libraries=geometry&v=weekly";
  apiGooleMap.id = "map_api_key";
  var footerScript = document.querySelectorAll('footer>script')
  var footer = document.querySelector('footer');
  footer.insertBefore(apiGooleMap, footerScript[0]);
  
  setTimeout(function(){
    initMap();  
  },200);
}
/* ----- */

// Fonction d'initialisation de la carte
function initMap() {
    //Création du style personalisé de la map
    var styledMapType = new google.maps.StyledMapType(
      [
        {
            "featureType": "landscape",
            "elementType": "geometry",
            "stylers": [
                {
                    "hue": "#f3f4f4"
                },
                {
                    "saturation": -84
                },
                {
                    "lightness": 59
                },
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "landscape",
            "elementType": "labels",
            "stylers": [
                {
                    "hue": "#ffffff"
                },
                {
                    "saturation": -100
                },
                {
                    "lightness": 100
                },
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "landscape.man_made",
            "elementType": "geometry.stroke",
            "stylers": [
                {
                    "lightness": "-5"
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "poi.park",
            "elementType": "geometry",
            "stylers": [
                {
                    "hue": "#83cead"
                },
                {
                    "saturation": 1
                },
                {
                    "lightness": -15
                },
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "poi.school",
            "elementType": "all",
            "stylers": [
                {
                    "hue": "#d7e4e4"
                },
                {
                    "saturation": -60
                },
                {
                    "lightness": 23
                },
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "geometry",
            "stylers": [
                {
                    "hue": "#ffffff"
                },
                {
                    "saturation": -100
                },
                {
                    "lightness": 100
                },
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "labels",
            "stylers": [
                {
                    "hue": "#bbbbbb"
                },
                {
                    "saturation": -100
                },
                {
                    "lightness": 26
                },
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "geometry",
            "stylers": [
                {
                    "saturation": 100
                },
                {
                    "lightness": -5
                },
                {
                    "visibility": "on"
                },
                {
                    "color": "#efefef"
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "elementType": "geometry",
            "stylers": [
                {
                    "saturation": 100
                },
                {
                    "lightness": -35
                },
                {
                    "visibility": "simplified"
                },
                {
                    "color": "#dddddd"
                }
            ]
        },
        {
            "featureType": "road.local",
            "elementType": "geometry.stroke",
            "stylers": [
                {
                    "lightness": "-20"
                }
            ]
        },
        {
            "featureType": "transit",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "all",
            "stylers": [
                {
                    "hue": "#00a9ff"
                },
                {
                    "saturation": 55
                },
                {
                    "lightness": -6
                },
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "labels",
            "stylers": [
                {
                    "hue": "#7fc8ed"
                },
                {
                    "saturation": 55
                },
                {
                    "lightness": -6
                },
                {
                    "visibility": "off"
                }
            ]
        }
    ]
    ,{name: 'Styled Map'});     
        
        // Localise et initialise la map
        map = new google.maps.Map(
          document.getElementById('datamaps'), {
            zoom: 18,
            center: new google.maps.LatLng(43.537577, 6.466987),
            options: {
              gestureHandling: 'greedy'
            },
           
            mapTypeId: "terrain",
            // Option des controles
            disableDefaultUI: true,
            mapTypeControlOptions: {
              // Cette option sert à définir comment les options se placent
              style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR
            },
            // Activation des options de navigation dans la carte (zoom...)
            navigationControl: true,
            navigationControlOptions: {
              // Comment ces options doivent-elles s'afficher
              style: google.maps.NavigationControlStyle.ZOOM_PAN
            }
        });
        //Applique le style de la carte
        map.mapTypes.set('styled_map', styledMapType);
        map.setMapTypeId('styled_map');

        //Affiche une image sur la map ( pour les bordure de la région PACA)
        //Pour changer le contour -> changer l'img puis aller sur https://www.scribblemaps.com/create/ pour recuperer les coord pour center l'img
        const bounds = new google.maps.LatLngBounds(
          new google.maps.LatLng(43.53675, 6.46570),
          new google.maps.LatLng(43.53904, 6.46830)
        );
        let image = "./images/tracer_ruetrans.svg";
        class USGSOverlay extends google.maps.OverlayView {
          constructor(bounds, image) {
            super();
            this.bounds = bounds;
            this.image = image;
          }
          onAdd() {
            this.div = document.createElement("div");
            this.div.style.borderStyle = "none";
            this.div.style.borderWidth = "0px";
            this.div.style.position = "absolute";
            // Create the img element and attach it to the div.
            const img = document.createElement("img");
            img.src = this.image;
            img.style.width = "100%";
            img.style.height = "100%";
            img.style.position = "absolute";
            this.div.appendChild(img);
            // Add the element to the "overlayLayer" pane.
            const panes = this.getPanes();
            panes.overlayLayer.appendChild(this.div);
          }
          draw() {
            const overlayProjection = this.getProjection();
            const sw = overlayProjection.fromLatLngToDivPixel(
              this.bounds.getSouthWest()
            );
            const ne = overlayProjection.fromLatLngToDivPixel(
              this.bounds.getNorthEast()
            );
            if (this.div) {
              this.div.style.left = sw.x + "px";
              this.div.style.top = ne.y + "px";
              this.div.style.width = ne.x - sw.x + "px";
              this.div.style.height = sw.y - ne.y + "px";
            }
          }
        }
        const overlay = new USGSOverlay(bounds, image);
        overlay.setMap(map);
        /* ---- */
      
        // Lien URL des retours BDD - Fonction GET à activier pour faire des recherches
        var searchUrl = siteUrl+'/module_data/read_datamaps.php';
        downloadUrl(searchUrl, function(data)
        {
          var xml = data.responseXML;
          var markersXML = xml.documentElement.getElementsByTagName('marker');
          Array.prototype.forEach.call(markersXML, function(markerElem)
          {   
            /* Ré appel d'un fonction car redondance */
            CreationMarker(markerElem);
          });
            /* Affichage des cluster application des certain param  */
          markerClusterer = new MarkerClusterer(map, gmarkers, {
            gridSize: 20,
            styles: styles,
            clusterClass:"custom-clustericon",
          });
        });
}

//Permet la création des markers
function CreateMarker(map, point, name, infowincontent, pin_url, pin_icon,footermodal,contentmodal,site_web)
{
  // Icone personalisé
  var image = {
    url: pin_url,// This marker is 20 pixels wide by 32 pixels high.
    size: new google.maps.Size(50, 50),// The origin for this image is (0, 0).
    origin: new google.maps.Point(0, 0),// The anchor for this image is the base of the flagpole at (0, 32).
    anchor: new google.maps.Point(0, 32)
  };
  // Instance d'ouverture des fenètres des markers
  infoWindow = new google.maps.InfoWindow;
  if(!pin_icon){
    pin_icon=name;
    document.documentElement.style.setProperty('--label-translate', '-35px');
  }
  var marker = new google.maps.Marker(
    {
    map: map,
    position: point,
    title: name,
    label:  {
      text: pin_icon,
      color:"#000",
      fontFamily:'FontAwesome,myCustomeFont',
      fontSize: "18px",
      fontWeight:"bold"
    },
    icon: image,
    animation: google.maps.Animation.DROP
  });
  // Fonction qui permet de géré le clique des boutton
  marker.addListener('click', function()
  {
    if(localStorage.getItem('configPopup') == "pop-up"){
      //Pour ouvrire une petite fenetre d'info
      infoWindow.setContent(infowincontent);
      //infoWindow.open(map, marker);
      //Boucle pour afficher la divInfo
      infowincontent.innerHTML="";
        infowincontent.style.display = "block";
        setTimeout(function(){ 
          if(localStorage.getItem('configPopup_pos')==0){
            infowincontent.style.right = "var(--divInfo-left)";
            if(localStorage.getItem('configSearch_enable') == 2){
              infowincontent.style.right ="5%";
            }
          }else{
            infowincontent.style.left = "var(--divInfo-left)";
            if(localStorage.getItem('configSearch_enable') == 1){
              infowincontent.style.left ="5%";
            }
          }
          infowincontent.style.opacity = "1";
          infowincontent.style.zIndex = "15";
        },50);
        var closeBtn = document.createElement('span');
        closeBtn.id = "divInfo_closeBtn";
        closeBtn.setAttribute('onclick','CloseDivInfo()')
        infowincontent.appendChild(closeBtn);
        infowincontent.appendChild(contentmodal);
        infowincontent.appendChild(footermodal);
    }else{
      window.open(site_web, '_blank');//Pour juste envoyer vers un liens 
    }

  });
  gmarkers.push(marker);
}

// Téléchargement du fichiers XML
function downloadUrl(url, callback)
{
  var request = window.ActiveXObject ?
      new ActiveXObject('Microsoft.XMLHTTP') :
      new XMLHttpRequest;
  request.onreadystatechange = function()
  {
    if (request.readyState == 4)
    {
      request.onreadystatechange = doNothing;
      callback(request, request.status);
    }
  };
  request.open('GET', url, true);
  request.send(null);
}
function setMapOnAll(map) {
  for (var i = 0; i < gmarkers.length; i++) {
    gmarkers[i].setMap(map);
  }
}
function clearOneMarkers()
{
  setMapOnAll(null);
}

var dataSearch1 = document.getElementsByName('ChampPerso1');
var dataSearch2 = document.getElementsByName('ChampPerso2');
var dataSearch3 = document.getElementsByName('ChampPerso3');
var filtreRecherche;

function filtreReset(){ 
  var checkboxes = document.querySelectorAll('input[type="checkbox"]');
  for (var checkbox of checkboxes) {
    checkbox.checked = this.checked;
  }
  filtreCheck();
}

function filtreCheck(){   
  filtreRecherche = "";

  for(i = 0; i < dataSearch1.length; i++){ 
    if(dataSearch1[i].checked)
    {
      filtreRecherche += "ChampPerso1[]="+dataSearch1[i].value+"&";
    }
  }
  for(i = 0; i < dataSearch2.length; i++)
  { 
    if(dataSearch2[i].checked)
    {
      filtreRecherche += "ChampPerso2[]="+dataSearch2[i].value+"&";
    }
  }
  for(i = 0; i < dataSearch3.length; i++)
  { 
    if(dataSearch3[i].checked)
    {
      filtreRecherche += "ChampPerso3[]="+dataSearch3[i].value+"&";
    }
  }
  if(filtreRecherche!=null || filtreRecherche !=""){
    SearchService(filtreRecherche);
  }
  else{
    clearOneMarkers();
  }
}

function SearchService(filter)
{
  if (markerClusterer) {
    markerClusterer.clearMarkers();
  }
  clearOneMarkers();
  if(filter != null ){
    // Lien URL des retours BDD - Fonction GET à activier pour faire des recherches
    gmarkers = [];
    if(filter != "" )
    {
      var searchUrl = siteUrl+'/module_data/read_datamaps.php?' + filter ;
    }
    else{
      var searchUrl = siteUrl+'/module_data/read_datamaps.php?';
    }
    downloadUrl(searchUrl, function(filter)
      {
        var xml = filter.responseXML;
        var markersXML = xml.documentElement.getElementsByTagName('marker');
        Array.prototype.forEach.call(markersXML, function(markerElem)
        {
          /* Ré appel d'un fonction car redondance */
          /* crétion des élément à afficher lors du clique sur un Pin */
          CreationMarker(markerElem);
        });
          var zoom = 8;
          var size = 30;
          markerClusterer = new MarkerClusterer(map, gmarkers, {
            maxZoom: zoom,
            gridSize: size,
            styles: styles,
            clusterClass: "custom-clustericon",
          });
      });
      //displayList(filter);
  }
}

/* Afficher & update la liste dans l'onglet "Liste" */
function displayList(filter){
    /* ini des bloc pour la liste*/
    var liste_ul = document.getElementById('liste_all_orga');
    var PDFliste_ul = document.getElementById('liste_all_orga_pdf');
    liste_ul.innerHTML = "";
    PDFliste_ul.innerHTML = "";
    // Lien URL des retours BDD - Fonction GET à activier pour faire des recherches
    var searchUrl = siteUrl+'/module_data/read_datamaps.php?' + filter ;
    downloadUrl(searchUrl, function(filter)
    {
      var xml = filter.responseXML;
      var markersXML = xml.documentElement.getElementsByTagName('marker');
    Array.prototype.forEach.call(markersXML, function(markerElem)
    {   
      // Sortie DATA
      var id = markerElem.getAttribute('id');
      var name = markerElem.getAttribute('name');
      var address = markerElem.getAttribute('address');   
      var code_postal = markerElem.getAttribute('code_postal');
      var ville = markerElem.getAttribute('ville');
      var structure = markerElem.getAttribute('structure');  
      var activite = markerElem.getAttribute('activite');  
      var telephone = markerElem.getAttribute('telephone');  

      var infoListcontent = document.createElement('li');

      var title = document.createElement('h3');
      title.textContent = name;

      var struc = document.createElement('p');
      struc.textContent = structure;
      struc.className = structure;

      var activ = document.createElement('p');
      activ.textContent = activite;

      var stru_infoCoord = document.createElement('h3');
      stru_infoCoord.textContent = "Coordonnées";
      
      var stru_addr = document.createElement('p');
      stru_addr.textContent = "Adresse :"+address + " " + ville + " " + code_postal;

      var stru_tel = document.createElement('p');
      stru_tel.textContent = "Tel :"+telephone;

      /* affichage dans la liste */

      infoListcontent.appendChild(title);
      infoListcontent.appendChild(struc);
      infoListcontent.appendChild(activ);
      infoListcontent.appendChild(stru_infoCoord);
      infoListcontent.appendChild(stru_addr);
      infoListcontent.appendChild(stru_tel);

      liste_ul.appendChild(infoListcontent);

      /* Création d'un tableau pour afficher dans le pdf  */
      var PDFinfoListcontent = document.createElement('tr');

      var PDFtitle = document.createElement('td');
      PDFtitle.textContent = name;

      var PDFstruc = document.createElement('td');
      PDFstruc.textContent = structure;
      PDFstruc.className = structure;

      var PDFactiv = document.createElement('td');
      PDFactiv.textContent = activite;

      var PDFstru_addr = document.createElement('td');
      PDFstru_addr.textContent = address ;

      var PDFstru_tel = document.createElement('td');
      PDFstru_tel.textContent = telephone;

      PDFinfoListcontent.appendChild(PDFtitle);
      PDFinfoListcontent.appendChild(PDFstruc);
      PDFinfoListcontent.appendChild(PDFactiv);
      PDFinfoListcontent.appendChild(PDFstru_addr);
      PDFinfoListcontent.appendChild(PDFstru_tel);
  
      PDFliste_ul.appendChild(PDFinfoListcontent);
      });
    });
};

function doNothing() {}

// Fonction d'initialisation qui s'exécute lorsque le DOM est chargé
window.onload = function()
{
};    

function CreationMarker(markerElem){
  // Sortie DATA
  // Déclaration des tous les attribute utilisé dans la divInfo
  var id = markerElem.getAttribute('id');
  var name = markerElem.getAttribute('name');
  var pin_url = markerElem.getAttribute('pin_url');
  var pin_icon = markerElem.getAttribute('pin_icon');
  var image_url = markerElem.getAttribute('image_url');
  var address = markerElem.getAttribute('address');   
  var code_postal = markerElem.getAttribute('code_postal');
  var ville = markerElem.getAttribute('ville');
  var description = markerElem.getAttribute('description');   
  var point = new google.maps.LatLng(
      parseFloat(markerElem.getAttribute('lat')),
      parseFloat(markerElem.getAttribute('lng')));
  var structure = markerElem.getAttribute('structure');  
  var territoire = markerElem.getAttribute('territoire');  
  var activite = markerElem.getAttribute('activite');  
  var telephone = markerElem.getAttribute('telephone');  
  var mail = markerElem.getAttribute('mail');  
  var site_web = markerElem.getAttribute('site_web'); 

  // Contenu du block texte - Lors d'un clique sur le marker
  var infowincontent = document.getElementById('divInfo');
  //var infowincontent = document.createElement('div');
  infowincontent.className = 'info_modale';
    /* Création de tous les éléments avec leur valeur */
  var headermodal = document.createElement('div');
  headermodal.className = 'header_modale';
  var contentmodal = document.createElement('div');
  contentmodal.className = 'content_modale';
  var footermodal = document.createElement('div');
  footermodal.className = 'footer_modale';
  var title = document.createElement('p');
  title.textContent = name;
  var typeTitle = document.createElement('p');
  typeTitle.textContent = "Type de structure";
  typeTitle.className = "subTitle smallSub";
  var struc = document.createElement('p');
  struc.textContent = structure;
  struc.className = "smallSub subContent";
  var activTitle = document.createElement('p');
  activTitle.textContent = "Actitivé";
  activTitle.className = "subTitle";
  var activ = document.createElement('p');
  activ.textContent = activite;
  var desc = document.createElement('p');
  desc.textContent = description;
  desc.className = 'desc-block';
  var teriTitle = document.createElement('p');
  teriTitle.textContent = "Territoire d'intervention";
  teriTitle.className = "subTitle";
  var teri = document.createElement('p');
  teri.textContent = territoire;
  var image = document.createElement('img');
  image.className = 'img-block';
  image.src = image_url;
  /*if(image_url == "" || image_url == undefined){ //Si on veut afficher une image aleatoire quand il n'y a pas d'images
    image.src = "https://picsum.photos/200/300";
  }*/
  var stru_infoCoord = document.createElement('h3');
  stru_infoCoord.textContent = "Contact";
  var stru_addr_title = document.createElement('p');
  stru_addr_title.className = "subTitle smallSub fas fa-map-marker";
  var stru_addr = document.createElement('p');
  stru_addr.textContent = address + ", " + ville + ", " + code_postal;
  stru_addr.className = "smallSub subContent";
  var stru_tel_title = document.createElement('p');
  stru_tel_title.className = "subTitle smallSub fas fa-phone";
  var stru_tel = document.createElement('p');
  stru_tel.textContent = telephone;
  stru_tel.className = "smallSub subContent";
  var stru_mail_title = document.createElement('p');
  stru_mail_title.className = "subTitle smallSub fas fa-envelope";
  var stru_mail = document.createElement('p');
  stru_mail.textContent = mail;
  stru_mail.className = "smallSub subContent";
  var stru_site_title = document.createElement('p');
  stru_site_title.className = "subTitle smallSub fas fa-globe";
  var stru_site = document.createElement('p');
  stru_site.textContent = site_web;
  stru_site.className = "smallSub subContent";
    /* Gestion des append - Ordre d'affichage */
  infowincontent.appendChild(contentmodal);
  infowincontent.appendChild(footermodal);

  headermodal.appendChild(title);
  contentmodal.appendChild(headermodal);
  contentmodal.appendChild(desc);
  contentmodal.appendChild(activTitle);
  contentmodal.appendChild(activ);
  contentmodal.appendChild(teriTitle);
  contentmodal.appendChild(teri);
  contentmodal.appendChild(typeTitle);
  contentmodal.appendChild(struc);
  contentmodal.appendChild(image);
  footermodal.appendChild(stru_infoCoord);
  if((address !== "")||(ville !== "")||(code_postal !== "")){
    footermodal.appendChild(stru_addr_title);
    footermodal.appendChild(stru_addr);
  }
  if(mail  !== ""){
    footermodal.appendChild(document.createElement('br'));
    footermodal.appendChild(stru_mail_title);
    footermodal.appendChild(stru_mail);
  }
  if(telephone  !== ""){
    footermodal.appendChild(document.createElement('br'));
    footermodal.appendChild(stru_tel_title);
    footermodal.appendChild(stru_tel);
  }
  if(site_web !== ""){
    footermodal.appendChild(document.createElement('br'));
    footermodal.appendChild(stru_site_title);
    footermodal.appendChild(stru_site);
  }
  footermodal.appendChild(document.createElement('br'));
  CreateMarker(map, point, name, infowincontent, pin_url, pin_icon,footermodal,contentmodal,site_web);
}
