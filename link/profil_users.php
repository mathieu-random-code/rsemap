<?php
require('Database.php'); //Accès sécurisé à la base de données !

//Remonte les informations de l'utilisateur - Remarque que le SN est généré par l'application puis il vérifie si l'usagers existe déjà dans la table !

  if (isset($_POST['sn_users'], $_POST['ImgPseudo'], $_POST['Pseudo'], $_POST['AdressePostale'], $_POST['AdresseMail'], $_POST['Age'], $_POST['NbConnect']))
  {
    $Bdd = Database::connect();
    $retourFr = array();
    $data = array();
    $index = 0;
    $lecture = $Bdd->prepare('SELECT * FROM profils WHERE sn = :sn' );
    $lecture->execute(array(
      'sn' => $_POST['sn_users']
    ));
    $donnees = $lecture->fetch();

    if ($donnees["sn"] != null AND $donnees["sn"] != "")
    {
      //UPDATE
      $connectBDD = Database::connect();
      $update = $connectBDD->prepare('UPDATE profils SET pseudo = :pseudo,
                                                        img_pseudo = :img_pseudo,
                                                        adresse_postale = :adresse_postale,
                                                        adresse_mail = :adresse_mail,
                                                        age = :age,
                                                        nb_connexion = :nb_connexion,
                                                        times_profils = NOW() WHERE sn = :sn');
      $update->execute(array(
        'pseudo' => $_POST['Pseudo'],
        'img_pseudo' => $_POST['ImgPseudo'],
        'adresse_postale' => $_POST['AdressePostale'],
        'adresse_mail' => $_POST['AdresseMail'],
        'age' => $_POST['Age'],
        'nb_connexion' => $_POST['NbConnect'],
        'sn' => $_POST['sn_users']
      ));
      echo "Update";
      $update->closecursor();
      $connectBDD = Database::disconnect();
    }
    else
    {
      //INSERT
      $connectBDD = Database::connect();
      $update = $connectBDD->prepare('INSERT INTO profils (pseudo, img_pseudo, adresse_postale, adresse_mail, age, sn, nb_connexion, times_profils)
                                      VALUES (:pseudo, :img_pseudo, :adresse_postale, :adresse_mail, :age, :sn, :nb_connexion, NOW())');
      $update->execute(array(
        'pseudo' => $_POST['Pseudo'],
        'img_pseudo' => $_POST['ImgPseudo'],
        'adresse_postale' => $_POST['AdressePostale'],
        'adresse_mail' => $_POST['AdresseMail'],
        'age' => $_POST['Age'],
        'nb_connexion' => $_POST['NbConnect'],
        'sn' => $_POST['sn_users']
      ));
      echo "INSER";
      $update->closecursor();
      $connectBDD = Database::disconnect();
    }

    // Fermeture des instances en mémoire
    $lecture->closecursor();
    $Bdd = Database::disconnect();
  }
