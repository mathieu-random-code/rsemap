<?php
// Actualites
header('Content-Type: application/json');
require('Database.php');


function ReadActualites() 
{
  $Bdd = Database::connect();
  $lecture = $Bdd->query('SELECT * FROM liste_actus');
  $index = 0;
  $retour = array();
    while ($donnees = $lecture->fetch(PDO::FETCH_ASSOC))
    	{
        $retour[$index] = array(
          "IdNumActus" => stripslashes($donnees['id']),
          "Titre" => stripslashes($donnees['titre']),
          "PorteurEvent" => stripslashes($donnees['porteur_event']),
          "ImgUrl" => stripslashes($donnees['img_url']),
          "MailAdresse" => stripslashes($donnees['mail']),
          "SiteWeb" => stripslashes($donnees['site_web']),
          "TitreSiteWeb" => stripslashes($donnees['titre_site_web']),
          "TitreVideo" => stripslashes($donnees['titre_video']),
          "UrlVideo" => stripslashes($donnees['video_url']),
          "Description" => stripslashes($donnees['descriptions']),
          "Adresse" => stripslashes($donnees['adresse']),
          "Ville" => stripslashes($donnees['ville']),
          "CodePostal" => stripslashes($donnees['code_postal']),
          "Latitude" => stripslashes($donnees['latitude']),
          "Longitude" => stripslashes($donnees['longitude']),
          "DateStart" => stripslashes($donnees['date_start']),
          "DateEnd" => stripslashes($donnees['date_end']),
          "DateMaj" => stripslashes($donnees['date_maj']));
      $index++;
    	}
    //retourne les valeurs en JSON
    echo json_encode($retour);

    // Fermeture des instances en mémoire
    $lecture->closecursor();
    $Bdd = Database::disconnect();
}
if (isset($_POST['Actualites'])) 
{
  ReadActualites();
}