<?php
// Actualites
header('Content-Type: application/json');
require('Database.php');

function ReadBonsPlans() 
{
  $Bdd = Database::connect();
  $lecture = $Bdd->query('SELECT * FROM bons_plans');
  $index = 0;
  $retour = array();
    while ($donnees = $lecture->fetch(PDO::FETCH_ASSOC))
    	{
        $retour[$index] = array(
          "IdBonsPlans" => stripslashes($donnees['id']),
          "Titre" => stripslashes($donnees['titre']),
          "PorteurEvent" => stripslashes($donnees['porteur_event']),
          "ImgUrl" => stripslashes($donnees['img_url']),
          "MailAdresse" => stripslashes($donnees['mail']),
          "TitreSiteWeb" => stripslashes($donnees['titre_site_web']),
          "SiteWeb" => stripslashes($donnees['site_web']),
          "TitreVideo" => stripslashes($donnees['titre_video']),
          "VideoUrl" => stripslashes($donnees['video_url']),
          "Description" => stripslashes($donnees['descriptions']),
          "DateMaj" => stripslashes($donnees['date_maj']));
      $index++;
    	}
    //retourne les valeurs en JSON
    echo json_encode($retour);

    // Fermeture des instances en mémoire
    $lecture->closecursor();
    $Bdd = Database::disconnect();
}
if (isset($_POST['BonsPlans'])) 
{
  ReadBonsPlans();
}