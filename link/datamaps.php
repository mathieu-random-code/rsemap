<?php
header('Content-Type: application/json');
require('Database.php'); //Accès sécurisé à la base de données !

// Exemple d'instance sécurisé à la base
// Exemple d'instance sécurisé à la base
function ReadDataMaps() {
  $Bdd = Database::connect();
  $lecture = $Bdd->query('SELECT * FROM declic_api_dj_datamaps');
  $index = 0;
  $retour = array();
    while ($donnees = $lecture->fetch(PDO::FETCH_ASSOC))
    	{
        $retour[$index] = array(
          "Idnum" => stripslashes($donnees['id']),
          "Titre" => stripslashes($donnees['titre']),
          "Adresse" => stripslashes($donnees['adresse']),
          "Ville" => stripslashes($donnees['ville']),
          "CodePostal" => stripslashes($donnees['code_postal']),
          "Latitude" => stripslashes($donnees['latitude']),
          "Longitude" => stripslashes($donnees['longitude']),
          "TelNumero" => stripslashes($donnees['tel_numero']),
          "AddMail" => stripslashes($donnees['mail']),
          "Description" => stripslashes($donnees['description']),
          "TitrePdf1" => stripslashes($donnees['titre_pdf1']),
          "UrlPdf1" => stripslashes($donnees['url_pdf1']),
          "TitrePdf2" => stripslashes($donnees['titre_pdf2']),
          "UrlPdf2" => stripslashes($donnees['url_pdf2']),
          "TitreSiteWeb1" => stripslashes($donnees['titre_site_web']),
          "SiteWeb" => stripslashes($donnees['site_web_url']),
          "TitreSiteWeb2" => stripslashes($donnees['titre_site_web_two']),
          "SiteWeb2" => stripslashes($donnees['site_web_url_two']),
          "TitreVideo" => stripslashes($donnees['titre_video_url']),
          "UrlVideo" => stripslashes($donnees['video_url']),
          "CategorieStructure" => stripslashes($donnees['categorie']),
          "SousCategorieStructure" => stripslashes($donnees['sous_categorie']),
          "LogoUrl" => stripslashes($donnees['logo_url']),
          "DelaisProcedure" => stripslashes($donnees['delais_procedure']),
          "PublicCible" => stripslashes($donnees['public_cible']),
          "AgeCible" => stripslashes($donnees['age_cible']),
          "MotCle" => stripslashes($donnees['mot_cle']),
          "MajDateTimeMaps" => $donnees['maj_datetime']);
      $index++;
    	}
    //retourne les valeurs en JSON
    echo json_encode($retour);

    // Fermeture des instances en mémoire
    $lecture->closecursor();
    $Bdd = Database::disconnect();
}
function ReadThematique() {
  $Bdd = Database::connect();
  $lecture = $Bdd->query('SELECT * FROM declic_api_thematique');
  $index = 0;
  $retour = array();
    while ($donnees = $lecture->fetch(PDO::FETCH_ASSOC))
    	{
        $retour[$index] = array(
          "Idnum" => stripslashes($donnees['id']),
          "OrderId" => stripslashes($donnees['order_id']),
          "CatThematique" => stripslashes($donnees['categorie']),
          "CatSousThematique" => stripslashes($donnees['sous_categorie']),
          "Descriptions" => stripslashes($donnees['descriptions']),
          "ImgUrlMenu" => stripslashes($donnees['img_url']),
          "MajDateTimeThematique" => $donnees['maj_datetime']);
      $index++;
    	}
    //retourne les valeurs en JSON
    echo json_encode($retour);

    // Fermeture des instances en mémoire
    $lecture->closecursor();
    $Bdd = Database::disconnect();
}
function ReadDispositif() {
  $Bdd = Database::connect();
  $lecture = $Bdd->query('SELECT * FROM declic_api_dispositif');
  $index = 0;
  $retour = array();
    while ($donnees = $lecture->fetch(PDO::FETCH_ASSOC))
    	{
        $retour[$index] = array(
          "IdDispositif" => stripslashes($donnees['id']),
          "Titre" => stripslashes($donnees['titre']),
          "Description" => stripslashes($donnees['description']),
          "Structure" => stripslashes($donnees['structure']),
          "Adresse" => stripslashes($donnees['adresse']),
          "Ville" => stripslashes($donnees['ville']),
          "CodePostal" => stripslashes($donnees['code_postal']),
          "Latitude" => stripslashes($donnees['latitude']),
          "Longitude" => stripslashes($donnees['longitude']),
          "NumeroTel" => stripslashes($donnees['numero_tel']),
          "AdresseMail" => stripslashes($donnees['adresse_mail']),
          "LogoUrl" => stripslashes($donnees['logo_url']),
          "CategorieDispositif" => stripslashes($donnees['categorie']),
          "SousCategorieDispositif" => stripslashes($donnees['sous_categorie']),
          "PublicCible" => stripslashes($donnees['public_cible']),
          "AgeCible" => stripslashes($donnees['age_cible']),
          "MotCle" => stripslashes($donnees['mot_cle']),
          "DelaisProcedure" => stripslashes($donnees['delais_procedure']),
          "TitrePdf1" => stripslashes($donnees['titre_pdf1']),
          "UrlPdf1" => stripslashes($donnees['url_pdf1']),
          "TitrePdf2" => stripslashes($donnees['titre_pdf2']),
          "UrlPdf2" => stripslashes($donnees['url_pdf2']),
          "TitreSite1" => stripslashes($donnees['titre_site1']),
          "UrlSite1" => stripslashes($donnees['url_site1']),
          "TitreSite2" => stripslashes($donnees['titre_site2']),
          "UrlSite2" => stripslashes($donnees['url_site2']),
          "TitreSite3" => stripslashes($donnees['titre_site3']),
          "UrlSite3" => stripslashes($donnees['url_site3']),
          "MajDateTimeDispositif" => $donnees['maj_datetime']);
      $index++;
    	}
    //retourne les valeurs en JSON
    echo json_encode($retour);

    // Fermeture des instances en mémoire
    $lecture->closecursor();
    $Bdd = Database::disconnect();
}
function ReadGstFiltres() {
  $Bdd = Database::connect();
  $lecture = $Bdd->query('SELECT * FROM gestion_filtres');
  $index = 0;
  $retour = array();
    while ($donnees = $lecture->fetch(PDO::FETCH_ASSOC))
    	{
        $retour[$index] = array(
          "Idnum" => stripslashes($donnees['id']),
          "CatFiltres" => stripslashes($donnees['cat_filtres']),
          "NomFiltres" => stripslashes($donnees['nom_filtres']),
          "MajDatetime" => $donnees['maj_datetime']);
      $index++;
    	}
    //retourne les valeurs en JSON
    echo json_encode($retour);

    // Fermeture des instances en mémoire
    $lecture->closecursor();
    $Bdd = Database::disconnect();
}
if (isset($_POST['DataMaps'])) {
  ReadDataMaps();
}
elseif (isset($_POST['Thematique'])){
  ReadThematique();
}
elseif (isset($_POST['dispositif'])){
  ReadDispositif();
}
elseif (isset($_POST['GstFilters'])){
  ReadGstFiltres();
}
