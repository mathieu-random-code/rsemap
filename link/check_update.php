<?php
// Actualites
header('Content-Type: application/json');
require('Database.php');


function ReadDataMaps($id_message)
{
  $Bdd = Database::connect();
  // DATAMAPS
  $lectureDataMaps = $Bdd->query('SELECT * FROM declic_api_dj_datamaps ORDER BY maj_datetime DESC');
  $donneesDataMaps = $lectureDataMaps->fetch();
  // Dispositif
  $lectureDispositif = $Bdd->query('SELECT * FROM declic_api_dispositif ORDER BY maj_datetime DESC');
  $donneesDispositif = $lectureDispositif->fetch();
  // Thematique
  $lectureThematique = $Bdd->query('SELECT * FROM declic_api_thematique ORDER BY maj_datetime DESC');
  $donneesThematique = $lectureThematique->fetch();
  // Filtrages
  $lectureFiltrage = $Bdd->query('SELECT * FROM gestion_filtres ORDER BY maj_datetime DESC');
  $donneesFiltrage = $lectureFiltrage->fetch();
  // Jules
  $lectureJules = $Bdd->query('SELECT * FROM jules ORDER BY jules_maj DESC');
  $donneesJules = $lectureJules->fetch();
  // Liste actualites
  $lectureActus = $Bdd->query('SELECT * FROM liste_actus ORDER BY date_maj DESC');
  $donneesActus = $lectureActus->fetch();
  // Urgences
  $lectureUrgences = $Bdd->query('SELECT * FROM urgences ORDER BY maj_datetime DESC');
  $donneesUrgences = $lectureUrgences->fetch();
  // Translate Data
  $lectureTrslData = $Bdd->query('SELECT * FROM translate_data ORDER BY trsl_datetime DESC');
  $donneesTrslData = $lectureTrslData->fetch();
  // Bons Plans
  $lectureBonsPlans = $Bdd->query('SELECT * FROM bons_plans ORDER BY date_maj DESC');
  $donneesBonsPlans = $lectureBonsPlans->fetch();



  $indexJSON = 0;
  $retour = array();
  $retour[$indexJSON] = array(
    "MajDataMaps" => stripslashes($donneesDataMaps['maj_datetime']),
    "MajDispositif" => stripslashes($donneesDispositif['maj_datetime']),
    "MajThematique" => stripslashes($donneesThematique['maj_datetime']),
    "MajFiltrage" => stripslashes($donneesFiltrage['maj_datetime']),
    "MajJules" => stripslashes($donneesJules['jules_maj']),
    "MajActus" => stripslashes($donneesActus['date_maj']),
    "MajUrgences" => stripslashes($donneesUrgences['maj_datetime']),
    "MajTrslData" => stripslashes($donneesTrslData['trsl_datetime']),
    "MajBonsPlans" => stripslashes($donneesBonsPlans['date_maj'])
  );
  //retourne les valeurs en JSON
  echo json_encode($retour);

  // Fermeture des instances en mémoire
  $lectureDataMaps->closecursor();
  $lectureDispositif->closecursor();
  $lectureThematique->closecursor();
  $lectureFiltrage->closecursor();
  $lectureJules->closecursor();
  $lectureActus->closecursor();
  $lectureUrgences->closecursor();
  $lectureBonsPlans->closecursor();
  $Bdd = Database::disconnect();
}

if (isset($_POST['Update']))
{
  ReadDataMaps($_POST['Update']);
}
