<?php
header('Content-Type: application/json');
require('Database.php'); //Accès sécurisé à la base de données !

// Exemple d'instance sécurisé à la base
function RemoveDiacriticReplace($chaine)
{
  $result = strtolower($chaine);
  $diacritique = array("à", "á", "ä", "â", "ã", "å", "é", "è", "ê", "ë", "ì", "í", "ï", "î", "ò", "ó", "ô", "ö", "û", "ü", "ù", "ú", "ý", "ÿ", "ç", "ñ");
  $normalize = array("a", "a", "a", "a", "a", "a", "e", "e", "e", "e", "i", "i", "i", "i", "o", "o", "o", "o", "u", "u", "u", "u", "y", "y", "c", "n");
  $result = str_replace($diacritique, $normalize, $result);
  return $result;
}
function ReadJules() {
  $Bdd = Database::connect();
  $lecture = $Bdd->query('SELECT * FROM jules');
  $index = 0;
  $retour = array();
    while ($donnees = $lecture->fetch(PDO::FETCH_ASSOC))
    	{
        $retour[$index] = array(
          "IndexCall" => stripslashes($donnees['index_call']),
          "SelectViews" => stripslashes($donnees['select_views']),
          "TitleCategorie" => stripslashes($donnees['title_categorie']),
          "MotCle" => RemoveDiacriticReplace(stripslashes($donnees['mot_cle'])),
          "Question" => stripslashes($donnees['question']),
          "Reponse" => stripslashes($donnees['reponse']),
          "ResponseBoolYes" => stripslashes($donnees['bool_yes']),
          "ResponseBoolNo" => stripslashes($donnees['bool_no']),
          "JulesMaj" => $donnees['jules_maj']);
          $index++;
    	}
    //retourne les valeurs en JSON
    echo json_encode($retour);

    // Fermeture des instances en mémoire
    $lecture->closecursor();
    $Bdd = Database::disconnect();
}

ReadJules();
