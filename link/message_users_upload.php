<?php
require('Database.php'); //Accès sécurisé à la base de données !

//Remonte les informations de l'utilisateur - Remarque que le SN est généré par l'application puis il vérifie si l'usagers existe déjà dans la table ! 

  if (isset($_POST['sn_users'], $_POST['Pseudo'], $_POST['Message'], $_POST['Statut'], $_POST['IdMessages']))
  {
    if ($_POST['IdMessages'] != null OR $_POST['IdMessages'] != "")
    {
      //UPDATE
      $connectBDD = Database::connect();
      $update = $connectBDD->prepare('UPDATE message_usagers SET statut = :statut,
                                                        datetime_messages = NOW() WHERE id = :id');
      $update->execute(array(
        'statut' => $_POST['Statut'],
        'id' => $_POST['IdMessages']
      ));
      $update->closecursor();
      $connectBDD = Database::disconnect();
    }
    else 
    {
      //INSERT
      $connectBDD = Database::connect();
      $update = $connectBDD->prepare('INSERT INTO message_usagers (sn_users, pseudo, messages_users, statut, datetime_messages)
                                      VALUES (:sn_users, :pseudo, :messages_users, :statut, NOW())');
      $update->execute(array(
        'sn_users' => $_POST['sn_users'],
        'pseudo' => $_POST['Pseudo'],
        'messages_users' => $_POST['Message'],
        'statut' => $_POST['Statut']
      ));

      $update->closecursor();
      $connectBDD = Database::disconnect();
    }
  }
