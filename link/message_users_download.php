<?php
// Actualites
header('Content-Type: application/json');
require('Database.php');


function ReadMessageAdmin($id_message) 
{
  $Bdd = Database::connect();
  $lecture = $Bdd->prepare('SELECT * FROM message_usagers WHERE sn_users = :sn_users');
  $lecture->execute(array('sn_users' => $id_message));
  $indexJSON = 0;
  $retour = array();
    while ($donnees = $lecture->fetch(PDO::FETCH_ASSOC))
    	{
        if($donnees['statut'] == 1)
        {
          
          $retour[$indexJSON] = array(
            "IdMessages" => stripslashes($donnees['id']),
            "MessagesUsers" => stripslashes($donnees['messages_users']),
            "ReponseAdmin" => stripslashes($donnees['reponse_admin']),
            "Statut" => stripslashes($donnees['statut']),
            "DateTimeMessageAdmin" => stripslashes($donnees['datetime_messages'])
          );
          $indexJSON++;
        }      
    	}
    //retourne les valeurs en JSON
    echo json_encode($retour);

    // Fermeture des instances en mémoire
    $lecture->closecursor();
    $Bdd = Database::disconnect();
}
if (isset($_POST['Messages'])) 
{
  ReadMessageAdmin($_POST['Messages']);
}