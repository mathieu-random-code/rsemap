<?php
// Actualites
header('Content-Type: application/json');
require('Database.php');

function ReadUrgences()
{
  $Bdd = Database::connect();
  $lecture = $Bdd->query('SELECT * FROM urgences');
  $index = 0;
  $retour = array();
    while ($donnees = $lecture->fetch(PDO::FETCH_ASSOC))
    	{
        $retour[$index] = array(
          "OrderId" => stripslashes($donnees['order_id']),
          "Services" => stripslashes($donnees['services']),
          "Descriptions" => stripslashes($donnees['descriptions']),
          "Numeros" => stripslashes($donnees['numeros']),
          "LogoUrl" => stripslashes($donnees['logo_url']),
          "MajDatetimeUrgences" => stripslashes($donnees['maj_datetime'])
        );
      $index++;
    	}
    //retourne les valeurs en JSON
    echo json_encode($retour);

    // Fermeture des instances en mémoire
    $lecture->closecursor();
    $Bdd = Database::disconnect();
}

if (isset($_POST['Urgences']))
{
  ReadUrgences();
}
