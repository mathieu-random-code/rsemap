<?php
header('Content-Type: application/json; charset=utf-8');
require('Database.php'); //Accès sécurisé à la base de données !

// Exemple d'instance sécurisé à la base
function SelectLangues() {
  $Bdd = Database::connect();
  $retour = array();
  $data = array();
  $name = array();
  $index = 0;
  $lecture = $Bdd->query('SELECT * FROM translate_data');
  $nbr = $Bdd->query('SELECT COUNT(name) AS nb_item FROM translate_data');
  $nb_donnees = $nbr->fetch();

  while ($donnees = $lecture->fetch(PDO::FETCH_ASSOC))
  {
    $data[$index] = $donnees["contenu"];
    $name[$index] = $donnees["name"];
    $index++;
  };
  $i = 0;

 while ($nb_donnees['nb_item'] - 1 >= $i)
 {
   $retour += array($name[$i] => $data[$i]);
   $i++;
 }
  //retourne les valeurs en JSON
  echo json_encode([$retour], JSON_UNESCAPED_UNICODE);

  // Fermeture des instances en mémoire
  $lecture->closecursor();
  $Bdd = Database::disconnect();
  $_POST['select_lang'] = "";
}

SelectLangues();
