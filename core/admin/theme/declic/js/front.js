$(document).ready(function () {

    // ------------------------------------------------------- //
    // Custom Scrollbar
    // ------------------------------------------------------ //

    if ($(window).outerWidth() > 992) {
        $("nav.side-navbar").mCustomScrollbar({
            scrollInertia: 200
        });
    }

    // Main Template Color
    var brandPrimary = '#33b35a';

    // ------------------------------------------------------- //
    // Side Navbar Functionality
    // ------------------------------------------------------ //
    $('#toggle-btn').on('click', function (e) {
        e.preventDefault();
        if ($(window).outerWidth() > 1194) {
            $('nav.side-navbar').toggleClass('shrink');
            $('.page').toggleClass('active');
        } else {
            $('nav.side-navbar').toggleClass('show-sm');
            $('.page').toggleClass('active-sm');
        }
    });

    // ------------------------------------------------------- //
    // Tooltips init
    // ------------------------------------------------------ //

    $('[data-toggle="tooltip"]').tooltip()

    // ------------------------------------------------------- //
    // Universal Form Validation
    // ------------------------------------------------------ //

    $('.form-validate').each(function () {
        $(this).validate({
            errorElement: "div",
            errorClass: 'is-invalid',
            validClass: 'is-valid',
            ignore: ':hidden:not(.summernote),.note-editable.card-block',
            errorPlacement: function (error, element) {
                // Add the `invalid-feedback` class to the error element
                error.addClass("invalid-feedback");
                //console.log(element);
                if (element.prop("type") === "checkbox") {
                    error.insertAfter(element.siblings("label"));
                }
                else {
                    error.insertAfter(element);
                }
            }
        });
    });
    // ------------------------------------------------------- //
    // Material Inputs
    // ------------------------------------------------------ //

    var materialInputs = $('input.input-material');

    // activate labels for prefilled values
    materialInputs.filter(function () {
        return $(this).val() !== "";
    }).siblings('.label-material').addClass('active');

    // move label on focus
    materialInputs.on('focus', function () {
        $(this).siblings('.label-material').addClass('active');
    });

    // remove/keep label on blur
    materialInputs.on('blur', function () {
        $(this).siblings('.label-material').removeClass('active');

        if ($(this).val() !== '') {
            $(this).siblings('.label-material').addClass('active');
        } else {
            $(this).siblings('.label-material').removeClass('active');
        }
    });

    // ------------------------------------------------------- //
    // External links to new window
    // ------------------------------------------------------ //

    $('.external').on('click', function (e) {

        e.preventDefault();
        window.open($(this).attr("href"));
    });

    // ------------------------------------------------------ //
    // For demo purposes, can be deleted
    // ------------------------------------------------------ //

    var stylesheet = $('link#theme-stylesheet');
    $("<link id='new-stylesheet' rel='stylesheet'>").insertAfter(stylesheet);
    var alternateColour = $('link#new-stylesheet');

    if ($.cookie("theme_csspath")) {
        alternateColour.attr("href", $.cookie("theme_csspath"));
    }

    $("#colour").change(function () {
        if ($(this).val() !== '') {
            var theme_csspath = 'css/style.' + $(this).val() + '.css';
            alternateColour.attr("href", theme_csspath);
            $.cookie("theme_csspath", theme_csspath, {
                expires: 365,
                path: document.URL.substr(0, document.URL.lastIndexOf('/'))
            });
        }
        return false;
    });
});

// DataTables
$(document).ready(function () {
    var table = $('#datamaps').DataTable({
        lengthChange: true,
        "pageLength": 50,
        "order": [[ 0, "desc" ]],
		    dom:'<"row"<"col-sm-4"l><"col-sm-4 text-center"p><"col-sm-4"f>>',
        stateSave: true,
        language: {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
        }
    });
});

$(document).ready(function () {
    var table = $('#urgences').DataTable({
        lengthChange: true,
        "pageLength": 50,
        "order": [[ 0, "asc" ]],
		    dom:'<"row"<"col-sm-4"l><"col-sm-4 text-center"p><"col-sm-4"f>>',
        stateSave: true,
        language: {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
        }
    });
});

// Ajax Latitute et Longitude
$(document).ready(function () {
    $("#gps").click(function () {
        var adresse = $("#adresse").val();
        var ville = $("#ville").val();
        var code_postal = $("#cp").val();

        if (code_postal != null && ville != null && adresse != null) {
            $.ajax({
                url: 'api/ajax.php',
                data: { adresse: adresse, ville: ville, cp: code_postal },
                type: 'POST',
                success: function (data) {
                    $(".result").html(data);
                }
            });
        }
    });
});
// ------------- Condition Upload de fichier --------------------------------------------
if (document.querySelector('#img_structure') != null && document.querySelector('#progressbar_structure') != null) {
    UploadFiles('#img_structure', '#progressbar_structure', 'datamaps_create.php');
}
// ------------- Fonction Upload fichier -------------------------------------------
function UploadFiles(urlPdf, progressBar, fichierPhp) {
    var fileInput = document.querySelector(urlPdf),
        progress = document.querySelector(progressBar);

    fileInput.addEventListener('change', function () {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', fichierPhp);
        xhr.upload.addEventListener('progress', function (e) {
            progress.value = e.loaded;
            progress.max = e.total;
        });
        xhr.addEventListener('load', function () {
        });
        var form = new FormData();
        form.append('file', fileInput.files);
        xhr.send(form);
    });
}
// -------------------------------- Fonction ToggleCat ----------------------------
function openCat(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
        tabcontent[i].setAttribute("name", "null");
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    document.getElementById(cityName).setAttribute("name", "sous_categorie");
    evt.currentTarget.className += " active";
}

// -------------------------------- Check date realtime 01 ----------------------------
function runClock() {
    today = new Date();
    hours = today.getHours();
    minutes = today.getMinutes();
    seconds = today.getSeconds();
    timeValue = hours;
    // Les deux prochaines conditions ne servent que pour l'affichage.
    // Si le nombre de minute est inférieur à 10, alors on rajoute un 0 devant.
    timeValue += ((minutes < 10) ? ":0" : ":") + minutes;
    timeValue += ((seconds < 10) ? ":0" : ":") + seconds;
    document.getElementById("time").value = timeValue;
    document.getElementById("time2").innerHTML = timeValue;
    timerID = setTimeout("runClock()", 1000);
    timerRunning = true;
}

// -------------------------------- Check date realtime 02 ----------------------------

// function CompareDate() {
//     var todayDate = new Date(); // Date du jour
//     var dateOne = new Date(2020, 01, 01);
//     if (todayDate > dateOne) {
//          alert("La date du jour est plus grande que la date à comparer");
//      }else {
//          alert("La date du jour est plus petite que la date à comparer");
//      }
//  }
//  CompareDate();
