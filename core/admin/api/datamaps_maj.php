<div class="windows_float">
	<div class="card-body">
		<form action="<?php echo $linkpage; ?>" method="post" enctype="multipart/form-data">
			<!-- Nom -->
			<div class="form-group row">
				<label class="col-sm-4 form-control-label">Titre</label>
				<div class="col-sm-8">
					<input value="<?php echo $donnees['titre']; ?>" type="text" name="titre" class="id_meta_description" class="form-control" size="50" maxlength="255" required>
				</div>
			</div>
			<!-- Adresse -->
			<div class="form-group row">
				<label class="col-sm-4 form-control-label">Adresse Postale (ex : 1 rue de la place)</label>
				<div class="col-sm-8">
					<input value="<?php echo $donnees['adresse']; ?>" type="text" name="adresse" id="adresse" class="form-control" size="50" maxlength="255" required>
				</div>
			</div>
			<!-- Code Postal -->
			<div class="form-group row">
				<label class="col-sm-4 form-control-label">Code Postal</label>
				<div class="col-sm-8">
					<input value="<?php echo $donnees['code_postal']; ?>" name="code_postal" id="cp" class="form-control" size="50" maxlength="255" required>
				</div>
			</div>
			<!-- Ville -->
			<div class="form-group row">
				<label class="col-sm-4 form-control-label">Ville</label>
				<div class="col-sm-8">
					<input value="<?php echo $donnees['ville']; ?>" type="text" name="ville" id="ville" class="form-control" size="50" maxlength="255" required>
				</div>
			</div>

			<label style="color:#fff">Trouver les coordonnées</label>
			<input type="button" name="coordonner" id="openMapCoordfind" value="Sur une carte" onclick="simpleGeocoding(true)">
			<input type="hidden" name="coordonner_cal" value="" id="coordonner_cal">
			<br>
			<!--<span style="color:#fff;font-size: .8rem;">(Si la carte ne s'ouvre pas enlever l'adresse postale puis cliquer sur afficher la carte) </span>-->

			<!-- Latitude -->
			<div class="form-group row">
				<label class="col-sm-4 form-control-label">Latitude (ex : 43.394587)</label>
				<div class="col-sm-8">
					<input value="<?php echo $donnees['latitude']; ?>" name="latitude" id="latitude" class="form-control" size="50" maxlength="255" required>
				</div>
			</div>
			<!-- Longitude -->
			<div class="form-group row">
				<label class="col-sm-4 form-control-label">Longitude (ex : 6.394587)</label>
				<div class="col-sm-8">
					<input value="<?php echo $donnees['longitude']; ?>" name="longitude" id="longitude" class="form-control" size="50" maxlength="255" required>
					<a id="linkToGoogleMap" class="form-control-label" <?php echo 'href="https://www.google.com/maps/search/?api=1&query='.$donnees['latitude'].','.$donnees['longitude'].'"'; ?> target="_blank" >Afficher la position sur Google Maps</a>
				</div>
			</div>
			<div id='map_findcoord' style="text-align:center"> 
			</div>
			<!-- Logo URL -->
			<div class="form-group row">
				<label class="col-sm-4 form-control-label">Logo Url **</label>
				<div class="col-sm-8">
					<div class="custom-file">
						<img class="img-fluid img-thumbnail" src="<?php echo $donnees['image_url']; ?>"/>
						<input value="<?php echo $donnees['image_url']; ?>" type="url" name="logo_url" id="logo_url" class="form-control" size="50">
						<input value="" style="color: white;" type="file" name="img_structure" id="img_structure" accept=".jpg, .jpeg, .png">
						<progress value="0" id="progressbar_structure"></progress>
					</div>
				</div>
			</div>
			<!-- Telephone / mail / Site web -->
			<div class="form-group row">
				<label class="col-sm-4 form-control-label">Téléphone</label>
				<div class="col-sm-8">
					<input value="<?php echo $donnees['telephone']; ?>" type="text" name="telephone" id="telephone" class="form-control" size="50" maxlength="255">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-4 form-control-label">Mail</label>
				<div class="col-sm-8">
					<input value="<?php echo $donnees['mail']; ?>" type="text" name="mail" id="mail" class="form-control" size="50" maxlength="255">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-4 form-control-label">Site Web</label>
				<div class="col-sm-8">
					<input value="<?php echo $donnees['site_web']; ?>" type="text" name="site_web" id="site_web" class="form-control" size="50" maxlength="255">
				</div>
			</div>
			<!-- Déscription -->
			<div class="form-group row">
				<label class="col-sm-4 form-control-label">Description</label>
				<div class="col-sm-8">
					<textarea type="text" name="description" id="description" class="form-control" size="50"><?php echo stripslashes($donnees['description']); ?></textarea>
				</div>
			</div>
			
			<?php include("api/views_filter_ChampPerso1.php"); ?>

			<?php include("api/views_filter_ChampPerso2.php"); ?>

			<?php include("api/views_filter_ChampPerso3.php"); ?>

			<!-- Mot clé -->
			<div class="form-group row">
			  <label class="col-sm-4 form-control-label">Mot Clef</label>
			  <div class="col-sm-8">
			  	<input value="<?php echo $donnees['mot_cle']; ?>" type="text" name="mot_cle" class="id_meta_description" class="form-control" size="50" maxlength="255">
			  </div>
			</div>
			<div class="d-flex justify-content-center">
				<div class="p-2">
					<button name="id_maj_post" class="btn btn-primary" type="submit" value="<?php echo $donnees['id']; ?>">
						<i class="fas fa-check-square"></i> Valider
					</button>
					<button type="button" class="btn btn-danger" onclick="window.location.href='<?php echo $linkpage; ?>'">
						<i class="fas fa-window-close"></i> Annuler
					</button>
				</div>
			</div>
			<p style="color: white;">** : Taille limite de l'image : 150Ko - Compatible : JPEG et PNG</p>
		</form>
	</div>
</div>

<!-- Google Map API -->
<script async defer src="https://maps.googleapis.com/maps/api/js?key=&callback=initMap&libraries=&v=weekly"defer></script>

<script  src="https://code.jquery.com/jquery-3.5.1.js"  integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc="  crossorigin="anonymous"></script>
<script type="text/javascript" src="../../module_js/mapsBis.js"></script>
<script type="text/javascript" src="../../module_js/findcoord.js"></script>