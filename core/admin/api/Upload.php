<?php

if(!defined('PLX_ROOT')) exit;
# Control de l'accès à la page en fonction du profil de l'utilisateur connecté
$plxAdmin->checkProfil(PROFIL_ADMIN, PROFIL_MODERATOR);

class Upload
{
    public static function ImageSqlData($fichierTemp, $fichier)
    {
        if (preg_match("/\bjpg\b/i", $fichier) || preg_match("/\bjpeg\b/i", $fichier)) //JPG
        {
            // Chemin du dossier
            $uploadFileDir = '../../data/medias/';
            $dest_path = $uploadFileDir . $fichier;
        
            // Deplacement du fichier temporaire
            move_uploaded_file($fichierTemp, $dest_path);
        
            // Récuperation de imagecreatefromjpeg avec son chemin
            $img = imagecreatefromjpeg($uploadFileDir . $fichier);
        
            // Enfin on la remet ou elle était avec la qualité qu'on veut ! (ici 50)
            // J'ai un plantage à ce niveau là, la taille de l'image qui dépasse plus 1Mo à tendance à planter. 
            // Il faudrat mettre une condition et faire en sorte de signaler à l'utilisateur de ne pas dépacer une certaine taille !
            imagejpeg($img, $uploadFileDir . $fichier, 50); 
        
            // Récuperation de l'url POST
            return "https://www.draguignan-quartierdesarts.fr/rse/data/medias/". $fichier;
        }
        elseif (preg_match("/\bpng\b/i", $fichier)) //PNG
        {
            // Chemin du dossier
            $uploadFileDir = '../../data/medias/';
            $dest_path = $uploadFileDir . $fichier;
        
            // Deplacement du fichier temporaire
            move_uploaded_file($fichierTemp, $dest_path);
        
            // Récuperation de l'url POST
            return "https://www.draguignan-quartierdesarts.fr/rse/data/medias/". $fichier;
        }
        else
        {
            DisplayAlerte("Le format de fichier n'est pas supporté ! Veuillez choisir une image au format jpeg avec une limite de 512Ko ");
            return "";
        }
    }

    public static function UploadPDF($fichierTemp, $fichier)
    {
        if (preg_match("/\bpdf\b/i", $fichier)) //PDF
        {
            // Chemin du dossier
            $uploadFileDir = '../../data/medias/pdf/';
            $dest_path = $uploadFileDir . $fichier;
        
            // Deplacement du fichier temporaire
            move_uploaded_file($fichierTemp, $dest_path);

            return "https://www.draguignan-quartierdesarts.fr/data/medias/pdf/". $fichier;
        }
    }
}