<?php
// Module d'enregistrement DECLIC
if(!defined('PLX_ROOT')) exit;
# Control de l'accès à la page en fonction du profil de l'utilisateur connecté
$plxAdmin->checkProfil(PROFIL_ADMIN, PROFIL_MODERATOR);
// ----------------------- VARIABLE TEXTE ---------------------------------------
$TxtImg = "L'image doit être inferieur 150 ko";
$TxtStrc = "Le nom de la structure et sa description est obligatoire";
$TxtCP = "L'adresse postale est obligatoire";
$TxtGPS = "Latitude et Longitude non renseigné !, Enregistrement annulé !";
$TxtNoImg = "Vous n'avez pas choisi le logo";
$TxtOpOk = "Enregistrement éffectué";
$TxtDelOk = "Données supprimées, opération éffectué";

function CheckboxFilterActivite()
{
  if(isset($_POST["ChampPerso1"]))
  {
    // Cummule les choix public cible - Checkbox
    $indexAct = 0;
    $lineActivite = "";
    $checkAct = array();
    foreach($_POST["ChampPerso1"] as $checkoptionsAct)
    {
      $checkAct[$indexAct] = $checkoptionsAct;
      $lineActivite = $lineActivite . $checkAct[$indexAct];
      $indexAct++;
    }
    return $lineActivite;
  }
  else
  {
    return "";
  }
}

function CheckboxFilterStructure()
{
  if(isset($_POST["ChampPerso2"]))
  {
    // Age Checkbox
    $indexStr = 0;
    $lineStr = "";
    $checkStr = array();
    foreach($_POST["ChampPerso2"] as $checkoptionsStr)
    {
      $checkStr[$indexStr] = $checkoptionsStr;
      $lineStr = $lineStr . $checkStr[$indexStr];
      $indexStr++;
    }
    return $lineStr;
  }
  else
  {
    return "";
  }
}

function CheckboxFilterTerritoire()
{
  if(isset($_POST["ChampPerso3"]))
  {
    // Age Checkbox
    $indexTer = 0;
    $lineTer = "";
    $checkTer = array();
    foreach($_POST["ChampPerso3"] as $checkoptionsTer)
    {
      $checkTer[$indexTer] = $checkoptionsTer;
      $lineTer = $lineTer ." ".$checkTer[$indexTer];
      $indexTer++;
    }
    return $lineTer;
  }
  else
  {
    return "";
  }
}

function ChangePin($filtre)
{

 $Bdd = Database::connect();

    $pin_info = $Bdd->query('SELECT pin_value FROM rse_gestion_filtres where nom_filtres = "'.$filtre.'" ');
    $pin = $pin_info->fetch();
    $pin_value = $pin['pin_value'];

  $Bdd = Database::disconnect();

    return $pin_value;

} 

// --------------------------------- FONCTION -----------------------------------------------
function InsertSQL($vUrl)
{
  $pinUrl = ChangePin(CheckboxFilterStructure());
  $pinValue = ChangePin($_POST["ChampPerso1"][0]);

  $Bdd = Database::connect();
  $req = $Bdd->prepare('INSERT INTO rse_datamaps (titre, pin_url, pin_icon, image_url,
                                                          cat_ChampPerso1,
                                                          cat_ChampPerso2,
                                                          cat_ChampPerso3,
                                                          adresse,
                                                          ville,
                                                          code_postal,
                                                          latitude,
                                                          longitude,
                                                          description,
                                                          telephone,
                                                          mail,
                                                          site_web,
                                                          mot_cle,
                                                          maj_datetime)
                              VALUES (:titre, :pin_url,:pin_icon,
                                      :image_url, 
                                      :cat_ChampPerso1,
                                      :cat_ChampPerso2,
                                      :cat_ChampPerso3,
                                      :adresse,
                                      :ville,
                                      :code_postal,
                                      :latitude,
                                      :longitude,
                                      :description,
                                      :telephone,
                                      :mail,
                                      :site_web,
                                      :mot_cle, NOW())');
          try {
            $req->execute(array(
              'titre' => $_POST['titre'],
              'pin_url' =>  $pinUrl,
              'pin_icon' =>  $pinValue,
              'image_url' => $vUrl,
              'cat_ChampPerso1' => CheckboxFilterActivite(),
              'cat_ChampPerso2' => CheckboxFilterStructure(),
              'cat_ChampPerso3' => CheckboxFilterTerritoire(),
              'adresse' => $_POST['adresse'],
              'ville' => $_POST['ville'],
              'code_postal' => $_POST['code_postal'],
              'latitude' => $_POST['latitude'],
              'longitude' => $_POST['longitude'],
              'description' => $_POST['description'],
              'telephone' => $_POST['telephone'],
              'mail'=>$_POST['mail'],
              'site_web' => $_POST['site_web'],
              'mot_cle' => $_POST['mot_cle']
            ));
        } catch (PDOException $e) {
            echo 'Échec lors de la connexion : ' . $e->getMessage();
        }
 

  $req->closecursor();
  $Bdd = Database::disconnect();

}

// ---------------------------------------- Mise à jours d'une entrée dans la table
function UpdateSQL($vUrl)
{
  $pinUrl = ChangePin(CheckboxFilterStructure());
  $pinValue = ChangePin($_POST["ChampPerso1"][0]);

  $Bdd = Database::connect();
  $update = $Bdd->prepare("UPDATE rse_datamaps SET titre = :titre,
                                                            pin_url = :pin_url,
                                                            pin_icon = :pin_icon,
                                                            image_url = :image_url,
                                                            cat_ChampPerso1 = :cat_ChampPerso1,
                                                            cat_ChampPerso2 = :cat_ChampPerso2,
                                                            cat_ChampPerso3 = :cat_ChampPerso3,
                                                            adresse = :adresse,
                                                            ville = :ville,
                                                            code_postal = :code_postal,
                                                            latitude = :latitude,
                                                            longitude = :longitude,
                                                            description = :description,
                                                            telephone = :telephone,
                                                            mail = :mail,
                                                            site_web = :site_web,
                                                            mot_cle = :mot_cle,
                                                            maj_datetime = NOW() WHERE id = :id");
  $update->execute(array(
    'titre' => $_POST['titre'],
    'pin_url' =>  $pinUrl,
    'pin_icon' =>  $pinValue,
    'image_url' => $vUrl,
    'cat_ChampPerso1' => CheckboxFilterActivite(),
    'cat_ChampPerso2' => CheckboxFilterStructure(),
    'cat_ChampPerso3' => CheckboxFilterTerritoire(),
    'adresse' => $_POST['adresse'],
    'ville' => $_POST['ville'],
    'code_postal' => $_POST['code_postal'],
    'latitude' => $_POST['latitude'],
    'longitude' => $_POST['longitude'],
    'description' => $_POST['description'],
    'telephone' => $_POST['telephone'],
    'mail'=> $_POST['mail'],
    'site_web' => $_POST['site_web'],
    'mot_cle' => $_POST['mot_cle'],
    'id' => $_POST['id_maj_post']
  ));
  $update->closecursor();
  $Bdd = Database::disconnect();
}

function ReturnMaj($id)
{
  if($id == "")
  {
    $Bdd = Database::connect();
    $endLine = $Bdd->query('SELECT * FROM rse_datamaps ORDER BY id DESC');
    $id = $endLine->fetch();
    $_SESSION['id'] = $id['id'];
    $endLine->closecursor();
    $Bdd = Database::disconnect();
  }
  else
  {
    $_SESSION['id'] = $id;
  }
}

// Suppresion d'une entrée
function EraseSQL($valID)
{
  $Bdd = Database::connect();
  $delete_item = $Bdd->prepare('DELETE FROM rse_datamaps WHERE id = :id_delete');
  $delete_item->execute(array('id_delete' => $valID));
  $delete_item->closecursor();
  $Bdd = Database::disconnect();
}

// DisplayAlerte
function DisplayAlerte($txt)
{
  echo "<script>alert(\"" . $txt . "\");</script>"; // Aie !
}

// --------------------------------------- CONDITION ----------------------------------------------
if(isset($_POST['create_page']))
{
  if (isset($_POST['titre']) != null AND isset($_POST['description']) != null)
  {
    if(isset($_POST['adresse']) != null AND isset($_POST['ville']) != null AND isset($_POST['code_postal']) != null)
    {
      if(isset($_POST['longitude']) != "" AND isset($_POST['latitude']) != "")
      {
        // Gestion des images
        if($_FILES['img_structure'] != null)
        {
          $fichierTemp = $_FILES['img_structure']['tmp_name'];
          $fichier = $_FILES['img_structure']['name'];
          $taille = filesize($fichierTemp); //Récupère la taille du fichier

          if($taille <= 153600 AND $taille > 0 AND $taille != null)
          {
            $vUrl = Upload::ImageSqlData($fichierTemp, $fichier); // Enregistrement de l'imagee
          }
          elseif($taille >= 153600)
          {
            $vUrl = '';
            $indexError = 1;
            DisplayAlerte($TxtImg);
          }
          elseif(isset($_POST['img_structure']) AND $_POST['img_structure'] != null)
          {
            $vUrl = $_POST['img_structure'];
          }
          else
          {
            $vUrl = '';
          }
        }
        elseif(isset($_POST['img_structure']) AND $_POST['img_structure'] != null)
        {
          $vUrl = $_POST['img_structure'];
        }
        else
        {
          $vUrl = '';
        }

        // Traitement des données
        if(isset($indexError) == 1)
        {
          InsertSQL($vUrl);
          ReturnMaj('');
          $indexError == 0;
        }
        else
        {
          InsertSQL($vUrl);
        }
      }
      else
      {
        DisplayAlerte($TxtGPS);
      }
    }
    else
    {
      DisplayAlerte($TxtCP);
    }
  }
  else
  {
    DisplayAlerte($TxtStrc);
  }
}
elseif (isset($_POST['id_maj_post']))
{
  if (isset($_POST['titre']) != null AND isset($_POST['description']) != null)
  {
    if(isset($_POST['adresse']) != null AND isset($_POST['ville']) != null AND isset($_POST['code_postal']) != null)
    {
      if(isset($_POST['longitude']) != "" AND isset($_POST['latitude']) != "")
      {
        // Gestion des images
        if($_FILES['img_structure'] != null)
        {
          $fichierTemp = $_FILES['img_structure']['tmp_name'];
          $fichier = $_FILES['img_structure']['name'];
          $taille = filesize($fichierTemp); //Récupère la taille du fichier

          if($taille <= 153600 AND $taille > 0 AND $taille != null)
          {
            $vUrl = Upload::ImageSqlData($fichierTemp, $fichier); // Enregistrement de l'imagee
          }
          elseif($taille >= 153600)
          {
            $vUrl = '';
            $indexError = 1;
            DisplayAlerte($TxtImg);
          }
          elseif(isset($_POST['logo_url']))
          {
            $vUrl = $_POST['logo_url'];
          }
          else
          {
            $vUrl = '';
          }
        }
        elseif(isset($_POST['logo_url']))
        {
          $vUrl = $_POST['logo_url'];
        }
        else
        {
          $vUrl = '';
        }
        // Traitement des données finaux
        if(isset($indexError) == 1)
        {
          // Ici j'ai un problème - renvoie de page possible, enregistrement BDD impossible
          UpdateSQL($vUrl);
          ReturnMaj($_POST['id_maj_post']);
          $indexError == 0;
        }
        else
        {
          UpdateSQL($vUrl);
        }
      }
      else
      {
        DisplayAlerte($TxtGPS);
      }
    }
    else
    {
      DisplayAlerte($TxtCP);
    }
  }
  else
  {
    DisplayAlerte($TxtStrc);
  }
}
elseif (isset($_POST['id_del']))
{
  EraseSQL($_POST['id_del']);
}
