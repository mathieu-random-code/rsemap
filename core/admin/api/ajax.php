<?php

if(isset($_POST['adresse']) != "" && isset($_POST['ville']) != "" && isset($_POST['cp']) != "")
{
  $adresse = $_POST['adresse'];
  $ville = $_POST['ville'];
  $cp = $_POST['cp'];
  $data = array(
      'street'     => $adresse,
      'postalcode' => $cp,
      'city'       => $ville,
      'country'    => 'france',
      'format'     => 'json'
    );

  $url = 'https://nominatim.openstreetmap.org/search.php?' . http_build_query($data);
  $ch = curl_init($url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 5.1; rv:56.0) Gecko/20100101 Firefox/56.0');
  $geopos = curl_exec($ch);
  curl_close($ch);

  $json_data = json_decode($geopos, true);
  $index = 0;
  if($json_data != null)
  {
    foreach ($json_data as $v) 
    {
      if($index == 0)
      {
        echo '<input type="text" name="latitude" id="latitude" value="' . htmlspecialchars($v['lat']) . '" class="form-control" size="50" maxlength="255" required/>';
        echo '<input type="text" name="longitude" id="longitude" value="' . htmlspecialchars($v['lon']) . '" class="form-control" size="50" maxlength="255" required/>'."\n";
        echo '<a class="form-control-label" href="https://www.google.com/maps/search/?api=1&query='.htmlspecialchars($v['lat']).','.htmlspecialchars($v['lon']).'" target="_blank" >Afficher la position sur Google Maps</a>';
        $index++;
      }
    }
  }
  else 
  {
    echo '<input type="text" name="latitude" id="latitude" value="" class="form-control" size="50" maxlength="255" required/>';
    echo '<input type="text" name="longitude" id="longitude" value="" class="form-control" size="50" maxlength="255" required/>'."\n"; 
  }
}
else 
{
  echo '<input type="text" name="latitude" id="latitude" value="" class="form-control" size="50" maxlength="255" required/>';
  echo '<input type="text" name="longitude" id="longitude" value="" class="form-control" size="50" maxlength="255" required/>'."\n"; 
}