<div class="windows_float">
	<div class="card-body">
		<form action="<?php echo $linkpage; ?>" method="post" enctype="multipart/form-data" id="form">
			<!-- Nom -->
			<div class="form-group row">
			  <label class="col-sm-4 form-control-label">Titre</label>
			  <div class="col-sm-8">
			  <input type="text" value="" name="titre" id="titre" class="form-control" size="50" maxlength="175" required>
			  </div>
			</div>

			<!-- Adresse -->
			<div class="form-group row">
			  <label class="col-sm-4 form-control-label">Adresse Postale</label>
			  <div class="col-sm-8">
			  <input placeholder="exemple : 1 rue de la place" type="text" value="" name="adresse" id="adresse" class="form-control" size="50" maxlength="255" required>
			  </div>
			</div>

			<!-- Code Postal -->
			<div class="form-group row">
			  <label class="col-sm-4 form-control-label">Code Postal</label>
			  <div class="col-sm-8">
			  <input placeholder="exemple : 83000" value="" name="code_postal" id="cp" class="form-control" size="50" maxlength="5" required>
			  </div>
			</div>

			<!-- Ville -->
			<div class="form-group row">
			  <label class="col-sm-4 form-control-label">Ville</label>
			  <div class="col-sm-8">
			  <input placeholder="exemple : Toulon" type="text" value="" name="ville" id="ville" class="form-control" size="50" maxlength="255" required>
			  </div>
			</div>

			<!-- Latitude - Longitude 
			<div class="form-group row">
				<label class="col-sm-4 form-control-label">
				<button class="btn btn-info" id="gps" type="button">Récupérer Latitude / Longitude</button>
				</label>
				<div class="result col-sm-8">
					<input type="text" id="id_meta_description" name="latitude" id="latitude" value="" placeholder="Latitude (ex : 43.56879)" class="form-control" size="50" maxlength="255" required/>
				<input type="text" id="id_meta_description" name="longitude" id="longitude" value="" placeholder="Longitude (ex : 6.36859)" class="form-control" size="50" maxlength="255" required/>
				</div>
			</div>
-->


			<label style="color:#fff">Trouver les coordonnées</label>
			<input type="button" name="coordonner" id="openMapCoordfind" value="Sur une carte" onclick="simpleGeocoding(true)">
			<input type="hidden" name="coordonner_cal" value="" id="coordonner_cal">
			<br>
			<!--<span style="color:#fff;font-size: .8rem;">(Si la carte ne s'ouvre pas enlever l'adresse postale puis cliquer sur afficher la carte) </span>-->

			<!-- Latitude -->
			<div class="form-group row">
				<label class="col-sm-4 form-control-label">Latitude (ex : 43.394587)</label>
				<div class="col-sm-8">
					<input value="<?php echo $donnees['latitude']; ?>" name="latitude" id="latitude" class="form-control" size="50" maxlength="255" required>
				</div>
			</div>
			<!-- Longitude -->
			<div class="form-group row">
				<label class="col-sm-4 form-control-label">Longitude (ex : 6.394587)</label>
				<div class="col-sm-8">
					<input value="<?php echo $donnees['longitude']; ?>" name="longitude" id="longitude" class="form-control" size="50" maxlength="255" required>
					<a id="linkToGoogleMap" class="form-control-label" <?php echo 'href="https://www.google.com/maps/search/?api=1&query='.$donnees['latitude'].','.$donnees['longitude'].'"'; ?> target="_blank" >Afficher la position sur Google Maps</a>
				</div>
			</div>
			<div id='map_findcoord' style="text-align:center"> </div>


			<!-- Logo URL -->
			<div class="form-group row">
			<label class="col-sm-4 form-control-label">Logo **</label>
				<div class="col-sm-8">
					<input value="" style="color: white;" type="file" name="img_structure" id="img_structure" accept=".jpg, .jpeg, .png"/>
					<progress value="0" id="progressbar_structure"></progress>
				</div>
			</div>
			<!-- Telephone / mail / Site web -->
			<div class="form-group row">
				<label class="col-sm-4 form-control-label">Téléphone</label>
				<div class="col-sm-8">
					<input value="" type="text" name="telephone" id="telephone" class="form-control" size="50" maxlength="255">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-4 form-control-label">Mail</label>
				<div class="col-sm-8">
					<input value="" type="text" name="mail" id="mail" class="form-control" size="50" maxlength="255">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-4 form-control-label">Site Web</label>
				<div class="col-sm-8">
					<input value="" type="text" name="site_web" id="site_web" class="form-control" size="50" maxlength="255">
				</div>
			</div>
			<!-- Description -->
			<div class="form-group row">
			  <label class="col-sm-4 form-control-label">Description</label>
			  <div class="col-sm-8">
			  	<textarea type="text" name="description" id="description" class="form-control" size="50"></textarea>
			  </div>
			</div>

			<?php include("api/views_filter_ChampPerso1.php"); ?>

			<?php include("api/views_filter_ChampPerso2.php"); ?>

			<?php include("api/views_filter_ChampPerso3.php"); ?>

			<!-- Mot clé -->
			<div class="form-group row">
			  <label class="col-sm-4 form-control-label">Mot Clef</label>
			  <div class="col-sm-8">
			  <input value="" type="text" name="mot_cle" id="mot_cle" class="form-control" size="50" maxlength="255">
			  </div>
			</div>

			<div class="d-flex justify-content-center">
				<div class="p-2">
					<button name="create_page" class="btn btn-primary" type="submit" value="valider">
						<i class="fas fa-check-square"></i> Valider
					</button>
					<button type="button" class="btn btn-danger" onclick="window.location.href='<?php echo $linkpage; ?>'">
						<i class="fas fa-window-close"></i> Annuler
					</button>
				</div>
			</div>
			<p style="color: white;">** : Taille limite de l'image : 150Ko - Compatible : JPEG et PNG</p>
			</div>
		</form>
	</div>
</div>
<!-- Google Map API -->
<script async defer src="https://maps.googleapis.com/maps/api/js?key=&callback=initMap&libraries=&v=weekly"defer></script>

<script  src="https://code.jquery.com/jquery-3.5.1.js"  integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc="  crossorigin="anonymous"></script>
<script type="text/javascript" src="../../module_js/mapsBis.js"></script>
<script type="text/javascript" src="../../module_js/findcoord.js"></script>