<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
					<?php eval($plxAdmin->plxPlugins->callHook('AdminUsersTop')) # Hook Plugins ?>
						<div class="table-responsive">
							<table class="table table-striped">
								<thead class="thead-dark">
									<tr>
										<th>Ajouter</th>
										<th><?php echo L_PROFIL_USER ?></th>
										<th><?php echo L_PROFIL_LOGIN ?></th>
										<th><?php echo L_PROFIL_PASSWORD ?></th>
										<th><?php echo L_PROFIL_MAIL ?></th>
										<th><?php echo L_PROFIL ?></th>
										<th><?php echo L_CONFIG_USERS_ACTIVE ?></th>
										<th><?php echo L_CONFIG_USERS_ACTION ?></th>
									</tr>
								</thead>
								<tbody>
									<?php
									# Initialisation de l'ordre
									$num = 0;
									if($plxAdmin->aUsers) {
										foreach($plxAdmin->aUsers as $_userid => $_user)	{
											if (!$_user['delete']) {
												if($_userid=='001') {
													plxUtils::printInput($_userid.'_profil', $_user['profil'], 'hidden');
													plxUtils::printInput($_userid.'_active', $_user['active'], 'hidden');
												} else {
												}
											}
										}
										# On récupère le dernier identifiant
										$a = array_keys($plxAdmin->aUsers);
										rsort($a);
									} else {
										$a['0'] = 0;
									}
									$new_userid = str_pad($a['0']+1, 3, "0", STR_PAD_LEFT);
									?>
									<tr class="new">
										<td><?php echo L_CONFIG_USERS_NEW; ?></td>
										<td>
										<?php
											echo '<input type="hidden" name="userNum[]" value="'.$new_userid.'" />';
											plxUtils::printInput($new_userid.'_newuser', 'true', 'hidden');
											plxUtils::printInput($new_userid.'_name', '', 'text', '');
											plxUtils::printInput($new_userid.'_infos', '', 'hidden');
											echo '</td><td>';
											plxUtils::printInput($new_userid.'_login', '', 'text', '');
											echo '</td><td>';
											plxUtils::printInput($new_userid.'_password', '', 'password', '', false, '', '', 'onkeyup="pwdStrength(this.id)"');
											echo '</td><td>';
											plxUtils::printInput($new_userid.'_email', '', 'email', '');
											echo '</td><td>';
											plxUtils::printSelect($new_userid.'_profil', $aProfils, PROFIL_WRITER);
											echo '</td><td>';
											plxUtils::printSelect($new_userid.'_active', array('1'=>L_YES,'0'=>L_NO), '1');
											echo '</td>';
										?>
										<td>
                                    <div class="p-2">
                                        <?php echo plxToken::getTokenPostMethod() ?>
                                        <input class="btn btn-primary" type="submit" name="update" value="<?php echo L_CONFIG_USERS_UPDATE ?>" />
                                    </td>
									</tr>
								</tbody>
							</table>
						</div>		
					</div>
				</div>
			</div>