<?php if(!defined('PLX_ROOT')) exit; ?>
<?php
if(isset($_GET["del"]) AND $_GET["del"]=="install") {
	if(@unlink(PLX_ROOT.'install.php'))
		plxMsg::Info(L_DELETE_SUCCESSFUL);
	else
		plxMsg::Error(L_DELETE_FILE_ERR.' install.php');
	header("Location: index.php");
	exit;
}
?>
<!DOCTYPE html>
<html lang="<?php echo $plxAdmin->aConf['default_lang'] ?>">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo plxUtils::strCheck($plxAdmin->aConf['title']) ?> <?php echo L_ADMIN ?></title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="robots" content="all,follow">
		<!-- Bootstrap -->
		<link rel="stylesheet" href="<?php echo PLX_CORE ?>admin/theme/declic/css/bootstrap.min.css">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="<?php echo PLX_CORE ?>admin/theme/declic/font-awesome/css/font-awesome.min.css">
		<!-- Google fonts -->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
		<!-- Scrollbar-->
		<link rel="stylesheet" href="<?php echo PLX_CORE ?>admin/theme/declic/css/jquery.mCustomScrollbar.css">
		<!-- theme -->
		<link rel="stylesheet" href="<?php echo PLX_CORE ?>admin/theme/declic/css/declic.css">
		<link rel="stylesheet" href="<?php echo PLX_CORE ?>admin/theme/declic/css/api_dj_admin.css">
		<!-- Custom Back Office css -->
		<link rel="stylesheet" href="<?php echo PLX_CORE ?>admin/theme/custome.css">
		<!-- Favicon -->
		<link rel="shortcut icon" href="<?php echo PLX_CORE ?>admin/theme/declic/img/favicon.png">
		<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

		<?php
		if(file_exists(PLX_ROOT.$plxAdmin->aConf['racine_plugins'].'admin.css'))
		echo '<link rel="stylesheet" type="text/css" href="'.PLX_ROOT.$plxAdmin->aConf['racine_plugins'].'admin.css" media="screen" />'."\n";
		?>
		<!-- Hack pour IE --><!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
		<script src="<?php echo PLX_CORE ?>lib/functions.js?ver=<?php echo PLX_VERSION ?>"></script>
		<script src="<?php echo PLX_CORE ?>lib/visual.js?ver=<?php echo PLX_VERSION ?>"></script>
		<script src="<?php echo PLX_CORE ?>lib/mediasManager.js?ver=<?php echo PLX_VERSION ?>"></script>
		<script defer src="<?php echo PLX_CORE ?>lib/multifiles.js?ver=<?php echo PLX_VERSION ?>"></script>
		<?php eval($plxAdmin->plxPlugins->callHook('AdminTopEndHead')) ?>
	</head>
	<body>
		<!-- Sidebar Global -->
		<nav class="side-navbar">
			<div class="side-navbar-wrapper">
				<!-- Sidebar En tête -->
				<div class="sidenav-header d-flex align-items-center justify-content-center">
					<!-- Info Utilisateur -->
					<div class="sidenav-header-inner text-center"><img src="<?php echo PLX_CORE ?>admin/theme/declic/img/logofw.png" alt="person" class="img-fluid rounded-circle">
						<h2 class="h5"><?php echo plxUtils::strCheck($plxAdmin->aUsers[$_SESSION['user']]['name']) ?></h2>
						<span>
							<?php if($_SESSION['profil']==PROFIL_ADMIN) echo L_PROFIL_ADMIN;
							elseif($_SESSION['profil']==PROFIL_MANAGER) echo L_PROFIL_MANAGER;
							elseif($_SESSION['profil']==PROFIL_MODERATOR) echo L_PROFIL_MODERATOR;
							elseif($_SESSION['profil']==PROFIL_EDITOR) echo L_PROFIL_EDITOR;
							else echo L_PROFIL_WRITER; ?>
						</span>
					</div>
					<!-- Logo declic pour la reduction du menu -->
					<div class="sidenav-header-logo"><a href="<?php echo PLX_CORE ?>admin" class="brand-small text-center"> <strong>D</strong><strong class="text-primary">J</strong></a></div>
				</div>
				<!-- Sidebar Menu -->
				<div class="main-menu">
				<?php
				if($_SESSION['profil']==PROFIL_ADMIN){				
				?>
					<ul id="side-main-menu" class="side-menu list-unstyled">
						<li class="nav-item"><a href="<?php echo PLX_CORE ?>admin/index.php"> <i class="fas fa-tachometer-alt"></i>Tableau de bord</a></li>
						<li class="nav-item">
							<a href="<?php echo PLX_CORE ?>admin/api_gt_filtres.php"> <i class="fas fa-map-marked-alt"></i>Gestion des filtres</a>
						</li>
						<li class="nav-item">
							<a href="<?php echo PLX_CORE ?>admin/api_datamaps.php"> <i class="fas fa-map-marked-alt"></i>Structures</a>
						</li>
						<li class="nav-item"><a href="<?php echo PLX_CORE ?>admin/parametres_base.php"> <i class="fas fa-cogs"></i>Paramètres</a></li>
						<li class="nav-item"><a href="<?php echo PLX_CORE ?>admin/parametres_users.php"> <i class="fa fa-users"></i>Utilisateurs</a></li>
						<li class="nav-item"><a href="<?php echo PLX_CORE ?>admin/parametres_carto.php"> <i class="fa fa-users"></i>Parametre Maps</a></li>
						<!--<li class="nav-item"><a href="<?php echo PLX_CORE ?>admin/parametres_affichage.php"> <i class="fa fa-users"></i>Affichage</a></li>
						<li class="nav-item"><a href="<?php echo PLX_CORE ?>admin/parametres_themes.php"> <i class="fa fa-users"></i>Thème</a></li>
						<li class="nav-item"><a href="<?php echo PLX_CORE ?>admin/parametres_avances.php"> <i class="fa fa-users"></i>Parametres avancés</a></li>-->
						<?php
						}
						elseif($_SESSION['profil']==PROFIL_MODERATOR){
						?>
						<ul id="side-main-menu" class="side-menu list-unstyled">
							<li class="nav-item"><a href="<?php echo PLX_CORE ?>admin/index.php"> <i class="fas fa-tachometer-alt"></i>Dashboard</a></li>

							<?php
						}

						$menus = array();
						# récuperation des menus admin pour les plugins
						foreach($plxAdmin->plxPlugins->aPlugins as $plugName => $plugInstance) {
						if($plugInstance AND is_file(PLX_PLUGINS.$plugName.'/admin.php')) {
						if($plxAdmin->checkProfil($plugInstance->getAdminProfil(),false)) {
						if($plugInstance->adminMenu) {
						$menu = plxUtils::formatMenu(plxUtils::strCheck($plugInstance->adminMenu['title']), PLX_CORE.'admin/plugin.php?p='.$plugName, plxUtils::strCheck($plugInstance->adminMenu['caption']));
						if($plugInstance->adminMenu['position']!='')
						array_splice($menus, ($plugInstance->adminMenu['position']-1), 0, $menu);
						else
						$menus[] = $menu;
						} else {
						$menus[] = plxUtils::formatMenu(plxUtils::strCheck($plugInstance->getInfo('title')), PLX_CORE.'admin/plugin.php?p='.$plugName, plxUtils::strCheck($plugInstance->getInfo('title')));
						}
						}
						}
						}

						# Hook Plugins
						eval($plxAdmin->plxPlugins->callHook('AdminTopMenus'));
						echo implode('', $menus);
						?>
					</ul>
				</div>
			</div>
		</nav>
		<div class="page">
			<!-- Top Barre -->
			<header class="header">
				<nav class="navbar">
					<div class="container-fluid">
						<div class="navbar-holder d-flex align-items-center justify-content-between">
							<div class="navbar-header"><a id="toggle-btn" href="#" class="menu-btn"><i class="fas fa-bars"></i></a></div>
							<ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
								<!-- Déconnexion -->
								<li class="nav-item">
									<a class="btn btn-primary" href="<?php echo PLX_CORE ?>admin/auth.php?d=1" class="nav-link logout">
										<span class="d-none d-sm-inline-block">Déconnexion</span>
										<i class="fas fa-sign-out-alt"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</nav>
			</header>
<?php
if(is_file(PLX_ROOT.'install.php')) echo '<p class="alert red">'.L_WARNING_INSTALLATION_FILE.'</p>';
plxMsg::Display();
?>
<?php eval($plxAdmin->plxPlugins->callHook('AdminTopBottom')) ?>
