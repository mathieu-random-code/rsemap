	<?php if(!defined('PLX_ROOT')) exit; ?>
      </section>
      <footer class="main-footer">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-6">
              <p>RSE PACA &copy; 2020</p>
            </div>
            <div class="col-sm-6 text-right">
              <p>Créer par <a href="" class="external">Résine Média</a></p>
            </div>
          </div>
        </div>
      </footer>
    </div>
	<?php eval($plxAdmin->plxPlugins->callHook('AdminFootEndBody')) ?>
    <!-- JavaScript files-->
    <script src="<?php echo PLX_CORE ?>admin/theme/declic/js/jquery.min.js"></script>
    <script src="<?php echo PLX_CORE ?>admin/theme/declic/js/bootstrap.min.js"></script>
    <script src="<?php echo PLX_CORE ?>admin/theme/declic/js/jquery.cookie.js"> </script>
    <script src="<?php echo PLX_CORE ?>admin/theme/declic/js/jquery.validate.min.js"></script>
    <script src="<?php echo PLX_CORE ?>admin/theme/declic/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <!-- Main File-->
    <script src="<?php echo PLX_CORE ?>admin/theme/declic/js/front.js"></script>
	<script>
		var url = window.location;
		$('ul#side-main-menu a[href="'+ url +'"]').parent().addClass('active');
		$('ul#side-main-menu a').filter(function() {
			return this.href == url;
		}).parent().addClass('active');
	</script>
	<script>
		setMsg();
		mediasManager.construct({
			windowName : "<?php echo L_MEDIAS_TITLE ?>",
			racine:	"<?php echo plxUtils::getRacine() ?>",
			urlManager: "core/admin/medias.php"
		});
	</script>
  </body>
</html>