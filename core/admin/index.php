<?php

/**
 * Listing des articles
 *
 * @package PLX
 * @author	Stephane F et Florent MONTHEL
 **/

include(dirname(__FILE__).'/prepend.php');

# Control du token du formulaire
plxToken::validateFormToken($_POST);

# Hook Plugins
eval($plxAdmin->plxPlugins->callHook('AdminIndexPrepend'));

# Récuperation de l'id de l'utilisateur
$userId = ($_SESSION['profil'] < PROFIL_WRITER ? '[0-9]{3}' : $_SESSION['user']);

# Récuperation des paramètres
if(!empty($_GET['sel']) AND in_array($_GET['sel'], array('all','published', 'draft','mod'))) {
	$_SESSION['sel_get']=plxUtils::nullbyteRemove($_GET['sel']);
	$_SESSION['sel_cat']='';
}
else
	$_SESSION['sel_get']=(isset($_SESSION['sel_get']) AND !empty($_SESSION['sel_get']))?$_SESSION['sel_get']:'all';

if(!empty($_POST['sel_cat']))
	if(isset($_SESSION['sel_cat']) AND $_SESSION['sel_cat']==$_POST['sel_cat']) # annulation du filtre
		$_SESSION['sel_cat']='all';
	else # prise en compte du filtre
		$_SESSION['sel_cat']=$_POST['sel_cat'];
else
	$_SESSION['sel_cat']=(isset($_SESSION['sel_cat']) AND !empty($_SESSION['sel_cat']))?$_SESSION['sel_cat']:'all';

# On inclut le header
include(dirname(__FILE__).'/top.php');
?>

<?php eval($plxAdmin->plxPlugins->callHook('AdminIndexTop')) # Hook Plugins ?>
<div class="breadcrumb-holder">
		<div class="container-fluid">
			<ul class="breadcrumb">
				<li class="breadcrumb-item"><a href="<?php echo PLX_CORE ?>admin/index.php">Dasboard</a></li>
			</ul>
		</div>
	</div>
	<section>
		<div class="container-fluid">
			<!-- Page Header-->
			<header> 
				<h1 class="h3 display">Gestion des statistiques</h1>
			</header>
			<div class="row">
      <!--
			<div class="col-12 col-sm-6 col-md-6 col-xl-3 grid-margin stretch-card">
              <div class="card">
                <div class="card-body"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
                  <h4 class="card-title">Stats Utilisateur</h4>
                  <p class="text-small">+5.27% depuis le mois dernier</p>
                  <div class="d-flex justify-content-between align-items-center">
                    <h2 class="text-light font-weight-bold">30<span class="text-muted text-extra-small"> / par mois</span></h2>
                    <div class="text-danger font-weight-bold d-flex justify-content-between align-items-center">
                        <i class="mdi mdi-chevron-down mdi-24px"></i> <span class=" text-extra-small">40.8%</span>
                    </div>
                  </div>
                  <canvas id="customer" height="68" style="display: block; width: 342px; height: 68px;" width="342" class="chartjs-render-monitor"></canvas>
                </div>
              </div>
            </div>
			<div class="col-12 col-sm-6 col-md-6 col-xl-3 grid-margin stretch-card">
              <div class="card">
                <div class="card-body"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
                  <h4 class="card-title">Langue le plus utilisé</h4>
                  <p class="text-small">+5.27% de français depuis le mois derniers</p>
                  <div class="d-flex justify-content-between align-items-center">
                    <h2 class="text-light font-weight-bold">52<span class="text-muted text-extra-small"> / par mois</span></h2>
                    <div class="text-danger font-weight-bold d-flex justify-content-between align-items-center">
                        <i class="mdi mdi-chevron-down mdi-24px"></i> <span class=" text-extra-small">80.8%</span>
                    </div>
                  </div>
                  <canvas id="customer" height="68" style="display: block; width: 342px; height: 68px;" width="342" class="chartjs-render-monitor"></canvas>
                </div>
              </div>
            </div>

			</div>
    -->
		</div>
	</section>
<?php
# Hook Plugins
eval($plxAdmin->plxPlugins->callHook('AdminIndexFoot'));
# On inclut le footer
include(dirname(__FILE__).'/foot.php');
?>