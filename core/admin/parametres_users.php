<?php
/**
 * Edition des utilisateurs
 *
 * @package PLX
 * @author	Stephane F.
 **/

include(dirname(__FILE__).'/prepend.php');

# Control du token du formulaire
plxToken::validateFormToken($_POST);

# Control de l'accès à la page en fonction du profil de l'utilisateur connecté
$plxAdmin->checkProfil(PROFIL_ADMIN);

//lien de la page
$linkpage = "parametres_users.php";

# Edition des utilisateurs
if (!empty($_POST)) {
	$plxAdmin->editUsers($_POST);
	header('Location: parametres_users.php');
	exit;
}

# Tableau des profils
$aProfils = array(
	PROFIL_ADMIN => L_PROFIL_ADMIN,
	PROFIL_MANAGER => L_PROFIL_MANAGER,
	PROFIL_MODERATOR => L_PROFIL_MODERATOR,
	PROFIL_EDITOR => L_PROFIL_EDITOR,
	PROFIL_WRITER => L_PROFIL_WRITER
);

# On inclut le header
include(dirname(__FILE__).'/top.php');
?>
<form action="parametres_users.php" method="post" id="form_users">
	<!-- Breadcrumb-->
	<div class="breadcrumb-holder">
		<div class="container-fluid">
			<ul class="breadcrumb">
				<li class="breadcrumb-item"><a href="<?php echo PLX_CORE ?>admin/index.php">Dasboard</a></li>
				<li class="breadcrumb-item">Paramètres</li>
				<li class="breadcrumb-item active">Utilisateurs</li>
			</ul>
		</div>
	</div>
	<section>
	  <div class="container-fluid">
		<!-- Page Header-->
		<header> 
			<h1 class="h3 display"><?php echo L_CONFIG_USERS_TITLE; ?></h1>
		<button type="button" class="btn btn-primary" onclick="window.location.href='<?php echo $linkpage; ?>?create_users'">
			<i class="fa fa-map-marker"></i> Ajouter un utilisateur
		</button>
		</header>
		<?php
		// Création d'un marqueur de Structure
			if (isset($_GET['create_users']))
			{
				include('parametres_create_users.php');
			}
			?>
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
					<?php eval($plxAdmin->plxPlugins->callHook('AdminUsersTop')) # Hook Plugins ?>
						<div class="table-responsive">
							<table class="table table-striped">
								<thead class="thead-dark">
									<tr>
										<th>
										  <input id="checkbox" type="checkbox" class="form-control-custom" onclick="checkAll(this.form, 'idUser[]')">
										  <label for="checkbox">&nbsp;</label>
										</th>
										<th><?php echo L_ID ?></th>
										<th><?php echo L_PROFIL_USER ?></th>
										<th><?php echo L_PROFIL_LOGIN ?></th>
										<th><?php echo L_PROFIL_PASSWORD ?></th>
										<th><?php echo L_PROFIL_MAIL ?></th>
										<th><?php echo L_PROFIL ?></th>
										<th><?php echo L_CONFIG_USERS_ACTIVE ?></th>
										<th><?php echo L_CONFIG_USERS_ACTION ?></th>
									</tr>
								</thead>
								<tbody>
									<?php
									# Initialisation de l'ordre
									$num = 0;
									if($plxAdmin->aUsers) {
										foreach($plxAdmin->aUsers as $_userid => $_user)	{
											if (!$_user['delete']) {
												echo '<tr>';
												echo '<td><input id="checkbox_'.$_userid.'" type="checkbox" class="form-control-custom" name="idUser[]" value="'.$_userid.'" /><label for="checkbox_'.$_userid.'">&nbsp;</label></td>';
												echo '<td>'.$_userid.'</td><td>';
												plxUtils::printInput($_userid.'_name', plxUtils::strCheck($_user['name']), 'text', '');
												echo '</td><td>';
												plxUtils::printInput($_userid.'_login', plxUtils::strCheck($_user['login']), 'text', '');
												echo '</td><td>';
												plxUtils::printInput($_userid.'_password', '', 'password', '', false, '', '', 'onkeyup="pwdStrength(this.id)"');
												echo '</td><td>';
												plxUtils::printInput($_userid.'_email', plxUtils::strCheck($_user['email']), 'email', '');
												echo '</td><td>';
												if($_userid=='001') {
													plxUtils::printInput($_userid.'_profil', $_user['profil'], 'hidden');
													plxUtils::printInput($_userid.'_active', $_user['active'], 'hidden');
													plxUtils::printSelect($_userid.'__profil', $aProfils, $_user['profil'], true, 'readonly');
													echo '</td><td>';
													plxUtils::printSelect($_userid.'__active', array('1'=>L_YES,'0'=>L_NO), $_user['active'], true, 'readonly');
												} else {
													plxUtils::printSelect($_userid.'_profil', $aProfils, $_user['profil']);
													echo '</td><td>';
													plxUtils::printSelect($_userid.'_active', array('1'=>L_YES,'0'=>L_NO), $_user['active']);
												}
												echo '</td>';
												echo '<td><a class="btn btn-primary" href="user.php?p='.$_userid.'"><i class="fas fa-edit"></i> Éditer</a></td>';
												echo '</tr>';
											}
										}
										# On récupère le dernier identifiant
										$a = array_keys($plxAdmin->aUsers);
										rsort($a);
									} else {
										$a['0'] = 0;
									}
									$new_userid = str_pad($a['0']+1, 3, "0", STR_PAD_LEFT);
									?>
								</tbody>
							</table>
						</div>
						<div class="d-flex justify-content-center">
						  <div class="p-2">
							<?php echo plxToken::getTokenPostMethod() ?>
							<input class="btn btn-primary" type="submit" name="update" value="<?php echo L_CONFIG_USERS_UPDATE ?>" />
						  </div>
						  <div class="p-2">
							<?php plxUtils::printSelect('selection', array( '' => L_FOR_SELECTION, 'delete' => L_DELETE), '', false, 'no-margin form-control', 'id_selection') ?>
						  </div>
						  <div class="p-2">
							<input class="btn btn-danger" type="submit" name="submit" value="<?php echo L_OK ?>" onclick="return confirmAction(this.form, 'id_selection', 'delete', 'idUser[]', '<?php echo L_CONFIRM_DELETE ?>')" />
						  </div>
						</div>					
					</div>
				</div>
			</div>
		</div>
	</section>
</form>

<?php

# Hook Plugins
eval($plxAdmin->plxPlugins->callHook('AdminUsersFoot'));
# On inclut le footer
include(dirname(__FILE__).'/foot.php');
?>