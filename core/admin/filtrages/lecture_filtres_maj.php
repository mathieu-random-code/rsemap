<?php

/**
 * Requette SQL DataMaps
 *
 * @package PLX
 * @author	Stephane F.
 **/

 if(!defined('PLX_ROOT')) exit;
 # Control de l'accès à la page en fonction du profil de l'utilisateur connecté
 $plxAdmin->checkProfil(PROFIL_ADMIN, PROFIL_MODERATOR);

//service de tri et lecture de la base
if (isset($_POST['id_maj']))
{
  $Bdd = Database::connect();
  $item = $Bdd->prepare('SELECT * FROM rse_gestion_filtres WHERE id = :id_maj ORDER BY id DESC');
  $item->execute(array('id_maj' => $_POST["id_maj"]));
}
elseif(isset($_SESSION['id']))
{
  $Bdd = Database::connect();
  $item = $Bdd->prepare('SELECT * FROM rse_gestion_filtres WHERE id = :id_maj ORDER BY id DESC');
  $item->execute(array('id_maj' => $_SESSION['id']));
  unset($_SESSION['id']);
}
