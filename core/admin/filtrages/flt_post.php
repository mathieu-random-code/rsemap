<?php

# Control de l'accès à la page en fonction du profil de l'utilisateur connecté
$plxAdmin->checkProfil(PROFIL_ADMIN, PROFIL_MODERATOR);

//INSERT
function InsertSQL() {
  $Bdd = Database::connect();
  $update = $Bdd->prepare('INSERT INTO rse_gestion_filtres (cat_filtres, nom_filtres,pin_value, maj_datetime)
                                      VALUES( :cat_filtres, :nom_filtres,:pin_value, NOW())');
  $update->execute(array(
    'cat_filtres' => $_POST['cat_filtres'],
    'pin_value' => $_POST['pin_value'],
    'nom_filtres' => $_POST['nom_filtres']
  ));
  $update->closecursor();
  $Bdd = Database::disconnect();
}
//UPDATE



function UpdateSQL() {

  $PostNew = $_POST['pin_value'];
  $PostOld = $_POST['pin_value_old'];

  //Mise à jour la base FIltre 
  $Bdd = Database::connect();
  $update = $Bdd->prepare('UPDATE rse_gestion_filtres
                            SET   cat_filtres = :cat_filtres, nom_filtres = :nom_filtres,pin_value = :pin_value, maj_datetime = NOW() WHERE id = :id');
  $update->execute(array(
    'cat_filtres' => $_POST['cat_filtres'],
    'pin_value' => $_POST['pin_value'],
    'nom_filtres' => $_POST['nom_filtres'],
    'id' => $_POST['id_maj_post']
  ));

  $update->closecursor();
  $Bdd = Database::disconnect();


  //Mise à jour des marker qui sont déjà dans la déjà base 
  // Modification des anciennes categorie Dispositif et Structure
  // Partie dispositif
  $Bdd = Database::connect();
  //$updateFiltre = $Bdd->prepare("UPDATE rse_datamaps SET ".$tableColonnePin ." = :pin_value_new WHERE ".$tableColonnePin ." = :pin_value_old AND ".$tableColonneFiltre ." =  :filtres_value");

  if ($_POST['cat_filtres'] == "ChampPerso1") {

    $updateFiltre = $Bdd->prepare("UPDATE rse_datamaps SET pin_icon = :pin_value_new WHERE pin_icon = :pin_value_old AND cat_ChampPerso1 =  :filtres_value");
 
  }elseif($_POST['cat_filtres'] == "ChampPerso2"){
  
    $updateFiltre = $Bdd->prepare("UPDATE rse_datamaps SET pin_url = :pin_value_new WHERE pin_url = :pin_value_old AND cat_ChampPerso2 =  :filtres_value");
  
  }

  $updateFiltre->execute(array(
  'pin_value_new' => $PostNew,
  'pin_value_old' => $PostOld,
  'filtres_value' => $_POST['nom_filtres']
  ));
  $updateFiltre->closecursor();
  $Bdd = Database::disconnect();


}
//ERASE
function EraseSQL()
{
  $Bdd = Database::connect();
  $delete_item = $Bdd->prepare('DELETE FROM rse_gestion_filtres WHERE id = :id_delete');
  $delete_item->execute(array('id_delete' => $_POST["id_del_flt"]));
  $delete_item->closecursor();
  $Bdd = Database::disconnect();
}

if(isset($_POST["create_page"]))
{
  InsertSQL();
}
elseif(isset($_POST['id_maj_post']))
{
  UpdateSQL();
}
elseif(isset($_POST['id_del_flt']))
{
  EraseSQL();
}
