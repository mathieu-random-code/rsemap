<?php
//Selection de la page 

$Page = ""; // Telle écrie dans la BDD
$TitrePage = ""; // Titre de la page
$LinkPage = ""; // renvoie la variable get sur l'ensemble des pages
$ImgScreen = ""; // Une vue de la page 
if (isset($_GET['home_page']))
{
    $Page = "Home Page";
    $TitrePage = "Page d'accueil";
    $LinkPage = "home_page";
    $ImgScreen = "";
}
elseif (isset($_GET['page_mon_compte']))
{
    $Page = "Page Mon compte";
    $TitrePage = "Page Mon compte";
    $LinkPage = "page_mon_compte";
    $ImgScreen = "";
}
elseif (isset($_GET['page_edit_infos']))
{
    $Page = "Page Éditer mes informations";
    $TitrePage = "Page Éditer mes informations";
    $LinkPage = "page_edit_infos";
    $ImgScreen = "";
}
elseif (isset($_GET['page_bus']))
{
    $Page = "Page Bus";
    $TitrePage = "Page Bus";
    $LinkPage = "page_bus";
    $ImgScreen = "";
}