<?php

if(!defined('PLX_ROOT')) exit;
# Control de l'accès à la page en fonction du profil de l'utilisateur connecté
$plxAdmin->checkProfil(PROFIL_ADMIN, PROFIL_MODERATOR);

  if (isset($_POST['id_maj_post']))
  {
      $Bdd = Database::connect();
      $update = $Bdd->prepare('UPDATE translate_data
                                SET   contenu = :contenu,
                                      commentaire = :commentaire,
                                      trsl_datetime = NOW() WHERE id = :id');
      $update->execute(array(
        'contenu' => $_POST['contenu'],
        'commentaire' => $_POST['commentaire'],
        'id' => $_POST['id_maj_post']
      ));

      $update->closecursor();
      $Bdd = Database::disconnect();
  }
