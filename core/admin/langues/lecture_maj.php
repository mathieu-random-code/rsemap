<?php
/**
 * Requette SQL DataMaps
 *
 * @package PLX
 * @author	Stephane F.
 **/

 if(!defined('PLX_ROOT')) exit;
 # Control de l'accès à la page en fonction du profil de l'utilisateur connecté
 $plxAdmin->checkProfil(PROFIL_ADMIN, PROFIL_MODERATOR);
//service de tri et lecture de la base

  if (isset($_POST['id_maj']))
  {
    $Bdd = Database::connect();
    $item = $Bdd->prepare('SELECT * FROM datamaps WHERE id = :id_maj');
    $item->execute(array('id_maj' => $_POST["id_maj"]));
  }
