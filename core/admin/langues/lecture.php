<?php
/**
 * Requette SQL translate_data
 *
 * @package PLX
 * @author	Stephane F.
 **/

 if(!defined('PLX_ROOT')) exit;
 # Control de l'accès à la page en fonction du profil de l'utilisateur connecté
 $plxAdmin->checkProfil(PROFIL_ADMIN, PROFIL_MODERATOR);

if(isset($Page))
{
    $Bdd = Database::connect();
    $item = $Bdd->prepare("SELECT * FROM translate_data WHERE emplacement = :pages ORDER BY id");
    $item->execute(array('pages' => $Page));
}


