<div class="windows_float">
		<div class="card-body">
			<form action="<?php echo $linkpage; ?>" method="post">
			<!-- Catégorie -->
			<div class="form-group row">
			  <label class="col-sm-4 form-control-label">Usagers</label>
			  <div class="col-sm-8">
					<select name="categorie">
						<option selected value="<?php echo stripslashes($donnees['categorie']); ?>"><?php echo stripslashes($donnees['categorie']); ?></option>
						<option value="Demandeurs d'asile">Demandeurs d'asile</option>
						<option value="Réfugiés">Réfugiés</option>
						<option value="Tous">Tous</option>
					</select>
			  </div>
			</div>
			<!-- Structure -->
			<div class="form-group row">
			  <label class="col-sm-4 form-control-label">Structure</label>
			  <div class="col-sm-8">
			  <input value="<?php echo stripslashes($donnees['structure']); ?>" type="text" name="structure" id="id_meta_description" class="form-control" size="50" maxlength="255">
			  </div>
			</div>
			<!-- Description -->
			<div class="form-group row">
			  <label class="col-sm-4 form-control-label">Description</label>
			  <div class="col-sm-8">
			  <input value="<?php echo stripslashes($donnees['description']); ?>" type="text" name="description" id="id_meta_description" class="form-control" size="50" maxlength="255">
			  </div>
			</div>
			<!-- Adresse -->
			<div class="form-group row">
			  <label class="col-sm-4 form-control-label">Adresse</label>
			  <div class="col-sm-8">
			  <input value="<?php echo stripslashes($donnees['adresse']); ?>" type="text" name="adresse" id="id_meta_description" class="form-control" size="50" maxlength="255">
			  </div>
			</div>
			<!-- Ville -->
			<div class="form-group row">
			  <label class="col-sm-4 form-control-label">Ville</label>
			  <div class="col-sm-8">
			  <input value="<?php echo stripslashes($donnees['ville']); ?>" type="text" name="ville" id="id_meta_description" class="form-control" size="50" maxlength="255">
			  </div>
			</div>
			<!-- Code Postal -->
			<div class="form-group row">
			  <label class="col-sm-4 form-control-label">CP</label>
			  <div class="col-sm-8">
			  <input value="<?php echo stripslashes($donnees['code_postal']); ?>" name="code_postal" id="id_meta_description" class="form-control" size="50" maxlength="255">
			  </div>
			</div>
			<!-- Cat Structure 1 -->
			<div class="form-group row">
			  <label class="col-sm-4 form-control-label">Cat 1</label>
			  <div class="col-sm-8">
					<select name="categorie_structure_1">
						<option selected value="<?php echo stripslashes($donnees['categorie_structure_1']); ?>"><?php echo stripslashes($donnees['categorie_structure_1']); ?></option>
						<option value="Tout">Tout</option>
						<option value="Papiers">Papiers</option>
						<option value="Langue">Langue</option>
						<option value="Santé">Santé</option>
						<option value="Maison">Maison</option>
					</select>
			  </div>
			</div>
			<!-- Cat Structure 2 -->
			<div class="form-group row">
			  <label class="col-sm-4 form-control-label">Cat 2</label>
			  <div class="col-sm-8">
					<select name="categorie_structure_2">
						<option selected value="<?php echo stripslashes($donnees['categorie_structure_2']); ?>"><?php echo stripslashes($donnees['categorie_structure_2']); ?></option>
						<option value="Tout">Tout</option>
						<option value="Papiers">Papiers</option>
						<option value="Langue">Langue</option>
						<option value="Santé">Santé</option>
						<option value="Maison">Maison</option>
						<option value="">null</option>
					</select>
			  </div>
			</div>
			<!-- Nom responsable -->
			<div class="form-group row">
			  <label class="col-sm-4 form-control-label">Nom responsable</label>
			  <div class="col-sm-8">
			  <input value="<?php echo stripslashes($donnees['nom_responsable']); ?>" type="text" name="nom_responsable" id="id_meta_description" class="form-control" size="50" maxlength="255">
			  </div>
			</div>
			<!-- Latitude -->
			<div class="form-group row">
			  <label class="col-sm-4 form-control-label">Latitude</label>
			  <div class="col-sm-8">
			  <input value="<?php echo stripslashes($donnees['latitude']); ?>" name="latitude" id="id_meta_description" class="form-control" size="50" maxlength="255">
			  </div>
			</div>
			<!-- Longitude -->
			<div class="form-group row">
			  <label class="col-sm-4 form-control-label">Longitude</label>
			  <div class="col-sm-8">
			  <input value="<?php echo stripslashes($donnees['longitude']); ?>" name="longitude" id="id_meta_description" class="form-control" size="50" maxlength="255">
			  </div>
			</div>
			<!-- Tel Numero -->
			<div class="form-group row">
			  <label class="col-sm-4 form-control-label">Numéro</label>
			  <div class="col-sm-8">
			  <input value="<?php echo stripslashes($donnees['tel_numero']); ?>" name="tel_numero" id="id_meta_description" class="form-control" size="50" maxlength="255">
			  </div>
			</div>
			<!-- Site web Url -->
			<div class="form-group row">
			  <label class="col-sm-4 form-control-label">Site Web</label>
			  <div class="col-sm-8">
			  <input value="<?php echo stripslashes($donnees['site_web_url']); ?>" type="url" name="site_web_url" id="id_meta_description" class="form-control" size="50" maxlength="255">
			  </div>
			</div>
			<!-- Logo URL -->
			<div class="form-group row">
			  <label class="col-sm-4 form-control-label">Logo Url</label><img class="pre_img" src="<?php echo $donnees['logo_url']; ?>"/>
			  <div class="col-sm-8">
			  <input value="<?php echo stripslashes($donnees['logo_url']); ?>" type="url" name="logo_url" id="id_meta_description" class="form-control" size="50" maxlength="255">
			  </div>
			</div>
			<!-- Adresse Mail 1 -->
			<div class="form-group row">
			  <label class="col-sm-4 form-control-label">Mail 1</label>
			  <div class="col-sm-8">
			  <input value="<?php echo stripslashes($donnees['mail_one']); ?>" name="mail_one" id="id_meta_description" class="form-control" size="50" maxlength="255">
			  </div>
			</div>
			<!-- Adresse Mail 2 -->
			<div class="form-group row">
			  <label class="col-sm-4 form-control-label">Mail 2</label>
			  <div class="col-sm-8">
			  <input value="<?php echo stripslashes($donnees['mail_two']); ?>" name="mail_two" id="id_meta_description" class="form-control" size="50" maxlength="255">
			  </div>
			</div>
			<!-- Mot clé -->
			<div class="form-group row">
			  <label class="col-sm-4 form-control-label">Mot Clef</label>
			  <div class="col-sm-8">
			  <input value="<?php echo stripslashes($donnees['mot_cle']); ?>" type="text" name="mot_cle" id="id_meta_description" class="form-control" size="50" maxlength="255">
			  </div>
			</div>
			<div class="d-flex justify-content-center">
				<div class="p-2">
					<input type="hidden" name="id_maj_post" value="<?php echo $donnees['id']; ?>"/>
					<button name="Valider" class="btn btn-primary" type="submit" value="valider">Valider</button>
					<a href="<?php echo $linkpage; ?>">Annuler</a>
				</div>
			</div>
</div>
