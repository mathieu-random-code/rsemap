<?php

/**
 * Gestion de la configuration d'un plugin
 *
 * @package PLX
 * @author	Stephane F
 **/
include(dirname(__FILE__).'/prepend.php');

# Control de l'accès à la page en fonction du profil de l'utilisateur connecté
$plxAdmin->checkProfil(PROFIL_ADMIN);

$plugin = isset($_GET['p'])?urldecode($_GET['p']):'';
$plugin = plxUtils::nullbyteRemove($plugin);

$output='';
# chargement du fichier d'administration du plugin
$filename = realpath(PLX_PLUGINS.$plugin.'/config.php');
if(is_file($filename)) {
	# si le plugin n'est pas actif, aucune instance n'a été créée, on va donc la créer, sinon on prend celle qui existe
	if(empty($plxAdmin->plxPlugins->aPlugins[$plugin]))
		$plxPlugin = $plxAdmin->plxPlugins->getInstance($plugin);
	else
		$plxPlugin = $plxAdmin->plxPlugins->aPlugins[$plugin];

	# Control des autorisation d'accès à l'écran config.php du plugin
	$plxAdmin->checkProfil($plxPlugin->getConfigProfil());
	# chargement de l'écran de paramétrage du plugin config.php
	ob_start();
	include($filename);
	$output=ob_get_clean();
}
else {
	plxMsg::Error(L_NO_ENTRY);
	header('Location: parametres_plugins.php');
	exit;
}

# On inclut le header
include(dirname(__FILE__).'/top.php');
?>
<!-- Breadcrumb-->
<div class="breadcrumb-holder">
	<div class="container-fluid">
		<ul class="breadcrumb">
			<li class="breadcrumb-item"><a href="<?php echo PLX_CORE ?>admin/index.php">Dasboard</a></li>
			<li class="breadcrumb-item"><a href="<?php echo PLX_CORE ?>admin/parametres_plugins.php">Plugins</a></li>
			<li class="breadcrumb-item active">Configuration</li>
		</ul>
	</div>
</div>
<section>
  <div class="container-fluid">
	<!-- Page Header-->
	<header> 
		<h1 class="h3 display"><a class="btn btn-primary btn-xs" href="<?php echo PLX_CORE ?>admin/parametres_plugins.php"><i class="fas fa-arrow-left"></i> Retour</a>  Configuration du plugin</h1>
	</header>
	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-body">
					<?php echo $output; ?>
				</div>
			</div>	
		</div>		
	</div>
</section>
<?php
# Hook Plugins
eval($plxAdmin->plxPlugins->callHook('AdminSettingsPluginsFoot'));
# On inclut le footer
include(dirname(__FILE__).'/foot.php');
?>