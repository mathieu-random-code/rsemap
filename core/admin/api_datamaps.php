<?php

include(dirname(__FILE__).'/prepend.php');
if(!defined('PLX_ROOT')) exit;
# Control de l'accès à la page en fonction du profil de l'utilisateur connecté
$plxAdmin->checkProfil(PROFIL_ADMIN, PROFIL_MODERATOR);
# On inclut le header
include(dirname(__FILE__).'/top.php');

// Include - Accès la BDD
require('api/Database.php');
// Include - Gestion des Uploads des fichiers
require('api/Upload.php');
//Enregistre une entrée dans la table
include('api/post_admin.php');

//lien de la page
$linkpage = "api_datamaps.php";

?>
  <!-- Breadcrumb-->
<div class="breadcrumb-holder">
	<div class="container-fluid">
		<ul class="breadcrumb">
			<li class="breadcrumb-item"><a href="<?php echo PLX_CORE ?>admin/index.php">Dasboard</a></li>
			<li class="breadcrumb-item active">Gestion des structures</li>
		</ul>
	</div>
</div>

<section>
  <div class="container-fluid">
	<!-- Page Header-->
	<header>
		<button type="button" class="btn btn-primary" onclick="window.location.href='<?php echo $linkpage; ?>?create_page'">
		<i class="fa fa-map-marker"></i> Ajouter une Structure
		</button>
	</header>
		<!-- Tableau de la liste des marqueurs -->
	  <div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-body">
					<table id="datamaps" class="table table-striped table-responsive-xl table-bordered">
						<thead class="thead-dark">
							<tr>
								<th style="width: 120px;">Date Maj</th>
								<th style="width: 80px;">Nom</th>
								<th style="width: 150px;">Adresse</th>
								<th style="width: 30px;">Url</th>
								<th style="width: 30px;">ChampPerso1</th>
								<th style="width: 30px;">ChampPerso2</th>
								<th style="width: 30px;">ChampPerso3</th>
								<th style="width: 100px;">Action</th>
							</tr>
						</thead>
							<!-- TBODY--------------------------------------------------------------------------------->
						<tbody>
							<?php
							// Simple lecture du tableau DataMaps
							include('api/lecture.php');
							while ($donnees = $item->fetch())
							{
							?>
							<tr>
								<td><?php echo date("Y-m-d H:i:s" , strtotime($donnees['maj_datetime'])); ?> </td>
								<td><?php echo stripslashes($donnees['titre']); ?> </td>
								<td><?php echo stripslashes($donnees['adresse'])." ".stripslashes($donnees['ville'])." ".stripslashes($donnees['code_postal']); ?> </td>
								<td><a href="<?php echo stripslashes($donnees['site_web']); ?>" target="_blank"><?php echo stripslashes($donnees['site_web']); ?></a> </td>
								<td><?php echo stripslashes($donnees['cat_ChampPerso1']); ?> </td>
								<td><?php echo stripslashes($donnees['cat_ChampPerso2']); ?> </td>
								<td><?php echo stripslashes($donnees['cat_ChampPerso3']); ?> </td>
								<td>
									<form action="<?php echo $linkpage; ?>" method="get">
										<input type="hidden" name="id_maj" value="<?php echo $donnees['id']; ?>"/>
										<button type="submit" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i> Éditer</button>
									</form>
									<br>
									<form action="<?php echo $linkpage; ?>" method="post">
										<input type="hidden" name="id_del" value="<?php echo $donnees['id']; ?>"/>
										<button type="submit" name="Supprimer" class="btn btn-danger btn-xs"><i class="fa fa-times"></i> Supprimer</button>
									</form>
								</td>
							</tr>
							<?php
							}
							$item->closecursor();
							$Bdd = Database::disconnect();
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	  </div>
	</div>
</section>
<?php
	// Création d'un marqueur de Structure
	if (isset($_GET['create_page']))
	{
		include('api/datamaps_create.php');
	}
	// Mise à jour d'une entrée
	if (isset($_GET['id_maj']) OR isset($_SESSION['id']))
	{
		include('api/lecture_maj.php');
		$donnees = $item->fetch();
		include('api/datamaps_maj.php');

		$item->closecursor();
		$Bdd = Database::disconnect();
	}
	elseif (isset($_GET['gestion_thematique']))
	{
		include('api/gestion_thematique.php');
	}
# Hook Plugins
eval($plxAdmin->plxPlugins->callHook('AdminUserFoot'));
# On inclut le footer
include(dirname(__FILE__).'/foot.php');
