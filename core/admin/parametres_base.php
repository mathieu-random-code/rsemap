<?php

/**
 * Edition des paramètres de base
 *
 * @package PLX
 * @author	Florent MONTHEL, Stephane F
 **/

include(dirname(__FILE__).'/prepend.php');
include(PLX_CORE.'lib/class.plx.timezones.php');

# Control du token du formulaire
plxToken::validateFormToken($_POST);

# Control de l'accès à la page en fonction du profil de l'utilisateur connecté
$plxAdmin->checkProfil(PROFIL_ADMIN);

# On édite la configuration
if(!empty($_POST)) {
	$plxAdmin->editConfiguration($plxAdmin->aConf,$_POST);
	header('Location: parametres_base.php');
	exit;
}

# On inclut le header
include(dirname(__FILE__).'/top.php');
?>
<?php eval($plxAdmin->plxPlugins->callHook('AdminSettingsBaseTop')) # Hook Plugins ?>
<div class="breadcrumb-holder">
	<div class="container-fluid">
		<ul class="breadcrumb">
			<li class="breadcrumb-item"><a href="<?php echo PLX_CORE ?>admin/index.php">Dasboard</a></li>
			<li class="breadcrumb-item">Paramètres</li>
			<li class="breadcrumb-item active">Principal</li>
		</ul>
	</div>
</div>
<section>
	<div class="container-fluid">
		<!-- Page Header-->
		<header> 
		<h1 class="h3 display"><?php echo L_CONFIG_BASE_CONFIG_TITLE ?></h1>
		</header>
		<form action="parametres_base.php" method="post" id="form_settings">
			<div class="row">
				<div class="col-lg-12">
					<div class="card">
						<div class="card-body">
							<div class="form-group row">
							  <label class="col-sm-4 form-control-label"><?php echo L_CONFIG_BASE_SITE_TITLE ?></label>
							  <div class="col-sm-8">
								<?php plxUtils::printInput('title', plxUtils::strCheck($plxAdmin->aConf['title'])); ?>
							  </div>
							</div>
							<div class="line"></div>
							<div class="form-group row">
							  <label class="col-sm-4 form-control-label"><?php echo L_CONFIG_BASE_SITE_SLOGAN ?></label>
							  <div class="col-sm-8">
								<?php plxUtils::printInput('description', plxUtils::strCheck($plxAdmin->aConf['description'])); ?>
							  </div>
							</div>
							<div class="line"></div>
							<div class="form-group row">
							  <label class="col-sm-4 form-control-label"><?php echo L_CONFIG_META_DESCRIPTION ?></label>
							  <div class="col-sm-8">
								<?php plxUtils::printInput('meta_description', plxUtils::strCheck($plxAdmin->aConf['meta_description'])); ?>
							  </div>
							</div>
							<div class="line"></div>
							<div class="form-group row">
							  <label class="col-sm-4 form-control-label"><?php echo L_CONFIG_META_KEYWORDS ?></label>
							  <div class="col-sm-8">
								<?php plxUtils::printInput('meta_keywords', plxUtils::strCheck($plxAdmin->aConf['meta_keywords'])); ?>
							  </div>
							</div>
							<div class="line"></div>
							<div class="form-group row">
							  <label class="col-sm-4 form-control-label"><?php echo L_CONFIG_BASE_DEFAULT_LANG ?></label>
							  <div class="col-sm-8">
								<?php plxUtils::printSelect('default_lang', plxUtils::getLangs(), $plxAdmin->aConf['default_lang']) ?>
							  </div>
							</div>
							<div class="line"></div>
							<div class="form-group row">
							  <label class="col-sm-4 form-control-label"><?php echo L_CONFIG_BASE_TIMEZONE ?></label>
							  <div class="col-sm-8">
								<?php plxUtils::printSelect('timezone', plxTimezones::timezones(), $plxAdmin->aConf['timezone']); ?>
							  </div>
							</div>
							<div class="line"></div>
							<div class="form-group row">
							  <label class="col-sm-4 form-control-label">Pop up Ou lien redirection ?</label>
							  <div class="col-sm-8">
								<?php plxUtils::printSelect('popup',array('pop-up'=>L_POPUP,'link'=>L_LINK), $plxAdmin->aConf['popup']) ?>
							  </div>
							</div>
							<div class="form-group row">
							  <label class="col-sm-4 form-control-label">Position du block info (si choix pop-up)</label>
							  <div class="col-sm-8">
								<?php plxUtils::printSelect('popup_pos',array('0'=>L_POPUP_TRIGTH,'1'=>L_POPUP_TLEFT), $plxAdmin->aConf['popup_pos']) ?>
							  </div>
							</div>




							<!-- <div class="form-group row">
							  <label class="col-sm-4 form-control-label"><?php echo L_CONFIG_BASE_ALLOW_COMMENTS ?></label>
							  <div class="col-sm-8">
								<?php plxUtils::printSelect('allow_com',array('1'=>L_YES,'0'=>L_NO), $plxAdmin->aConf['allow_com']); ?>
							  </div>
							</div>
							<div class="line"></div>
							<div class="form-group row">
							  <label class="col-sm-4 form-control-label"><?php echo L_CONFIG_BASE_MODERATE_COMMENTS ?></label>
							  <div class="col-sm-8">
								<?php plxUtils::printSelect('mod_com',array('1'=>L_YES,'0'=>L_NO), $plxAdmin->aConf['mod_com']); ?>
							  </div>
							</div>
							<div class="line"></div>
							<div class="form-group row">
							  <label class="col-sm-4 form-control-label"><?php echo L_CONFIG_BASE_MODERATE_ARTICLES ?></label>
							  <div class="col-sm-8">
								<?php plxUtils::printSelect('mod_art',array('1'=>L_YES,'0'=>L_NO), $plxAdmin->aConf['mod_art']); ?>
							  </div>
							</div>
							<div class="line"></div>-->
							<div class="d-flex justify-content-center">
								<div class="p-2">								
									<?php echo plxToken::getTokenPostMethod() ?>
									<button class="btn btn-primary" type="submit" value="<?php echo L_CONFIG_BASE_UPDATE ?>"><?php echo L_CONFIG_BASE_UPDATE ?></button>
								</div>
							</div>
							<?php eval($plxAdmin->plxPlugins->callHook('AdminSettingsBase')) # Hook Plugins ?>
						</diV>
					</div>
				</div>
			</div>
		</form>
	</div>
</section>

<?php
# Hook Plugins
eval($plxAdmin->plxPlugins->callHook('AdminSettingsBaseFoot'));
# On inclut le footer
include(dirname(__FILE__).'/foot.php');
?>