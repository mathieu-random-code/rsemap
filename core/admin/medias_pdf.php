<?php

include(dirname(__FILE__).'/prepend.php');
if(!defined('PLX_ROOT')) exit;
# Control de l'accès à la page en fonction du profil de l'utilisateur connecté
$plxAdmin->checkProfil(PROFIL_ADMIN, PROFIL_MODERATOR);
# On inclut le header
include(dirname(__FILE__).'/top.php');

// Include - Accès la BDD
require('api/Database.php');
// Include - Gestion des Uploads des fichiers
require('api/Upload.php');
//Enregistre une entrée dans la table
include('media/post_admin_pdf.php');

//lien de la page
$linkpage = "medias_pdf.php";

?>
  <!-- Breadcrumb-->
<div class="breadcrumb-holder">
	<div class="container-fluid">
		<ul class="breadcrumb">
			<li class="breadcrumb-item"><a href="<?php echo PLX_CORE ?>admin/index.php">Dasboard</a></li>
			<li class="breadcrumb-item active">Gestion des fichies PDF</li>
		</ul>
	</div>
</div>
<section>
  <div class="container-fluid">
	<!-- Page Header-->
	<header>
		<button type="button" class="btn btn-primary" onclick="window.location.href='<?php echo $linkpage; ?>?create_page'">
		<i class="fa fa-map-marker"></i> Ajouter une entrée
		</button>
	</header>
		<!-- Tableau de la liste des marqueurs -->
	  <div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-body">
					<table id="datamaps" class="table table-striped table-responsive-xl table-bordered">
						<thead class="thead-dark">
							<tr>
								<th style="width: 80px;">ID</th>
								<th style="width: 80px;">Titre</th>
								<th style="width: 80px;">URL</th>
								<th style="width: 100px;">Action</th>
							</tr>
						</thead>
							<!-- TBODY--------------------------------------------------------------------------------->
						<tbody>
							<?php
							// Simple lecture du tableau DataMaps
							include('media/lecture_pdf.php');
							while ($donnees = $item->fetch())
							{
							?>
							<tr>
								<td><?php echo stripslashes($donnees['id']); ?> </td>
								<td><?php echo stripslashes($donnees['titre']); ?> </td>
								<td><?php echo stripslashes($donnees['url_pdf']); ?> </td>
								<td>
									<form action="<?php echo $linkpage; ?>" method="post">
										<input type="hidden" name="id_del" value="<?php echo $donnees['id']; ?>"/>
										<button type="submit" name="Supprimer" class="btn btn-danger btn-xs"><i class="fa fa-times"></i> Supprimer</button>
									</form>
								</td>
							</tr>
							<?php
							}
							$item->closecursor();
							$Bdd = Database::disconnect();
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	  </div>
	</div>
</section>
<?php
	// Création d'un marqueur de Structure
	if (isset($_GET['create_page']))
	{
		include('media/fichiers_create_pdf.php');
	}
	// Mise à jour d'une entrée
	if (isset($_GET['id_maj']))
	{
		include('media/lecture_pdf.php');
		while ($donnees = $item->fetch())
		{
			//include('api/datamaps_maj.php');
		}
		$item->closecursor();
		$Bdd = Database::disconnect();
	}
# Hook Plugins
eval($plxAdmin->plxPlugins->callHook('AdminUserFoot'));
# On inclut le footer
include(dirname(__FILE__).'/foot.php');