<?php
include(dirname(__FILE__).'/prepend.php');

if(!defined('PLX_ROOT')) exit;
# Control de l'accès à la page en fonction du profil de l'utilisateur connecté
$plxAdmin->checkProfil(PROFIL_ADMIN, PROFIL_MODERATOR);
# On inclut le header
include(dirname(__FILE__).'/top.php');

// Include - Accès la BDD
require('api/Database.php');

//Enregistre une entrée dans la table
require('config_map/configmaps_post.php');

//lien de la page
$linkpagefiltres = "parametres_carto save.php";
?>
  <!-- Breadcrumb-->
<div class="breadcrumb-holder">
	<div class="container-fluid">
		<ul class="breadcrumb">
			<li class="breadcrumb-item"><a href="<?php echo PLX_CORE ?>admin/index.php">Dasboard</a></li>
			<li class="breadcrumb-item active">Gestion des Paramètres</li>
		</ul>
	</div>
</div>
<section>
  <div class="container-fluid">
	<!-- Page Header-->
	<header>
	</header>
		<!-- Tableau de la liste des marqueurs -->
	  <div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-body">
					<table id="datamaps" class="table table-striped table-responsive-xl table-bordered">
						<thead class="thead-dark">
							<tr>
								<th style="width: 80px;">Nom</th>
								<th style="width: 80px;">Valeur</th>
								<th style="width: 80px;">Description</th>
								<th style="width: 80px;">Action</th>
							</tr>
						</thead>
							<!-- TBODY--------------------------------------------------------------------------------->
						<tbody>
							<?php
							// Simple lecture du tableau DataMaps
							include('config_map/lecture_configmaps.php');
							while ($donnees = $item->fetch())
							{
								if (isset($_POST['id_maj']) AND $_POST['id_maj'] == $donnees['id_config']) // A modifier
								{
									?>
										<tr>
											<form action="<?php echo $linkpagefiltres; ?>" method="post">
											<?php echo $donnees['id_config'];  ?>
											<td>
												<?php echo stripslashes($donnees['name_config']); ?>
												<input type="hidden" name="name_config" value="<?php echo stripslashes($donnees['name_config']); ?>">
											</td>
											<td><input type="text" name="value_config" value="<?php echo stripslashes($donnees['value_config']); ?>"></input></td>
											<td>
												<input type="text" name="description_config" value="<?php echo stripslashes($donnees['description_config']); ?>"></input>
											</td>
											<td>
													<button type="submit" class="btn btn-primary btn-xs" name="id_maj_post" value="<?php echo $donnees['id_config']; ?>">
														<i class="fa fa-edit"></i> Valider
													</button>
													<button type="submit" class="btn btn-danger btn-xs" onclick="window.location.href='<?php echo $linkpagefiltres; ?>'">
														<i class="fa fa-times"></i> Annuler
													</button>
											</td>
										</form>
										</tr>
									<?php
								}
								else
								{
									?>
									<tr>
										<td><?php echo stripslashes($donnees['name_config']); ?> </td>
										<td><?php echo stripslashes($donnees['value_config']); ?> </td>
										<td><?php echo stripslashes($donnees['description_config']); ?> </td>
										<td>
											<form action="<?php echo $linkpagefiltres; ?>" method="post">
												<input type="hidden" name="id_maj" value="<?php echo $donnees['id_config']; ?>"/>
												<button type="submit" name="Modifer" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i> Éditer</button>
											</form>
											<br>
											<form action="<?php echo $linkpagefiltres; ?>" method="post">
												<input type="hidden" name="id_del_flt" value="<?php echo $donnees['id_config']; ?>"/>
												<button type="submit" name="del" class="btn btn-danger btn-xs"><i class="fa fa-times"></i> Supprimer</button>
											</form>
										</td>
									</tr>
									<?php
								}
							}
							$item->closecursor();
							$Bdd = Database::disconnect();
							?>
						</tbody>
					</table>

				</div>
			</div>
		</div>
	  </div>
	</div>

</section>
<?php
# Hook Plugins
eval($plxAdmin->plxPlugins->callHook('AdminUserFoot'));

# On inclut le footer
include(dirname(__FILE__).'/foot.php');
