<?php
include(dirname(__FILE__).'/prepend.php');

if(!defined('PLX_ROOT')) exit;
# Control de l'accès à la page en fonction du profil de l'utilisateur connecté
$plxAdmin->checkProfil(PROFIL_ADMIN, PROFIL_MODERATOR);
# On inclut le header
include(dirname(__FILE__).'/top.php');

// Include - Accès la BDD
require('api/Database.php');

//Enregistre une entrée dans la table
require('filtrages/flt_post.php');

//lien de la page
$linkpagefiltres = "api_gt_filtres.php";
?>
  <!-- Breadcrumb-->
<div class="breadcrumb-holder">
	<div class="container-fluid">
		<ul class="breadcrumb">
			<li class="breadcrumb-item"><a href="<?php echo PLX_CORE ?>admin/index.php">Dasboard</a></li>
			<li class="breadcrumb-item active">Gestion des filtres</li>
		</ul>
	</div>
</div>
<section>
  <div class="container-fluid">
	<!-- Page Header-->
	<header>
		<!-- Block en attente - Attention au Icon du pin sur la carte --->
		<button type="button" class="btn btn-primary" onclick="window.location.href='<?php echo $linkpagefiltres; ?>?create_sec_activite'">
			<i class="fa fa-map-marker"></i> Ajouter un ChampPerso1
		</button>
		<!-- Block en attente - Attention au pin logo de la carte --->
		<button type="button" class="btn btn-primary" onclick="window.location.href='<?php echo $linkpagefiltres; ?>?create_tstructure'">
			<i class="fa fa-map-marker"></i> Ajouter un ChampPerso2
		</button>
		<button type="button" class="btn btn-primary" onclick="window.location.href='<?php echo $linkpagefiltres; ?>?create_territoire'">
			<i class="fa fa-map-marker"></i> Ajouter un ChampPerso3
		</button>
		<style> 
			.tg td{
				border:solid 1px #000;
			}
		</style>
		<table class="tg">
			<thead>
				<tr>
					<th class="tg-0lax" style="color:red;">Attention</th>
					<th class="tg-0lax">Filtre</th>
					<th class="tg-0lax">Pin Value</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="tg-0pky">ChampPerso1</td>
					<td class="tg-0pky">Choix unique</td>
					<td class="tg-0lax">Icone dans dans le point sur la carte(charactère)</td>
				</tr>
				<tr>
					<td class="tg-0pky">ChampPerso2</td>
					<td class="tg-0pky">Choix unique</td>
					<td class="tg-0lax">Image qui représente le point sur la carte(url)</td>
				</tr>
				<tr>
					<td class="tg-0pky">ChampPerso3</td>
					<td class="tg-0pky">Choix Multiple</td>
					<td class="tg-0pky">Aucun effet</td>
				</tr>
			</tbody>
		</table>
	</header>
		<!-- Tableau de la liste des marqueurs -->
	  <div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-body">
					<table id="datamaps" class="table table-striped table-responsive-xl table-bordered">
						<thead class="thead-dark">
							<tr>
								<th style="width: 80px;">Catégorie filtre</th>
								<th style="width: 80px;">Nom du filtre</th>
								<th style="width: 80px;">Pin value</th>
								<th style="width: 80px;">Date Maj</th>
								<th style="width: 30px;">Action</th>
							</tr>
						</thead>
							<!-- TBODY--------------------------------------------------------------------------------->
						<tbody>
							<?php
							if (isset($_GET['create_sec_activite']) OR isset($_GET['create_tstructure']) OR isset($_GET['create_territoire']))
							{
								?>
									<tr>
									<form action="<?php echo $linkpagefiltres; ?>" method="post">
										<td>
											<?php
											if(isset($_GET['create_sec_activite']))
											{

												?>
													<p>Vue utilisé : ChampPerso1</p>
													<input type="hidden" name="cat_filtres" value="ChampPerso1">
												<?php
											}
											else if(isset($_GET['create_tstructure']))
											{
												?>
													<p>Vue utilisé : ChampPerso2</p>
													<input type="hidden" name="cat_filtres" value="ChampPerso2">
												<?php
											}
											else if(isset($_GET['create_territoire']))
											{
												?>
													<p>Vue utilisé : ChampPerso3</p>
													<input type="hidden" name="cat_filtres" value="ChampPerso3">
												<?php
											}
											?>
										</td>
										<td><input type="text" name="nom_filtres"></td>
										<td><input type="text" name="pin_value"></td>
										<td></td>
										<td>
												<button type="submit" name="create_page" class="btn btn-primary btn-xs">
													<i class="fa fa-edit"></i> Valider
												</button>
												<button type="submit" name="Supprimer" class="btn btn-danger btn-xs" onclick="window.location.href='<?php echo $linkpagefiltres; ?>'">
													<i class="fa fa-times"></i> Annuler
												</button>
										</td>
									</form>
									</tr>
							<?php
							}
							// Simple lecture du tableau DataMaps
							include('filtrages/lecture_filtres.php');
							while ($donnees = $item->fetch())
							{
								if (isset($_POST['id_maj']) AND $_POST['id_maj'] == $donnees['id']) // A modifier
								{
									?>
										<tr>
											<form action="<?php echo $linkpagefiltres; ?>" method="post">
											<td>
												<?php echo stripslashes($donnees['cat_filtres']); ?>
												<input type="hidden" name="cat_filtres" value="<?php echo stripslashes($donnees['cat_filtres']); ?>">
											</td>
											<td><input type="text" name="nom_filtres" value="<?php echo stripslashes($donnees['nom_filtres']); ?>"></input></td>
											<td>
												<input type="text" name="pin_value" value="<?php echo stripslashes($donnees['pin_value']); ?>"></input>
												<input type="hidden" name="pin_value_old" value="<?php echo stripslashes($donnees['pin_value']); ?>"></input>
											</td>
											<td><?php echo strftime("ID : %s - %d/%m/%Y à %H:%M " , strtotime($donnees['maj_datetime'])); ?> </td>
											<td>
													<button type="submit" class="btn btn-primary btn-xs" name="id_maj_post" value="<?php echo $donnees['id']; ?>">
														<i class="fa fa-edit"></i> Valider
													</button>
													<button type="submit" class="btn btn-danger btn-xs" onclick="window.location.href='<?php echo $linkpagefiltres; ?>'">
														<i class="fa fa-times"></i> Annuler
													</button>
											</td>
										</form>
										</tr>
									<?php
								}
								else
								{
									?>
									<tr>
										<td><?php echo stripslashes($donnees['cat_filtres']); ?> </td>
										<td><?php echo stripslashes($donnees['nom_filtres']); ?> </td>
										<td><?php echo stripslashes($donnees['pin_value']); ?> </td>
										<td><?php echo strftime("ID : %s - %d/%m/%Y à %H:%M " , strtotime($donnees['maj_datetime'])); ?> </td>
										<td>
											<form action="<?php echo $linkpagefiltres; ?>" method="post">
												<input type="hidden" name="id_maj" value="<?php echo $donnees['id']; ?>"/>
												<button type="submit" name="Modifer" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i> Éditer</button>
											</form>
											<br>
											<form action="<?php echo $linkpagefiltres; ?>" method="post">
												<input type="hidden" name="id_del_flt" value="<?php echo $donnees['id']; ?>"/>
												<button type="submit" name="del" class="btn btn-danger btn-xs"><i class="fa fa-times"></i> Supprimer</button>
											</form>
										</td>
									</tr>
									<?php
								}
							}
							$item->closecursor();
							$Bdd = Database::disconnect();
							?>
						</tbody>
					</table>

	<span style="color: #fff;">Pour changer/ajouter les icons prioriser FontAwesome et séléctioner juste le caractère sinon lire le fichier readme sur le serveur.</span>
				</div>
			</div>
		</div>
	  </div>
	</div>

</section>
<?php
# Hook Plugins
eval($plxAdmin->plxPlugins->callHook('AdminUserFoot'));

# On inclut le footer
include(dirname(__FILE__).'/foot.php');
