<?php

/**
 * Page d'authentification
 *
 * @package PLX
 * @author	Stephane F et Florent MONTHEL
 **/

# Variable pour retrouver la page d'authentification
define('PLX_AUTHPAGE', true);

include(dirname(__FILE__).'/prepend.php');

# Control du token du formulaire
plxToken::validateFormToken($_POST);

# Protection anti brute force
$maxlogin['counter'] = 3; # nombre de tentative de connexion autorisé dans la limite de temps autorisé
$maxlogin['timer'] = 3 * 60; # temps d'attente limite si nombre de tentative de connexion atteint (en minutes)

# Hook Plugins
eval($plxAdmin->plxPlugins->callHook('AdminAuthPrepend'));

# Initialisation variable erreur
$error = '';
$msg = '';

if(isset($_SESSION['maxtry'])) {
	if( intval($_SESSION['maxtry']['counter']) >= $maxlogin['counter'] AND (time() < $_SESSION['maxtry']['timer'] + $maxlogin['timer']) ) {
		# écriture dans les logs du dépassement des 3 tentatives successives de connexion
		@error_log("PluXml: Max login failed. IP : ".plxUtils::getIp());
		# message à affiche sur le mire de connexion
		$msg = sprintf(L_ERR_MAXLOGIN, ($maxlogin['timer']/60));
		$error = 'error';
	}
	if( time() > ($_SESSION['maxtry']['timer'] + $maxlogin['timer']) ) {
		# on réinitialise le control brute force quand le temps d'attente limite est atteint
		$_SESSION['maxtry']['counter'] = 0;
		$_SESSION['maxtry']['timer'] = time();
	}
} else {
	# initialisation de la variable qui compte les tentatives de connexion
	$_SESSION['maxtry']['counter'] = 0;
	$_SESSION['maxtry']['timer'] = time();
}

# Control et filtrage du parametre $_GET['p']
$redirect=$plxAdmin->aConf['racine'].'core/admin/';
if(!empty($_GET['p']) AND $error=='') {

	# on incremente la variable de session qui compte les tentatives de connexion
	$_SESSION['maxtry']['counter']++;

	$racine = parse_url($plxAdmin->aConf['racine']);
	$get_p = parse_url(urldecode($_GET['p']));
	$error = (!$get_p OR (isset($get_p['host']) AND $racine['host']!=$get_p['host']));
	if(!$error AND !empty($get_p['path']) AND file_exists(PLX_ROOT.'core/admin/'.basename($get_p['path']))) {
		# filtrage des parametres de l'url
		$query='';
		if(isset($get_p['query'])) {
			$query=strtok($get_p['query'],'=');
			$query=($query[0]!='d'?'?'.$get_p['query']:'');
		}
		# url de redirection
		$redirect=$get_p['path'].$query;
	}
}

# Déconnexion
if(!empty($_GET['d']) AND $_GET['d']==1) {

	$_SESSION = array();
	session_destroy();
	header('Location: auth.php');
	exit;

	$formtoken = $_SESSION['formtoken']; # sauvegarde du token du formulaire
	$_SESSION = array();
	session_destroy();
	session_start();
	$msg = L_LOGOUT_SUCCESSFUL;
	$_GET['p']='';
	$_SESSION['formtoken']=$formtoken; # restauration du token du formulaire
	unset($formtoken);
}

# Authentification
if(!empty($_POST['login']) AND !empty($_POST['password']) AND $error=='') {

	$connected = false;
	foreach($plxAdmin->aUsers as $userid => $user) {
		if ($_POST['login']==$user['login'] AND sha1($user['salt'].md5($_POST['password']))===$user['password'] AND $user['active'] AND !$user['delete']) {
			$_SESSION['user'] = $userid;
			$_SESSION['profil'] = $user['profil'];
			$_SESSION['hash'] = plxUtils::charAleatoire(10);
			$_SESSION['domain'] = $session_domain;
			# on définit $_SESSION['admin_lang'] pour stocker la langue à utiliser la 1ere fois dans le chargement des plugins une fois connecté à l'admin
			# ordre des traitements:
			# page administration : chargement fichier prepend.php
			# => creation instance plxAdmin : chargement des plugins, chargement des prefs utilisateurs
			# => chargement des langues en fonction du profil de l'utilisateur connecté déterminé précédemment
			$_SESSION['admin_lang'] = $user['lang'];
			$connected = true;
			break;
		}
	}
	if($connected) {
		unset($_SESSION['maxtry']);
		header('Location: '.htmlentities($redirect));
		exit;
	} else {
		$msg = L_ERR_WRONG_PASSWORD;
		$error = 'error';
	}
}
plxUtils::cleanHeaders();
?>
<!DOCTYPE html>
<html lang="<?php echo $plxAdmin->aConf['default_lang'] ?>">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo plxUtils::strCheck($plxAdmin->aConf['title']) ?> <?php echo L_ADMIN ?></title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="robots" content="all,follow">
		<!-- Bootstrap -->
		<link rel="stylesheet" href="<?php echo PLX_CORE ?>admin/theme/declic/css/bootstrap.min.css">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="<?php echo PLX_CORE ?>admin/theme/declic/font-awesome/css/font-awesome.min.css">
		<!-- Google fonts -->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
		<!-- Scrollbar-->
		<link rel="stylesheet" href="<?php echo PLX_CORE ?>admin/theme/declic/css/jquery.mCustomScrollbar.css">
		<!-- theme -->
		<link rel="stylesheet" href="<?php echo PLX_CORE ?>admin/theme/declic/css/declic.css">
		<!-- Favicon -->
		<link rel="shortcut icon" href="<?php echo PLX_CORE ?>admin/theme/declic/img/favicon.png">
		<?php
		if(file_exists(PLX_ROOT.$plxAdmin->aConf['racine_plugins'].'admin.css'))
		echo '<link rel="stylesheet" type="text/css" href="'.PLX_ROOT.$plxAdmin->aConf['racine_plugins'].'admin.css" media="screen" />'."\n";
		?>
		<!-- Hack pour IE --><!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
		<script src="<?php echo PLX_CORE ?>lib/functions.js?ver=<?php echo PLX_VERSION ?>"></script>
		<script src="<?php echo PLX_CORE ?>lib/visual.js?ver=<?php echo PLX_VERSION ?>"></script>
		<script src="<?php echo PLX_CORE ?>lib/mediasManager.js?ver=<?php echo PLX_VERSION ?>"></script>
		<script defer src="<?php echo PLX_CORE ?>lib/multifiles.js?ver=<?php echo PLX_VERSION ?>"></script>
		<?php eval($plxAdmin->plxPlugins->callHook('AdminTopEndHead')) ?>
	</head>
	<body id="auth">
		<div class="pages login-page">
		  <div class="container">
			<div class="form-outer text-center d-flex align-items-center">
			  <div class="form-inner">
				<div class="logo text-uppercase"><span>BackOffice</span><strong class="text-primary">Carto</strong></div>
				<?php eval($plxAdmin->plxPlugins->callHook('AdminAuthTop')) ?>
				<form class="text-left form-validate" action="auth.php<?php echo !empty($redirect)?'?p='.plxUtils::strCheck(urlencode($redirect)):'' ?>" method="post" id="form_auth">
				  <?php echo plxToken::getTokenPostMethod() ?>
				  <div class="form-group-material">
					<input id="login-username" type="text" name="login" required data-msg="S'il vous plaît entrez votre nom d'utilisateur" class="input-material">
					<label for="login-username" class="label-material">Nom d'utilisateur</label>
				  </div>
				  <div class="form-group-material">
					<input id="login-password" type="password" name="password" required data-msg="S'il vous plait entrez votre mot de passe" class="input-material">
					<label for="login-password" class="label-material">Mot de Passe</label>
				  </div>
				  <?php eval($plxAdmin->plxPlugins->callHook('AdminAuth')) ?>
				  <div class="form-group text-center">
					<input class="btn btn-primary" type="submit" value="Connexion" />
				  </div>
				</form>
			  </div>
			</div>
		  </div>
		</div>
	<?php eval($plxAdmin->plxPlugins->callHook('AdminAuthEndBody')) ?>
		<!-- JavaScript files-->
		<script src="<?php echo PLX_CORE ?>admin/theme/declic/js/jquery.min.js"></script>
		<script src="<?php echo PLX_CORE ?>admin/theme/declic/js/bootstrap.min.js"></script>
		<script src="<?php echo PLX_CORE ?>admin/theme/declic/js/jquery.cookie.js"> </script>
		<script src="<?php echo PLX_CORE ?>admin/theme/declic/js/jquery.validate.min.js"></script>
		<script src="<?php echo PLX_CORE ?>admin/theme/declic/js/jquery.mCustomScrollbar.concat.min.js"></script>
		<!-- Main File-->
		<script src="<?php echo PLX_CORE ?>admin/theme/declic/js/front.js"></script>
		<script src="<?php echo PLX_CORE ?>admin/theme/declic/js/apps.js"></script>
	</body>
</html>