<?php

/**
 * Edition des options d'un utilisateur
 *
 * @package PLX
 * @author	Stephane F.
 **/

include(dirname(__FILE__).'/prepend.php');

# Control du token du formulaire
plxToken::validateFormToken($_POST);

# Hook Plugins
eval($plxAdmin->plxPlugins->callHook('AdminUserPrepend'));

# Control de l'accès à la page en fonction du profil de l'utilisateur connecté
$plxAdmin->checkProfil(PROFIL_ADMIN);

# On édite la page statique
if(!empty($_POST) AND isset($plxAdmin->aUsers[ $_POST['id'] ])) {
	$plxAdmin->editUser($_POST);
	header('Location: user.php?p='.$_POST['id']);
	exit;
}
elseif(!empty($_GET['p'])) { # On vérifie l'existence de l'utilisateur
	$id = plxUtils::strCheck(plxUtils::nullbyteRemove($_GET['p']));
	if(!isset($plxAdmin->aUsers[ $id ])) {
		plxMsg::Error(L_USER_UNKNOWN);
		header('Location: parametres_users.php');
		exit;
	}
} else { # Sinon, on redirige
	header('Location: parametres_users.php');
	exit;
}

# On inclut le header
include(dirname(__FILE__).'/top.php');
?>
<?php eval($plxAdmin->plxPlugins->callHook('AdminUserTop')) # Hook Plugins ?>
<form action="user.php" method="post" id="form_user">
	<div class="breadcrumb-holder">
		<div class="container-fluid">
			<ul class="breadcrumb">
				<li class="breadcrumb-item"><a href="<?php echo PLX_CORE ?>admin/index.php">Dasboard</a></li>
				<li class="breadcrumb-item"><a href="<?php echo PLX_CORE ?>admin/parametres_users.php">Utilisateurs</a></li>
				<li class="breadcrumb-item active">Édition</li>
			</ul>
		</div>
	</div>
	<section>
		<div class="container-fluid">
			<!-- Page Header-->
			<header> 
				<h1 class="h3 display"><a class="btn btn-primary btn-xs" href="<?php echo PLX_CORE ?>admin/parametres_users.php"><i class="fas fa-arrow-left"></i> Retour</a>  <?php echo L_USER_PAGE_TITLE ?> - <?php echo plxUtils::strCheck($plxAdmin->aUsers[$id]['name']); ?></h1>
			</header>
			<div class="row">
				<div class="col-lg-12">
					<div class="card">
						<div class="card-body">
							<div class="form-group row">
							  <label class="col-sm-2 form-control-label"><?php echo L_USER_LANG ?></label>
							  <div class="col-sm-10">
								<?php plxUtils::printInput('id', $id, 'hidden');?>
								<?php plxUtils::printSelect('lang', plxUtils::getLangs(), $plxAdmin->aUsers[$id]['lang']) ?>
							  </div>
							</div>
							<div class="line"></div>
							<div class="form-group row">
							  <label class="col-sm-2 form-control-label"><?php echo L_USER_MAIL ?></label>
							  <div class="col-sm-10">
								<?php plxUtils::printInput('email', plxUtils::strCheck($plxAdmin->aUsers[$id]['email']), 'email', '30-255') ?>
							  </div>
							</div>
							<div class="line"></div>
							<div class="form-group row">
							  <label class="col-sm-2 form-control-label"><?php echo L_USER_INFOS ?></label>
							  <div class="col-sm-10">
								<?php plxUtils::printArea('content',plxUtils::strCheck($plxAdmin->aUsers[$id]['infos']),95,8,false,'form-control') ?>
							  </div>
							</div>
							<div class="line"></div>
							<div class="d-flex justify-content-center">
								<div class="p-2">								
									<?php echo plxToken::getTokenPostMethod() ?>
									<button class="btn btn-primary" type="submit" value="<?php echo L_USER_UPDATE ?>"><?php echo L_USER_UPDATE ?></button>
								</div>
							</div>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php eval($plxAdmin->plxPlugins->callHook('AdminUser')) ?>
</form>
<?php
# Hook Plugins
eval($plxAdmin->plxPlugins->callHook('AdminUserFoot'));
# On inclut le footer
include(dirname(__FILE__).'/foot.php');
?>