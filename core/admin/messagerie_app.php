<?php
include(dirname(__FILE__).'/prepend.php');

if(!defined('PLX_ROOT')) exit;
# Control de l'accès à la page en fonction du profil de l'utilisateur connecté
$plxAdmin->checkProfil(PROFIL_ADMIN, PROFIL_MODERATOR);
# On inclut le header
include(dirname(__FILE__).'/top.php');

// Include - Accès la BDD
require('api/Database.php');
//Enregistre une entrée dans la table
include('messagerie/post_usagers.php');

//lien de la page
$linkpageMessagerie = "messagerie_app.php";
?>
  <!-- Breadcrumb-->
<div class="breadcrumb-holder">
	<div class="container-fluid">
		<ul class="breadcrumb">
			<li class="breadcrumb-item"><a href="<?= PLX_CORE ?>admin/index.php">Dasboard</a></li>
			<li class="breadcrumb-item active">Messagerie</li>
		</ul>
	</div>
</div>
<section>
  <div class="container-fluid">
  	<!-- Page Header-->
	<header>

	</header>
		<!-- Tableau de la liste des marqueurs -->
	  <div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-body">
					<table id="datamaps" class="table table-striped table-responsive-xl table-bordered">
							<thead class="thead-dark">
								<tr>
									<th>Statut</th>
									<th>ID</th>
									<th>Date</th>
									<th>SN</th>
									<th>Avatar</th>
									<th>Pseudo</th>
									<th>Question de l'usagers</th>
									<th>Action</th>
								</tr>
							</thead>
							  <!-- TBODY--------------------------------------------------------------------------------->
							<tbody>
								<?php
								// Simple lecture du tableau translate_data
								include('messagerie/lecture.php'); // Configuer
								while ($donnees = $item->fetch())
								{
									if($donnees['sn_users'] != null)
									{
									//Requette profil 
									$Bdd02 = Database::connect();
									$imgP = $Bdd02->prepare('SELECT * FROM profils WHERE sn = :sn');
									$imgP->execute(array('sn' => $donnees['sn_users']));
									$data = $imgP->fetch();
								?>
								<tr>
									<td><?php switch(stripslashes($donnees['statut'])){
											case 0: 
												echo "<span class\"statut\" 
												style=\" display: inline-block;
												background-color: #0d599c;
												padding: 10px;
												border-radius: 10px; \">Non lu</span>"; 
											break;
											case 1: 
												echo "<span class\"statut\" 
												style=\" display: inline-block;
												background-color: #f4a62a;
												padding: 10px;
												border-radius: 10px; \">Transmit</span>"; 
											break;
											case 2: 
												echo "<span class\"statut\" 
												style=\" display: inline-block;
												background-color: #15a084;
												padding: 10px;
												border-radius: 10px; \">Reçu</span>"; 
											break;

										}; ?></td>
									<td><?= stripslashes($donnees['id']); ?></td>
									<td><?= stripslashes($donnees['datetime_messages']); ?></td>
									<td><?= stripslashes($donnees['sn_users']); ?></td>
									<td><?php 
										if($data["img_pseudo"] != "")
										{ 
											echo '<img class="img-fluid img-thumbnail" src="https://ddcs-jeunes.tk/images/pseudo/' . $data['img_pseudo'] . '"/>';
										}
										else 
										{ 
											echo '<p>Vide</p>'; 
										} ?>
									</td>
									<td><?= stripslashes($donnees['pseudo']);?></td>
									<td><?= stripslashes($donnees['messages_users']); ?></td>
									<?php 
									}
									?>
									<td>
										<form action="<?= $linkpageMessagerie; ?>" method="post">
											<input type="hidden" name="id_maj" value="<?= $donnees['id']; ?>"/>
											<button type="submit" name="Modifer" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i> Éditer</button>
										</form>
									</td>
								</tr>
								<div class="modal fade" id="_<?= stripslashes($donnees['id']); ?>" tabindex="-1" role="dialog" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title" id="exampleModal3Label"><?= stripslashes($donnees['emplacement']); ?></h5>
											</div>
											<div class="modal-body">
												<div class="embed-responsive">
												<img class="embed-responsive-item" src="<?php echo $donnees['views']; ?>"/>
												</div>
											</div>
										</div>
										</div>
									</div>
								</div>
								<?php
									$imgP->closecursor();
									$Bdd02 = Database::disconnect();
									$item->closecursor();
									$Bdd = Database::disconnect();
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	  </div>
	</div>
</section>
<?php
	// Mise à jour d'une entrée
	if (isset($_POST['id_maj']))
	{
		include('messagerie/lecture_maj.php');
		while ($donnees = $item->fetch())
		{
			include('messagerie/usagers_maj.php');
		}
		$item->closecursor();
		$Bdd = Database::disconnect();
	}
# Hook Plugins
eval($plxAdmin->plxPlugins->callHook('AdminUserFoot'));
# On inclut le footer
include(dirname(__FILE__).'/foot.php');
