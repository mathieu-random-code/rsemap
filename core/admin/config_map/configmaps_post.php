<?php

# Control de l'accès à la page en fonction du profil de l'utilisateur connecté
$plxAdmin->checkProfil(PROFIL_ADMIN, PROFIL_MODERATOR);

//INSERT
function InsertSQL() {
  $Bdd = Database::connect();
  $update = $Bdd->prepare('INSERT INTO rse_configmaps (name_config, value_config,description_config)
                                      VALUES( :name_config, :value_config,:description_config)');
  $update->execute(array(
    'name_config' => $_POST['name_config'],
    'description_config' => $_POST['description_config'],
    'value_config' => $_POST['value_config']
  ));
  $update->closecursor();
  $Bdd = Database::disconnect();
}
//UPDATE



function UpdateSQL() {

  //Mise à jour la base Filtre 
  $Bdd = Database::connect();
  $update = $Bdd->prepare('UPDATE rse_configmaps SET name_config = :name_config, value_config = :value_config,description_config = :description_config WHERE id_config = :id_config');
  $update->execute(array(
    'name_config' => $_POST['name_config'],
    'description_config' => $_POST['description_config'],
    'value_config' => $_POST['value_config'],
    'id_config' => $_POST['id_maj_post']
  ));
  //echo $_POST['name_config'];

  $update->closecursor();
  $Bdd = Database::disconnect();

}
//ERASE
function EraseSQL()
{
  $Bdd = Database::connect();
  $delete_item = $Bdd->prepare('DELETE FROM rse_configmaps WHERE id_config = :id_delete');
  $delete_item->execute(array('id_delete' => $_POST["id_del_flt"]));
  $delete_item->closecursor();
  $Bdd = Database::disconnect();
}

if(isset($_POST["create_page"]))
{
  //InsertSQL();
}
elseif(isset($_POST['id_maj_post']))
{
  UpdateSQL();
}
elseif(isset($_POST['id_del_flt']))
{
  EraseSQL();
}
