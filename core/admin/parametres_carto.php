<?php
include(dirname(__FILE__).'/prepend.php');

if(!defined('PLX_ROOT')) exit;
# Control de l'accès à la page en fonction du profil de l'utilisateur connecté
$plxAdmin->checkProfil(PROFIL_ADMIN, PROFIL_MODERATOR);
# On inclut le header
include(dirname(__FILE__).'/top.php');

// Include - Accès la BDD
require('api/Database.php');

//Enregistre une entrée dans la table
require('config_map/configmaps_post.php');

//lien de la page
$linkpagefiltres = "parametres_carto.php";
?>
  <!-- Breadcrumb-->
<div class="breadcrumb-holder">
	<div class="container-fluid">
		<ul class="breadcrumb">
			<li class="breadcrumb-item"><a href="<?php echo PLX_CORE ?>admin/index.php">Dasboard</a></li>
			<li class="breadcrumb-item active">Gestion des Paramètres</li>
		</ul>
	</div>
</div>
<section>
  <div class="container-fluid">
	<!-- Page Header-->
	<header>
	</header>
		<!-- Tableau de la liste des marqueurs -->
	  <div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-body">
					<table id="datamaps" class="table table-striped table-responsive-xl table-bordered">
						<thead class="thead-dark">
							<tr>
								<th style="width: 80px;">Nom</th>
								<th style="width: 80px;">Valeur</th>
								<th style="width: 80px;">Description</th>
								<th style="width: 80px;">Action</th>
							</tr>
						</thead>
							<!-- TBODY--------------------------------------------------------------------------------->
						<tbody>
						<?php 
							include('config_map/lecture_configmaps.php');
							$donneesArray = [];
							while ($donnees = $item->fetch())
							{
								$donneesArray[$donnees['name_config']] = [$donnees['id_config'],$donnees['name_config'],$donnees['value_config'],$donnees['description_config']]; 
							}
							//print_r($donneesArray);
						?>
							<tr>
							<!-- Controle pour le activer/desactiver la pop-up -->
								<form action="<?php echo $linkpagefiltres; ?>" method="post">
								<td>
									<?php echo $donneesArray['popup'][1];?>
									<input type="hidden" name="name_config" value="<?php echo $donneesArray['popup'][1];?>">
								</td>
								<td>
									<input style="display: inline-block;width: fit-content;" type="radio" name="value_config" id="pop-up" value="pop-up" checked ></input>
									<label for="pop-up" >Pop-up</label>
									</br>
									<input style="display: inline-block;width: fit-content;" type="radio" name="value_config" id="lien" value="lien"<?php if($donneesArray['popup'][2]=="lien"){echo "checked";}?>></input>
									<label for="lien" >Lien</label>
								</td>
								<td>
									<?php echo $donneesArray['popup'][3];?>
									<input type="hidden" name="description_config" value="<?php echo $donneesArray['popup'][3];?>">

								</td>
								<td>
									<button type="submit" class="btn btn-primary btn-xs" name="id_maj_post" value="<?php echo $donneesArray['popup'][0];?>">
										<i class="fa fa-edit"></i> Valider
									</button>
									<!--<button type="submit" class="btn btn-danger btn-xs" onclick="window.location.href='<?php echo $linkpagefiltres; ?>'">
										<i class="fa fa-times"></i> Annuler
									</button>-->
								</td>
							</form>
							</tr>
							<?php if($donneesArray['popup'][2]=="pop-up"){ ?>
								<tr>
								<!-- Controle pour la position de la pop-up -->
									<form action="<?php echo $linkpagefiltres; ?>" method="post">
										<td>
											<?php echo $donneesArray['popup_pos'][1];?>
											<input type="hidden" name="name_config" value="<?php echo $donneesArray['popup_pos'][1];?>">
										</td>
										<td>
											<input style="display: inline-block;width: fit-content;" type="radio" name="value_config" id="popup_pos0" value="0" checked ></input>
											<label for="popup_pos0" >Droite</label>
											</br>
											<input style="display: inline-block;width: fit-content;" type="radio" name="value_config" id="popup_pos1" value="1"<?php if($donneesArray['popup_pos'][2]=="1"){echo "checked";}?>></input>
											<label for="popup_pos1" >Gauche</label>
										</td>
										<td>
											<?php echo $donneesArray['popup_pos'][3];?>
											<input type="hidden" name="description_config" value="<?php echo $donneesArray['popup_pos'][3];?>">

										</td>
										<td>
											<button type="submit" class="btn btn-primary btn-xs" name="id_maj_post" value="<?php echo $donneesArray['popup_pos'][0];?>">
												<i class="fa fa-edit"></i> Valider
											</button>
											<!--<button type="submit" class="btn btn-danger btn-xs" onclick="window.location.href='<?php echo $linkpagefiltres; ?>'">
												<i class="fa fa-times"></i> Annuler
											</button>-->
										</td>
									</form>
								</tr>
							<?php } ?>
							<tr>
							<!-- Controle pour afficher ou non la bar de recherche -->
								<form action="<?php echo $linkpagefiltres; ?>" id="searchBar" method="post">
									<td>
										<?php echo $donneesArray['search'][1];?>
										<input type="hidden" name="name_config" value="<?php echo $donneesArray['search'][1];?>">
									</td>
									<td>
										<input style="display: inline-block;width: fit-content;" type="radio" name="value_config" id="search0" value="0" checked ></input>
										<label for="search0" >Cacher la bar</label>
										</br><input style="display: inline-block;width: fit-content;" type="radio" name="value_config" id="search1" value="1" <?php if($donneesArray['search'][2]=="1"){echo "checked";}?> ></input>
										<label for="search1" >Droite</label>
										</br>
										<input style="display: inline-block;width: fit-content;" type="radio" name="value_config" id="search2" value="2"<?php if($donneesArray['search'][2]=="2"){echo "checked";}?>></input>
										<label for="search2" >Gauche</label>
										</br>
										<input style="display: inline-block;width: fit-content;" type="radio" name="value_config" id="search3" value="3" <?php if($donneesArray['search'][2]=="3"){echo "checked";}?> ></input>
										<label for="search3" >En bas</label>
									</td>
									<td>
										<?php echo $donneesArray['search'][3];?>
										<input type="hidden" name="description_config" value="<?php echo $donneesArray['search'][3];?>">
									</td>
									<td>
										<button type="submit" class="btn btn-primary btn-xs" name="id_maj_post" value="<?php echo $donneesArray['search'][0];?>">
											<i class="fa fa-edit"></i> Valider
										</button>
										<!--<button type="submit" class="btn btn-danger btn-xs" onclick="window.location.href='<?php echo $linkpagefiltres; ?>'">
											<i class="fa fa-times"></i> Annuler
										</button>-->
									</td>
								</form>
							</tr>
							<tr>
							<!-- Controle de la clé api -->
								<form action="<?php echo $linkpagefiltres; ?>" id="map_api_key" method="post">
									<td>
										<?php echo $donneesArray['map_api_key'][1];?>
										<input type="hidden" name="name_config" value="<?php echo $donneesArray['map_api_key'][1];?>">
									</td>
									<td>
										<input style="display: inline-block;width: 100%;" type="text" name="value_config"  value="<?php echo $donneesArray['map_api_key'][2];?>" checked ></input>
									</td>
									<td>
										<?php echo $donneesArray['map_api_key'][3];?>
										<input type="hidden" name="description_config" value="<?php echo $donneesArray['map_api_key'][3];?>">

									</td>
									<td>
										<button type="submit" class="btn btn-primary btn-xs" name="id_maj_post" value="<?php echo $donneesArray['map_api_key'][0];?>">
											<i class="fa fa-edit"></i> Valider
										</button>
										<!--<button type="submit" class="btn btn-danger btn-xs" onclick="window.location.href='<?php echo $linkpagefiltres; ?>'">
											<i class="fa fa-times"></i> Annuler
										</button>-->
									</td>
								</form>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	  </div>
	</div>

</section>
<?php
# Hook Plugins
eval($plxAdmin->plxPlugins->callHook('AdminUserFoot'));

# On inclut le footer
include(dirname(__FILE__).'/foot.php');
