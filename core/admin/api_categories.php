<?php
include(dirname(__FILE__).'/prepend.php');

if(!defined('PLX_ROOT')) exit;
# Control de l'accès à la page en fonction du profil de l'utilisateur connecté
$plxAdmin->checkProfil(PROFIL_ADMIN, PROFIL_MODERATOR);
# On inclut le header
include(dirname(__FILE__).'/top.php');

// Include - Accès la BDD
require('api/Database.php');
// Include - Gestion des Uploads des fichiers
require('api/Upload.php');
//Enregistre une entrée dans la table
include('api/post_admin_cat.php');

//lien de la page
$linkpageCatThematique = "api_categories.php";
?>
  <!-- Breadcrumb-->
<div class="breadcrumb-holder">
	<div class="container-fluid">
		<ul class="breadcrumb">
			<li class="breadcrumb-item"><a href="<?php echo PLX_CORE ?>admin/index.php">Dasboard</a></li>
			<li class="breadcrumb-item active">Gestion des sous-catégories</li>
		</ul>
	</div>
</div>
<section>
  <div class="container-fluid">
	<!-- Page Header-->
	<header>
		<button type="button" class="btn btn-primary" onclick="window.location.href='<?php echo $linkpageCatThematique; ?>?create_page'">
			<i class="fa fa-map-marker"></i> Ajouter une sous-catégorie
		</button>
	</header>
		<!-- Tableau de la liste des marqueurs -->
	  <div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-body">
					<table id="datamaps" class="table table-striped table-responsive-xl table-bordered">
						<thead class="thead-dark">
							<tr>
								<th style="width: 80px;">Catégories</th>
								<th style="width: 10px;">ID Ordres</th>
								<th style="width: 150px;">Sous catégorie (Structure)</th>
								<th style="width: 150px;">Descriptions</th>
								<th style="width: 80px;">Images</th>
								<th style="width: 80px;">Date Maj</th>
								<th style="width: 30px;">Action</th>
							</tr>
						</thead>
							<!-- TBODY--------------------------------------------------------------------------------->
						<tbody>

							<?php
							// Simple lecture du tableau DataMaps
							include('api/lecture_categories.php');
							while ($donnees = $item->fetch())
							{
							?>
							<tr>
								<td><?php echo stripslashes($donnees['categorie']); ?> </td>
								<td><?php echo stripslashes($donnees['order_id']); ?> </td>
								<td><?php echo stripslashes($donnees['sous_categorie']); ?> </td>
								<td><?php echo stripslashes($donnees['descriptions']); ?> </td>
								<td><img style="background-color: transparent;" class="img-fluid img-thumbnail" src="<?php echo $donnees['img_url']; ?>"/></td>
								<td><?php echo strftime("ID : %s - %d/%m/%Y à %H:%M " , strtotime($donnees['maj_datetime'])); ?> </td>
								<td>
									<form action="<?php echo $linkpageCatThematique; ?>" method="post">
										<input type="hidden" name="id_maj" value="<?php echo $donnees['id']; ?>"/>
										<button type="submit" name="Modifer" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i> Éditer</button>
									</form>
									<br>
									<form action="<?php echo $linkpageCatThematique; ?>" method="post">
										<input type="hidden" name="id_del_cat" value="<?php echo $donnees['id']; ?>"/>
										<button type="submit" name="del" class="btn btn-danger btn-xs"><i class="fa fa-times"></i> Supprimer</button>
									</form>
								</td>
							</tr>
							<?php
							}
							$item->closecursor();
							$Bdd = Database::disconnect();
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	  </div>
	</div>
</section>

<?php

	// Création d'un marqueur de Structure

	if (isset($_GET['create_page']))
	{
		include('api/categories_create.php');
	}
	// Mise à jour d'une entrée
	elseif (isset($_POST['id_maj']) OR isset($_SESSION['id']))
	{
		include('api/lecture_categories_maj.php');
		while ($donnees = $item->fetch())
		{
			include('api/categories_maj.php');
		}
		$item->closecursor();
		$Bdd = Database::disconnect();
	}

# Hook Plugins
eval($plxAdmin->plxPlugins->callHook('AdminUserFoot'));

# On inclut le footer
include(dirname(__FILE__).'/foot.php');
