<?php
include(dirname(__FILE__).'/prepend.php');
if(!defined('PLX_ROOT')) exit;
# Control de l'accès à la page en fonction du profil de l'utilisateur connecté
$plxAdmin->checkProfil(PROFIL_ADMIN, PROFIL_MODERATOR);
# On inclut le header
include(dirname(__FILE__).'/top.php');
// Include - Accès la BDD
require('api/Database.php');

?>
<div class="form-group row">
<label class="col-sm-4 form-control-label">Catégorie</label>
    <div class="tab col-sm-8">
        <input type="radio" class="tablinks" name="categorie" value="Je m'oriente, je me forme" onclick="openCat(event, 'orientation')"/> Je m'oriente, je me forme<br/>
        <input type="radio" class="tablinks" name="categorie" value="Je cherche un stage, un emploi" onclick="openCat(event, 'emploi')"> Je cherche un stage, un emploi<br/>
        <input type="radio" class="tablinks" name="categorie" value="Je cherche un logement" onclick="openCat(event, 'logement')"> Je cherche un logement<br/>
        <input type="radio" class="tablinks" name="categorie" value="Je me déplace en France, ou à l'étranger" onclick="openCat(event, 'mobilite')"> Je me déplace en France, ou à l'étranger<br/>
        <input type="radio" class="tablinks" name="categorie" value="Je m'informe sur ma santé" onclick="openCat(event, 'sante')"> Je m'informe sur ma santé<br/>
        <input type="radio" class="tablinks" name="categorie" value="J'organise ma vie sociale, je m'engage" onclick="openCat(event, 'sociale')"> J'organise ma vie sociale, je m'engage<br/>
    </div>
</div>
<div id="orientation" class="tabcontent form-group row">
    <label class="col-sm-4 form-control-label">Sous catégorie</label>
    <div class="col-sm-8">
        <select class="form-control" name="thematique">
            <?php         
                $selectTable = Database::connect();
                $itemTable = $selectTable->query('SELECT * FROM declic_api_thematique WHERE categorie = "Je m\'oriente, je me forme"');
                $selected = "";
                while ($selecter = $itemTable->fetch()) {
                if ($selecter['thematique'] != $selected) 
                {
                    ?>
                        <option value="<?php echo $selecter['thematique']; ?>"><?php echo " " . $selecter['thematique']; ?></option>
                    <?php
                }
                $selected = $selecter['thematique'];
                }
                $itemTable->closecursor();
                $selectTable = Database::disconnect(); 
            ?>
        </select>
    </div>
</div>
<div id="emploi" class="tabcontent form-group row">
    <label class="col-sm-4 form-control-label">Sous catégorie</label>
    <div class="col-sm-8">
        <select class="form-control" name="thematique">
            <?php         
                $selectTable = Database::connect();
                $itemTable = $selectTable->query('SELECT * FROM declic_api_thematique WHERE categorie = "Je cherche un stage, un emploi"');
                $selected = "";
                while ($selecter = $itemTable->fetch()) {
                if ($selecter['thematique'] != $selected) 
                {
                    ?>
                        <option value="<?php echo $selecter['thematique']; ?>"><?php echo $selecter['thematique']; ?></option>
                    <?php
                }
                $selected = $selecter['thematique'];
                }
                $itemTable->closecursor();
                $selectTable = Database::disconnect(); 
            ?>
        </select>
    </div>
</div>
<div id="logement" class="tabcontent form-group row">
    <label class="col-sm-4 form-control-label">Sous catégorie</label>
    <div class="col-sm-8">
        <select class="form-control" name="thematique">
            <?php         
                $selectTable = Database::connect();
                $itemTable = $selectTable->query('SELECT * FROM declic_api_thematique WHERE categorie = "Je cherche un logement"');
                $selected = "";
                while ($selecter = $itemTable->fetch()) {
                if ($selecter['thematique'] != $selected) 
                {
                    ?>
                        <option value="<?php echo $selecter['thematique']; ?>"><?php echo $selecter['thematique']; ?></option>
                    <?php
                }
                $selected = $selecter['thematique'];
                }
                $itemTable->closecursor();
                $selectTable = Database::disconnect(); 
            ?>
        </select>
    </div>
</div>
<div id="mobilite" class="tabcontent form-group row">
    <label class="col-sm-4 form-control-label">Sous catégorie</label>
    <div class="col-sm-8">
        <select class="form-control" name="thematique">
            <?php         
                $selectTable = Database::connect();
                $itemTable = $selectTable->query('SELECT * FROM declic_api_thematique WHERE categorie = "Je me déplace en France, ou à l\'étranger"');
                $selected = "";
                while ($selecter = $itemTable->fetch()) {
                if ($selecter['thematique'] != $selected) 
                {
                    ?>
                        <option value="<?php echo $selecter['thematique']; ?>"><?php echo $selecter['thematique']; ?></option>
                    <?php
                }
                $selected = $selecter['thematique'];
                }
                $itemTable->closecursor();
                $selectTable = Database::disconnect(); 
            ?>
        </select>
    </div>
</div>
<div id="sante" class="tabcontent form-group row">
    <label class="col-sm-4 form-control-label">Sous catégorie</label>
    <div class="col-sm-8">
        <select class="form-control" name="thematique">
            <?php         
                $selectTable = Database::connect();
                $itemTable = $selectTable->query('SELECT * FROM declic_api_thematique WHERE categorie = "Je m\'informe sur ma santé"');
                $selected = "";
                while ($selecter = $itemTable->fetch()) {
                if ($selecter['thematique'] != $selected) 
                {
                    ?>
                        <option value="<?php echo $selecter['thematique']; ?>"><?php echo $selecter['thematique']; ?></option>
                    <?php
                }
                $selected = $selecter['thematique'];
                }
                $itemTable->closecursor();
                $selectTable = Database::disconnect(); 
            ?>
        </select>
    </div>
</div>
<div id="sociale" class="tabcontent form-group row">
    <label class="col-sm-4 form-control-label">Sous catégorie</label>
    <div class="col-sm-8">
        <select class="form-control" name="thematique">
            <?php         
                $selectTable = Database::connect();
                $itemTable = $selectTable->query('SELECT * FROM declic_api_thematique WHERE categorie = "J\'organise ma vie sociale, je m\'engage"');
                $selected = "";
                while ($selecter = $itemTable->fetch()) {
                if ($selecter['thematique'] != $selected) 
                {
                    ?>
                        <option value="<?php echo $selecter['thematique']; ?>"><?php echo $selecter['thematique']; ?></option>
                    <?php
                }
                $selected = $selecter['thematique'];
                }
                $itemTable->closecursor();
                $selectTable = Database::disconnect(); 
            ?>
        </select>
    </div>
</div>
<?php
# Hook Plugins
eval($plxAdmin->plxPlugins->callHook('AdminUserFoot'));
# On inclut le footer
include(dirname(__FILE__).'/foot.php');
?>