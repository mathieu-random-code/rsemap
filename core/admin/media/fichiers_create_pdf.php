<div class="windows_float">
	<div class="card-body">
		<form action="<?php echo $linkpage; ?>" method="post" enctype="multipart/form-data" id="form">
			<!-- Titre -->
			<div class="form-group row">
			  <label class="col-sm-4 form-control-label">Titre</label>
			  <div class="col-sm-8">
			  <input type="text" name="titre" id="titre" class="form-control" size="50" maxlength="255" required>
			  </div>
			</div>
			<!-- url_pdf -->
			<!-- Fichier PDF 1-->
			<div class="form-group row">
			  <label class="col-sm-4 form-control-label">Fichier PDF</label>
			  <div class="col-sm-8">
			  	<input style="color: white;" type="file" name="url_pdf" id="url_pdf" accept=".pdf" required>
				<progress value="0" id="progressbar_pdf"></progress>
			  </div>
			</div>

			<div class="d-flex justify-content-center">
				<div class="p-2">
					<button name="create_page" class="btn btn-primary" type="submit" value="valider">
						<i class="fas fa-check-square"></i> Valider
					</button>
					<button type="button" class="btn btn-danger" onclick="window.location.href='<?php echo $linkpage; ?>'">
						<i class="fas fa-window-close"></i> Annuler
					</button>
				</div>
			</div>
			<p style="color: white;">** : Taille limite du PDF : 1Mo</p>
			</div>
		</form>
	</div>
</div>


