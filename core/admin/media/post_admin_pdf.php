<?php
// Module d'enregistrement DECLIC
if(!defined('PLX_ROOT')) exit;
# Control de l'accès à la page en fonction du profil de l'utilisateur connecté
$plxAdmin->checkProfil(PROFIL_ADMIN, PROFIL_MODERATOR);
// ----------------------- VARIABLE TEXTE ---------------------------------------
$TxtImg = "L'image doit être inferieur 512 ko";
$TxtStrc = "Le nom de la structure et sa description est obligatoire";
$TxtCP = "L'adresse postale est obligatoire";
$TxtGPS = "Latitude et Longitude non renseigné !, Enregistrement annulé !";
$TxtNoImg = "Vous n'avez pas choisi le logo";
$TxtOpOk = "Enregistrement éffectué";
$TxtDelOk = "Données supprimées, opération éffectué";

// --------------------------------- FONCTION -----------------------------------------------

function InsertSQL($vUrl)
{
  $Bdd = Database::connect();
  $req = $Bdd->prepare('INSERT INTO media_pdf (titre, url_pdf) VALUES (:titre, :url_pdf)');
  $req->execute(array(
    'titre' => $_POST['titre'],
    'url_pdf' => $vUrl
  ));

  $req->closecursor();
  $Bdd = Database::disconnect();
}

function UpdateSQL($urlPDF)
{
  $Bdd = Database::connect();
  $update = $Bdd->prepare("UPDATE media_pdf SET titre = :titre, url_pdf = :url_pdf WHERE id = :id");
  $update->execute(array(
    'titre' => $_POST['titre'],
    'url_pdf' => $urlPDF,
    'id' => $_POST['id_maj_post']
  ));

  $update->closecursor();
  $Bdd = Database::disconnect();
}

// Suppresion d'une entrée 
function EraseSQL($valID)
{
  $Bdd = Database::connect();
  $delete_item = $Bdd->prepare('DELETE FROM media_pdf WHERE id = :id_delete');
  $delete_item->execute(array('id_delete' => $valID));
  $delete_item->closecursor();
  $Bdd = Database::disconnect();
}

// DisplayAlerte
function DisplayAlerte($txt)
{
  echo "<script>alert(\"" . $txt . "\");</script>"; // Aie ! 
}

// --------------------------------------- CONDITION ----------------------------------------------
if(isset($_POST['create_page']))
{
  if (isset($_POST['titre']))
  {
    // Ajoute une vérification, présence de la variable d'image
    if(isset($_FILES['url_pdf']) != null)
    {
      $fichierTemp = $_FILES['url_pdf']['tmp_name'];
      $fichier = $_FILES['url_pdf']['name'];
      $taille = filesize($fichierTemp); //Récupère la taille du fichier
      if($taille <= 1024000 AND $taille > 0 AND $taille != null)
      {
        $url = Upload::UploadPDF($fichierTemp, $fichier); // Enregistrement de l'imagee
        // Engregistrelment BDD 
        InsertSQL($url);
        echo "Enregistrement d'une nouvelle entrée, opération éffectué";
      }
      else
      {
        DisplayAlerte($TxtNoImg);
      }
    }
    else 
    {
      DisplayAlerte("Erreur ... ");
    }
  }
}
elseif (isset($_POST['id_maj_post']))
{
  if (isset($_POST['titre']))
  {
    // Ajoute une vérification, présence de la variable d'image
    if(isset($_FILES['url_pdf']) != null)
    {
      $fichierTemp = $_FILES['url_pdf']['tmp_name'];
      $fichier = $_FILES['url_pdf']['name'];
      $taille = filesize($fichierTemp); //Récupère la taille du fichier
      if($taille <= 1024000 AND $taille > 0 AND $taille != null)
      {
        $url = Upload::UploadPDF($fichierTemp, $fichier); // Enregistrement de l'imagee
        // Engregistrelment BDD 
        InsertSQL($url);
        echo "Enregistrement d'une nouvelle entrée, opération éffectué";
      }
      else
      {
        DisplayAlerte($TxtNoImg);
      }
    }
    else 
    {
      DisplayAlerte("Erreur ... ");
    }
  }
}
elseif (isset($_POST['id_del']))
{
  EraseSQL($_POST['id_del']);
}



