<?php
/**
 * Requette SQL DataMaps
 *
 * @package PLX
 * @author	Stephane F.
 **/

 if(!defined('PLX_ROOT')) exit;
 # Control de l'accès à la page en fonction du profil de l'utilisateur connecté
 $plxAdmin->checkProfil(PROFIL_ADMIN, PROFIL_MODERATOR);
//service de tri et lecture de la base
$Bdd = Database::connect();
$item = $Bdd->query('SELECT * FROM media_pdf ORDER BY id');
