<?php
    require('Database.php');
    $Database = Database::connect();
    $TableFilter = $Database->query('SELECT * FROM rse_gestion_filtres WHERE cat_filtres = "ChampPerso3" ORDER BY nom_filtres ASC');
    ?>
    <p class="title-radio">Choix de l'époque</P>
    <div class="choix_epoque">
      <?php
        while ($datafilter = $TableFilter->fetch())
        {
      ?>
          <div class="input_container"> 
            <input onchange="filtreCheck()" type="checkbox" id="<?php echo $datafilter['nom_filtres']; ?>" name="ChampPerso3" value="<?php echo $datafilter['nom_filtres']; ?>" /> 
            <label class="checkmark" for="<?php echo $datafilter['nom_filtres']; ?>"></label>
            <label class="checkbox-txt" for="<?php echo $datafilter['nom_filtres']; ?>"><?php echo $datafilter['nom_filtres']; ?></label>
            <br/>
          </div>
      <?php
        }
        $TableFilter->closecursor();
        $Database = Database::disconnect();
      ?>
    </div>
    <input type="button" id="resetFiltre" value="Réinitialiser les filtres" onclick="filtreReset()">

