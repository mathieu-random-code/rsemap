<?php

/**
 * Recup des configuration du site en XML
 *
 * @package PLX
 * @author	Florent MONTHEL, Stephane F 
 **/

include(dirname(__FILE__).'/prepend.php');
include(PLX_CORE.'lib/class.plx.timezones.php');

# Control du token du formulaire
plxToken::validateFormToken($_POST);

# Control de l'accès à la page en fonction du profil de l'utilisateur connecté
$plxAdmin->checkProfil(PROFIL_ADMIN);

# On édite la configuration
if(!empty($_POST)) {
	$plxAdmin->editConfiguration($plxAdmin->aConf,$_POST);
	header('Location: parametres_base.php');
	exit;
}
    // Start XML file, create parent node
    $dom = new DOMDocument("1.0");
    $node = $dom->createElement("config");
    $parnode = $dom->appendChild($node);
    header("Content-type: text/xml");

    $node = $dom->createElement("config");
    $newnode = $parnode->appendChild($node);
    $newnode->setAttribute("popup", $plxAdmin->aConf['popup'] );
    $newnode->setAttribute("popup_pos",$plxAdmin->aConf['popup_pos'] );
    
    echo $dom->saveXML();

