<?php
class Database
{
  private static $dbName = 'map' ;
  private static $dbHost = 'localhost' ;
  private static $dbUsername = 'root';
  private static $dbUserPassword = '';
  private static $cont = null;

  public static function connect() {
    if ( null == self::$cont )
    {
      try {
        self::$cont = new PDO( "mysql:host=".self::$dbHost.";"."dbname=".self::$dbName, self::$dbUsername, self::$dbUserPassword, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
      }
      catch(PDOException $e)
      {
         die('Erreur : '.$e->getMessage()); //permet d'afficher les erreurs
      }
    }
    return self::$cont;
   }
  public static function disconnect()
  {
    self::$cont = null;
  }
}
