<?php
require('Database.php');

$getEmpty = true;
$searchFilter = "";
$searchValue =array();
$isTitle = false; // Test pour le titre pour effectuer une recherche avec un LIKE % % 

$Database = Database::connect();

$TableFilter = $Database->query('SELECT * FROM rse_configmaps ORDER BY id_config ASC');
ReturnRequest($TableFilter);
/*
while ($data = $TableFilter->fetch()){
    echo $data['name_config']."->".$data['value_config']."<br>";
}*/

function ReturnRequest($data)
{
    // Start XML file, create parent node
    $dom = new DOMDocument("1.0");
    $node = $dom->createElement("configs");
    $parnode = $dom->appendChild($node);
    header("Content-type: text/xml");
    while ($row = $data->fetch())
    {
        $node = $dom->createElement("config");
        $newnode = $parnode->appendChild($node);
        $newnode->setAttribute("id", $row['id_config']);
        $newnode->setAttribute("name", $row['name_config']);
        $newnode->setAttribute("value", $row['value_config']);
    }
    echo $dom->saveXML();
}