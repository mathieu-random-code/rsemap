<!DOCTYPE html>
<html>
	<head>
		<title>Map</title>
		<link rel="stylesheet" href="css/styles.css">
		<meta charset="UTF-8">
		<!--- Google Map
		<script async defer src="https://maps.googleapis.com/maps/api/js?key=&libraries=geometry&v=weekly"defer></script>
		Font --->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"><!-- FontAwesome -->
		<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@500&display=swap" rel="stylesheet"><!-- Font Roboto -->
		<!--- Jquery -->
		<script  src="https://code.jquery.com/jquery-3.5.1.js"  integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc="  crossorigin="anonymous"></script>
		<!--- Api pour générer le PDF  -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.debug.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf-autotable/3.5.13/jspdf.plugin.autotable.min.js" integrity="sha512-AtJGnumoR/L4JbSw/HzZxkPbfr+XiXYxoEPBsP6Q+kNo9zh4gyrvg25eK2eSsp1VAEAP1XsMf2M984pK/geNXw==" crossorigin="anonymous"></script>
		<!--- Api pour les cluster des Marker  --->
		<script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
		<script src="https://unpkg.com/@google/markerclustererplus@4.0.1/dist/markerclustererplus.min.js"></script>
		<script src="https://unpkg.com/@googlemaps/markerclustererplus/dist/index.min.js"></script>
		<script src="./markerclustererplus/index.min.js" type="text/javascript"></script>
		<!--- Script de la map --->
		<script type="text/javascript" src="module_js/maps.js"></script>
	
	</head>
	<body>
		<div id="openResponsive" class="" onclick="showAside()">
			<div id="fleche" ></div>
			<div class="text">Recherche</div>
		</div>

		<aside id="mainAside" class="asideHidden">
			<div class="contenu_onglet" id="contenu_onglet_recherche">
				<?php
					include('module_data/views_filter.php');
				?>
			</div>
		</aside>

		<div id="divInfo"></div>
		<div id="datamaps"></div>	
	</body>
	<footer>
		<script>
			var root = document.documentElement;

			var i = true;
			function showAside(){
				var aside = document.getElementById('mainAside');
				var openResponsive = document.getElementById('openResponsive');
				if(i == false){
					aside.classList.add('asideShow');
					aside.classList.remove('asideHidden');
					openResponsive.classList.add('asideHidden');
					openResponsive.classList.remove('asideShow');
					root.style.setProperty('--openResposive-translate-height',aside.offsetHeight+50+"px");
					root.style.setProperty('--openResposive-translate-width',aside.offsetWidth+50+"px");
					root.style.setProperty('--divInfo-left',aside.offsetWidth+100+"px");	

					i = true;
				}else{
					aside.classList.add('asideHidden');
					aside.classList.remove('asideShow');
					openResponsive.classList.add('asideShow');
					openResponsive.classList.remove('asideHidden');
					root.style.setProperty('--openResposive-translate-height',"5%");
					root.style.setProperty('--openResposive-translate-width',"5%");	
					root.style.setProperty('--divInfo-left',"10%");	

					i = false;
				}
			}
			showAside();

			/*Fermueture de la divInfo si clique outside */
			document.addEventListener("click", (evt) => {
				var targetElement = evt.target; // clicked element
				var blocInfo = document.getElementById("divInfo");
				do {
					if (targetElement == blocInfo) {
						//console.log("Clicked inside!");
						return;
					}
					targetElement = targetElement.parentNode;
				} while (targetElement);
				CloseDivInfo();
				//console.log("Clicked outside!");
			});
			/* déplace la div et l'efface */
			function CloseDivInfo(){
				var blocInfo = document.getElementById("divInfo");
				
				blocInfo.style.opacity = "0";
				blocInfo.style.zIndex = "-1";
				infoDivOpen = false;
			}

		
		</script>

		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/regular.min.css" integrity="sha512-qgtcTTDJDk6Fs9eNeN1tnuQwYjhnrJ8wdTVxJpUTkeafKKP6vprqRx5Sj/rB7Q57hoYDbZdtHR4krNZ/11zONg==" crossorigin="anonymous" />
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/solid.min.css" integrity="sha512-QN7X/bUbbeel9bbq6JVNJXk1Zowt+n+QPN+DjhEmTa4TdL1YPCsCey5JrvfRW8xp28LDYgGG/waNVdrhwMQmVQ==" crossorigin="anonymous" />
		<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/js/regular.min.js" integrity="sha512-r6vrUzzJsN9hfGYoYo5qIFyuI4Hg/6wkIEaxCgFWneGHo1Nn2ES58YuRmrUU44JrD64yIBNwvepKexEIyk+TTQ==" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/js/solid.min.js" integrity="sha512-y0arK64LEWHxtkMLukPhGHPEu2v1fpWcK+mNRolb7DIjqqD5dVJDWKCHiBFOdM+lsPAKZZBC1x/hD+PXTS3Djg==" crossorigin="anonymous"></script>
	</footer>
</html>
